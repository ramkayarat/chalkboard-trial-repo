import { createContext, useState } from 'react';
import { adminTaskTypes } from '../constants/admin-tasks.type';
import {
  enrollStudent,
  markAdded,
  markCancelledPayment,
  markPaidPayment,
  unEnrollStudent,
  undoEnrollStudent,
} from '../services/admin.service';

export const AdminNoteContext = createContext({
  noteOpen: false,
  task: { type: '', data: undefined },
  toggleNoteOpen: () => {},
  updateTask: () => {},
  handleTask: () => {},
});

const AdminProvider = ({ children }) => {
  //  initial value = context ka valuesf
  const [noteOpen, setNoteOpen] = useState(false);
  const [task, setTask] = useState({ type: '', data: undefined });

  const toggleNoteOpen = () => setNoteOpen(!noteOpen);

  const updateTask = (taskInfo) => setTask({ ...taskInfo });
  const handleTask = async  (note) => {
    switch (task.type) {
      case adminTaskTypes.ENROLL_STUDENT:
        console.log(await enrollStudent({ ...task.data, note }));
        break;
      case adminTaskTypes.MARK_ADDED:
        console.log(await markAdded({ ...task.data, note }));
        break;
      case adminTaskTypes.MARK_CANCELLED_PAYMENT:
        console.log(await markCancelledPayment({ ...task.data, note }));
        break;
      case adminTaskTypes.MARK_PAID_PAYMENT:
        console.log(await markPaidPayment({ ...task.data, note }));
        break;
      case adminTaskTypes.UNENROLL_STUDENT:
        console.log(await unEnrollStudent({ ...task.data, note }));
        break;
      case adminTaskTypes.UNDO_ENROLL_STUDENT:
        console.log(await undoEnrollStudent({ ...task.data, note }));
        break;
      default:
        console.error('Task type undefined');
    }
  };

  return (
    <AdminNoteContext.Provider
      value={{
        noteOpen,
        toggleNoteOpen,
        task,
        updateTask,
        handleTask,
      }}
    >
      {children}
    </AdminNoteContext.Provider>
  );
};

export default AdminProvider;
