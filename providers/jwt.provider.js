import { createContext, useEffect, useState } from 'react';

import { AuthToken } from '../services/auth-token.service';

export const JWTContext = createContext(undefined);

const JWTProvider = ({ children }) => {
  const [jwt, setJWT] = useState(undefined);
  const token = AuthToken.getToken();
  
  useEffect(() => {
    setJWT(token);
  }, [token]);

  return <JWTContext.Provider value={jwt}>{children}</JWTContext.Provider>;
};

export default JWTProvider;
