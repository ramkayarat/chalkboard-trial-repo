import { createContext, useState } from 'react';

export const UnsubscribeContext = createContext({
  visible: false,
  toggleVisible: () => {},
  batchId: undefined,
  updateBatchId: () => {},
});

const UnsubscribeProvider = ({ children }) => {
  // context visible = Provider visible initial value
  const [visible, setVisible] = useState(false);
  const [batchId, setBatchId] = useState(undefined);
  const toggleVisible = () => setVisible(!visible);
  const updateBatchId = (id) => setBatchId(id);

  return (
    <UnsubscribeContext.Provider
      value={{ visible, toggleVisible, batchId, updateBatchId }}
    >
      {children}
    </UnsubscribeContext.Provider>
  );
};

export default UnsubscribeProvider;
