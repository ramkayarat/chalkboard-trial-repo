import { createContext, useState } from 'react';

export const PopUpContext = createContext({
  //it can have 3 states: null, fetching, success
  popUpState: null,
  togglePopUpState: () => {},
});

const PopUpProvider = ({ children }) => {
  const [popUpState, setPopUpState] = useState(null);

  const togglePopUpState = (popUpState) => setPopUpState(popUpState);

  return (
    <PopUpContext.Provider value={{ popUpState, togglePopUpState }}>
      {children}
    </PopUpContext.Provider>
  );
};

export default PopUpProvider;
