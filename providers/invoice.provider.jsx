import { createContext, useState } from 'react';

export const InvoiceContext = createContext({
  invoiceUrl: null,
  fetchingInvoice: false,
  toggleFetchingInvoice: () => {},
  setInvoiceUrl: () => {},
});

const InvoiceProvider = ({ children }) => {
  const [fetchingInvoice, setFetchingInvoice] = useState(false);
  const [url, setUrl] = useState(false);

  const toggleFetchingInvoice = (fetchingType) => {
    setFetchingInvoice(fetchingType);
  };

  const setInvoiceUrl = (url) => setUrl(url);

  return (
    <InvoiceContext.Provider
      value={{
        fetchingInvoice,
        invoiceUrl: url,
        toggleFetchingInvoice,
        setInvoiceUrl,
      }}
    >
      {children}
    </InvoiceContext.Provider>
  );
};

export default InvoiceProvider;
