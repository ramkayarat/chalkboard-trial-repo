import { set } from 'js-cookie';
import { createContext, useState } from 'react';

export const CheckoutContext = createContext({
  checkoutVisible: false,
  checkoutData: null,
  setCheckoutData: () => {},
  toggleCheckoutPopUp: () => {},
});

const CheckoutProvider = ({ children }) => {
  const [checkoutVisible, setCheckoutVisible] = useState(false);
  const [data, setData] = useState(null);

  const toggleCheckoutPopUp = () => setCheckoutVisible(!checkoutVisible);
  const setCheckoutData = (data) => setData(data);

  return (
    <CheckoutContext.Provider
      value={{
        checkoutVisible,
        toggleCheckoutPopUp,
        checkoutData: data,
        setCheckoutData,
      }}
    >
      {children}
    </CheckoutContext.Provider>
  );
};

export default CheckoutProvider;
