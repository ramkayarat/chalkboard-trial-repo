import AdminProvider from './admin.provider';
import CheckoutProvider from './checkout.provider';
import InvoiceProvider from './invoice.provider';
import JWTProvider from './jwt.provider';
import PopUpProvider from './pop-up.provider';

const RootProvider = ({ children }) => {
  return (
    <CheckoutProvider>
      <PopUpProvider>
        <InvoiceProvider>
          <JWTProvider>
            <AdminProvider>{children}</AdminProvider>
          </JWTProvider>
        </InvoiceProvider>
      </PopUpProvider>
    </CheckoutProvider>
  );
};

export default RootProvider;
