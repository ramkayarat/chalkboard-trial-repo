import axios from 'axios';
import { API_BASE_URL } from '../../utils/api-urls.utils';
import { catchAxiosError } from './error';

import { AuthToken } from '../auth-token.service';

const addedHeaders = {
  'Access-Control-Allow-Origin': API_BASE_URL,
  'Access-Control-Allow-Header': '*',
};

const customInstance = axios.create({
  baseURL: API_BASE_URL,
  headers: addedHeaders,
});

const baseConfig = {
  baseURL: API_BASE_URL,
};

export const get = async (route) => {
  const res = await customInstance.get(route);
  return res.data;
};

export const post = ({ route, data }) => {
  return customInstance.post(route, data).catch(catchAxiosError);
};

export const authenticatedPost = (url, data, token) => {
  const authorizationString = AuthToken.getAuthorizationString(token);
  const headersInfo = {
    headers: { Authorization: authorizationString, ...addedHeaders },
  };
  const config = { ...baseConfig, ...headersInfo };
  return axios.post(url, data, config).catch(catchAxiosError);
};

export const authenticatedGet = (url, token) => {
  const authorizationString = AuthToken.getAuthorizationString(token);
  const headersInfo = {
    headers: { Authorization: authorizationString, ...addedHeaders },
  };
  const config = { ...baseConfig, ...headersInfo };
  return axios.get(url, config).catch(catchAxiosError);
};
