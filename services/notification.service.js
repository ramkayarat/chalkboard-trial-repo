import { authenticatedPost } from './helpers/helpers';
import {
  SEND_VERIFICATION_SMS_URL,
  VERIFY_OTP_URL,
} from '../utils/api-urls.utils';

import { AuthToken } from './auth-token.service';

const token = AuthToken.getToken();

export async function sendVerificationSms({ phoneNumber }) {
  const res = await authenticatedPost(
    SEND_VERIFICATION_SMS_URL,
    { phoneNumber },
    token
  );

  if (res.error) {
    return res.error;
  } else if (!res.data) {
    return 'Something went wrong!';
  }
  return res.data;
}

export async function verifyOtp(otp) {
  const otpData = {
    otp: otp,
  };

  const res = await authenticatedPost(VERIFY_OTP_URL, otpData, token);

  if (res.error) {
    return res.error;
  } else if (!res.data) {
    return 'Something went wrong!';
  }
  return res.data;
}
