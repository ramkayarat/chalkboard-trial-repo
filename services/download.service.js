import Axios from 'axios'

export const download = async ({ setProgress, invoiceUrl }) => {
  const res = await Axios({
    url: invoiceUrl,
    method: 'GET',
    responseType: 'blob',
    onDownloadProgress: (progressEvent) => {
      const { loaded, total } = progressEvent;
      let progress = Math.round((loaded / total) * 100);
      setProgress(progress);
    },
  });

  return res;
};
