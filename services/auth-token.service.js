// import jwtDecode from "jwt-decode";
import Cookie from 'js-cookie';

// the field through which we are storing the token
const TOKEN_STORAGE_KEY = 'cba-nextjs.authToken';

export class AuthToken {
  //   var decodedToken;

  // constructor( token) {
  //   this.decodedToken = { email: "", exp: 0 };
  //   try {
  //     if (token) this.decodedToken = jwtDecode(token);
  //   } catch (e) {}
  // }

  // get expiresAt() {
  //   return new Date(this.decodedToken.exp * 1000);
  // }

  // get isExpired() {
  //   return new Date() > this.expiresAt;
  // }

  // get isAuthenticated() {
  //   return !this.isExpired;
  // }
  // replace with class if needed
  // we are using these

  static getAuthorizationString(token) {
    return `Bearer ${token}`;
  }

  static storeToken(token) {
    Cookie.set(TOKEN_STORAGE_KEY, token);
  }

  static getToken() {
    const token = Cookie.get(TOKEN_STORAGE_KEY);
    return token;
  }
  static removeToken() {
    Cookie.remove(TOKEN_STORAGE_KEY);
  }
}
