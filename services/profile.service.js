import { authenticatedGet, authenticatedPost } from './helpers/helpers';
import {
  GET_STUDENT_DATA_URL,
  UPDATE_STUDENT_PROFILE_URL,
  GET_USER_DATA_URL,
} from '../utils/api-urls.utils';
import { AuthToken } from './auth-token.service';

const token = AuthToken.getToken();

export async function getStudentInfo(studentDataUrl) {
  const res = await authenticatedGet(studentDataUrl, token);

  if (res.error) {
    return res.error;
  } else if (!res.data) {
    return 'Something went wrong!';
  }
  return res.data;
}

export async function getUserInfo(userDataUrl) {
  const res = await authenticatedGet(userDataUrl, token);

  if (res.error) {
    return res.error;
  } else if (!res.data) {
    return 'Something went wrong!';
  }
  return res.data;
}

export async function getStudentSubscriptions(token) {
  const data = await getStudentInfo(token);

  return data && data.subscriptons;
}

export async function updateStudentProfile({ studentData, token }) {
  const updatedData = {
    newData: studentData,
  };

  const res = await authenticatedPost(
    UPDATE_STUDENT_PROFILE_URL,
    updatedData,
    token
  );

  if (res.error) {
    return res.error;
  } else if (!res.data) {
    return 'Something went wrong!';
  }
  return res.data;
}
