import { post } from './helpers/helpers';
import { CONTACT_SUPPORT_ROUTE } from '../utils/api-urls.utils';

export const contactSupport = async ({ email, message, name }) => {
  const data = new URLSearchParams({ email, message, name });

  const res = await post({ route: CONTACT_SUPPORT_ROUTE, data });

  if (res.error) {
    return res.error;
  }

  return res.data;
};
