import { authenticatedGet } from "./helpers/helpers";
import { AuthToken } from "./auth-token.service";
import { catchAxiosError } from "./helpers/error";

export async function getTasks(url) {
    const token =AuthToken.getToken();
    const res = await authenticatedGet(url, token).catch(catchAxiosError);
    
    return res.data;
}
