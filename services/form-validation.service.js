const strapiEmailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const phoneNumberRegex = /^[6|7|8|9][0-9]{9}$/;

export const validateForm = (credentials, formErrors) => {
  let valid = true;

  Object.values(formErrors).forEach((val) => {
    val.length > 0 && (valid = false);
  });

  Object.values(credentials).forEach((val) => {
    (val === null || val === "") && (valid = false);
  });

  return valid;
};

export const validateInput = (id, value, errors, referencePassword = "") => {
  switch (id) {
    case "email":
      errors.email = value.match(strapiEmailRegex) ? "" : "email not valid";
      break;
    case "password":
      errors.password = value.length >= 8 ? "" : "minimum 8 characters needed";
      break;
    case "phoneNumber":
      errors.phoneNumber = value.match(phoneNumberRegex)
        ? ""
        : "enter valid 10 digit phone number";
      break;
    case "confirmPassword":
      errors.confirmPassword =
        value === referencePassword ? "" : "passwords do not match";
      break;
    default:
  }

  return errors;
};
