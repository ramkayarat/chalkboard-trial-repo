import { authenticatedGet, authenticatedPost } from "./helpers/helpers";
import {
  GENEARTE_INVOICE_URL,
  GET_INVOICE_URL,
  CANCEL_SUBSCRIPTION_URL,
  FULFILL_ORDER_URL,
  FULFILL_PENDING_PAYMENT_URL,
  GENEARTE_PENDING_PAYMENT_INVOICE_ROUTE,
} from "../utils/api-urls.utils";
import { AuthToken } from "./auth-token.service";

const token = AuthToken.getToken();

export async function getPayments(url) {
  const res = await authenticatedGet(url, token);

  return res.data;
}

export async function getInvoice({ paymentId }) {
  const getInvoiceData = { paymentId };

  const res = await authenticatedPost(GET_INVOICE_URL, getInvoiceData, token);

  if (res.error) {
    return res.error;
  } else if (!res.data) {
    return "Something went wrong!";
  }

  return res.data;
}

export async function generateInvoice({ batchId, paymentPlan }) {
  const generateInvocieData = {
    paymentPlan: paymentPlan,
    batchId: batchId,
  };

  const res = await authenticatedPost(
    GENEARTE_INVOICE_URL,
    generateInvocieData,
    token
  );

  if (res.error) {
    return res.error;
  } else if (!res.data) {
    return "Something went wrong!";
  }
  return res.data;
}

export async function generatePendingPaymentInvoice({ paymentId }) {
  const generateInvocieData = {
    paymentId: paymentId,
  };

  const res = await authenticatedPost(
    GENEARTE_PENDING_PAYMENT_INVOICE_ROUTE,
    generateInvocieData,
    token
  );

  if (res.error) {
    return res.error;
  } else if (!res.data) {
    return "Something went wrong!";
  }
  return res.data;
}

export async function cancelSubscription({ batchId }) {
  const requestData = {
    batchId: batchId,
  };
  const res = await authenticatedPost(
    CANCEL_SUBSCRIPTION_URL,
    requestData,
    token
  );

  if (res.error) {
    return res.error;
  } else if (!res.data) {
    return "Something went wrong!";
  }
  return res.data;
}

export async function fulfillOrder({ txnId, responseHash, status }) {
  let token = AuthToken.getToken();
  const generateInvocieData = {
    txnId,
    responseHash,
    status,
  };

  const res = await authenticatedPost(
    FULFILL_ORDER_URL,
    generateInvocieData,
    token
  );

  if (res.error) {
    return res.error;
  } else if (!res.data) {
    return "Something went wrong!";
  }
  return res.data;
}

export async function fulfillPendingPayment({
  txnId,
  status,
  paymentId,
  responseHash,
}) {
  const requestData = {
    txnId,
    status,
    paymentId,
    responseHash,
  };

  const res = await authenticatedPost(
    FULFILL_PENDING_PAYMENT_URL,
    requestData,
    token
  );

  if (res.error) {
    return res.error;
  } else if (!res.data) {
    return "Something went wrong!";
  }
  return res.data;
}
