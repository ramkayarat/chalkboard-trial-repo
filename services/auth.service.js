import Router from 'next/router';
import { post } from './helpers/helpers';
import { AuthToken } from './auth-token.service';
import {
  SIGNUP_URL,
  LOGIN_URL,
  FORGOT_PASSWORD_URL,
  RESET_PASSWORD_URL,
} from '../utils/api-urls.utils';
import { catchAxiosError } from './helpers/error';
import {
  handleLoginError,
  handlerSignUpError,
  handlePassowrdResetError,
} from '../utils/error-handler.utils';
import { AppRoutes } from '../constants/app-routes';

export async function signUp(signUpInfo) {
  const data = new URLSearchParams(signUpInfo);
  const res = await post({ route: SIGNUP_URL, data }).catch(catchAxiosError);

  if (res.error) {
    const message = handlerSignUpError(res.error);
    alert(message);
    return message;
  } else if (!res.data) {
    return 'Something went wrong!';
  }
  alert(
    'confirmation email has been sent. To preceed furthur , please confirm your email'
  );

  Router.push(AppRoutes.HOME);
}

export async function login(loginInfo) {
  const loginData = {
    identifier: loginInfo.email,
    password: loginInfo.password,
  };
  const data = new URLSearchParams(loginData);
  const res = await post({ route: LOGIN_URL, data });
  if (res.error) {
    const message = handleLoginError(res.error);
    alert(message);
    return message;
  } else if (!res.data || !res.data.jwt) {
    return 'Something went wrong!';
  }
  const {
    jwt,
    user: {
      role: { type },
    },
  } = res.data;
  // store the token into cookies
  AuthToken.storeToken(jwt);
  // sending to home page on succesfull login
  if (type === 'admin') await Router.push(AppRoutes.ADMIN);
  else await Router.push(AppRoutes.HOME);
}

export const logout = async () => {
  AuthToken.removeToken();
  alert('logout');
  await Router.push(AppRoutes.LOGIN);
};

export const forgotPassword = async (email) => {
  const emailData = {
    email: email,
  };
  const data = new URLSearchParams(emailData);
  const res = await post({ route: FORGOT_PASSWORD_URL, data });

  if (res.error) {
    const message = handlePassowrdResetError(res.error);
    alert(message);
    return message;
  } else if (!res.data) {
    return 'Something went wrong!';
  }

  return res.data;
};

export const resetPassword = async ({ code, password }) => {
  // we are assuming that the form validation will confirm the password
  const resetPasswordData = {
    code: code,
    password: password,
    passwordConfirmation: password,
  };
  const data = new URLSearchParams(resetPasswordData);
  const res = await post({ route: RESET_PASSWORD_URL, data });

  if (res.error) {
    const message = handlePassowrdResetError(res.error);
    alert(message);
    return message;
  } else if (!res.data) {
    return 'Something went wrong!';
  }

  const {
    jwt,
    user: {
      role: { type },
    },
  } = res.data;
  // store the token into cookies
  AuthToken.storeToken(jwt);
  // sending to home page on succesfull login
  if (type === 'admin') await Router.push(AppRoutes.ADMIN);
  else await Router.push(AppRoutes.HOME);

  return res.data;
};
