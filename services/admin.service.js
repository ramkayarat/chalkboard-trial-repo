import { authenticatedPost } from './helpers/helpers';
import {
  ENROLL_STUDENT_URL,
  MARK_ADDED_URL,
  MARK_CANCELLED_PAYMENT_URL,
  MARK_PAID_PAYMENT_URL,
  UN_ENROLL_STUDENT_URL,
  UNDO_ENROLL_STUDENT_URL,
} from '../utils/api-urls.utils';

export async function enrollStudent({ token, note, taskId, batchId }) {
  const data = {
    note: note,
    taskId: taskId,
    batchId: batchId,
  };
  const res = await authenticatedPost(ENROLL_STUDENT_URL, data, token);
  if (res.error) {
    return res.error;
  } else if (!res.data) {
    return 'Something went wrong!';
  }
  return res.data;
}

export async function markAdded({ taskId, studentId, note, token }) {
  const data = {
    taskId: taskId,
    studentId: studentId,
    note: note,
  };
  const res = await authenticatedPost(MARK_ADDED_URL, data, token);

  if (res.error) {
    return res.error;
  } else if (!res.data) {
    return 'Something went wrong!';
  }
  return res.data;
}
export async function markCancelledPayment({
  token,
  note,
  batchId,
  studentId,
}) {
  const data = {
    note: note,
    batchId: batchId,
    studentId: studentId,
  };
  const res = await authenticatedPost(MARK_CANCELLED_PAYMENT_URL, data, token);

  if (res.error) {
    return res.error;
  } else if (!res.data) {
    return 'Something went wrong!';
  }
  return res.data;
}
export async function markPaidPayment({ token, note, paymentId }) {
  const data = {
    note: note,
    paymentId: paymentId,
  };
  const res = await authenticatedPost(MARK_PAID_PAYMENT_URL, data, token);

  if (res.error) {
    return res.error;
  } else if (!res.data) {
    return 'Something went wrong!';
  }
  return res.data;
}

export async function unEnrollStudent({ studentId, batchId, note, token }) {
  const data = {
    studentId: studentId,
    batchId: batchId,
    note: note,
  };
  const res = await authenticatedPost(UN_ENROLL_STUDENT_URL, data, token);
  console.error(res);
  if (res.error) {
    return res.error;
  } else if (!res.data) {
    return 'Something went wrong!';
  }
  return res.data;
}
export async function undoEnrollStudent({ token, note, taskId }) {
  const data = {
    note: note,
    taskId: taskId,
  };
  const res = await authenticatedPost(UNDO_ENROLL_STUDENT_URL, data, token);

  if (res.error) {
    return res.error;
  } else if (!res.data) {
    return 'Something went wrong!';
  }
  return res.data;
}
