# chalkboard-monorepo

A single repo from which we build track and deploy CBA releases, with all the required dependencies specified in different docker compose files


## Attention
* cba-strapi folder contains the /data/db folder in which offline development mongoDB (volume) files exist. Make sure to properly close all the processes and allow all write operations to finish before making a commit and pushing your changes. Failing to do this will corrupt the strapi database and we will have to mock the data from scratch



## Scale Analysis
Current form of deployment heavily relies on docker-compose. It is mainly focused on single-server deployments with S3 based storage provider to scale assets.
This makes the most cost-resilience sense for a startup till 100SAUs or 10000 daily visitors


## Structure
* Strapi development code is in [/cba-strapi](/cba/strapi) folder. Only backend developers with experience developing with strapiJS should edit contents of that folder.
* cba-nextjs folder contains the frontend of the application. Only NextJS developers comfortable with the structure and state management practices should edit source in that folder.

## Development
* Make sure you have `docker` and `docker-compose` installed
* Copy the develop_strapi.env and develop_node.env to the root folder
* run develop.sh
* it will install all the dependencies including node, strapi etc
* once things are running, make changes to the code as you would in a traditional system and develop at your own pace
* before checking in your code, make sure that:
    * you have stopped all the containers
    * you have added any additional changes to the cba-strapi/data/db folder

Structure of `develop_strapi.env`:
```
NODE_ENV=development
DATABASE_CONNECTION=sql
SENDGRID_API_KEY=xxxxxxxxxxxxxxxxxxx
DEFAULT_EMAIL_ID=xxxxxxxxxxxxxxxx
DEFAULT_NUMBER=xxxxxxxxxxxxxxxxxxxxx
RAZORPAY_KEY=xxxxxxxxxxxxxxxxxx
RAZORPAY_SECRET=xxxxxxxxxxxxxxxxxxxx
BUCKET_NAME=bucket_name
AWS_ACCESS_KEY_ID=aws_access_key_id
AWS_SECRET_ACC`ESS_KEy=aws_secret_access_key
```

Structure of `develop_node.env`
```
NODE_ENV=development
```

## Preview
* Deployment sysadmins are required for this operation
* Create a compose file from the template of [develop-compose](develop-compose.yml) 
* Replace the contents of preview-compose.yml
* run preview.sh
Make sure the preview environment mirrors the development environment to ensure testing we can rely on

## Deployment
* Deployment sysadmins are required for this operation
* Create a compose file from the template of [develop-compose](develop-compose.yml) 
* Replace the contents of deploy-compose.yml
* run deploy.sh


