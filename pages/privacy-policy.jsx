import { useEffect } from 'react';
import { ExternalLinks } from '../constants/app-routes';

export default function PrivacyPolicy() {
  useEffect(() => {
    if (window) location.assign(ExternalLinks.PRIVACY_POLICY);
  }, []);

  return <div />;
}
