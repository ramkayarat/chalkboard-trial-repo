import { useRouter } from 'next/router';
import { useContext } from 'react';
import { JWTContext } from '../providers/jwt.provider';

import MainLayout from '../components/helper/main-layout/main-layout';
import SignUpSection from '../components/sign-up/signup-section.component';
import { AppRoutes } from '../constants/app-routes';

export default function SignUp() {
  const token = useContext(JWTContext);
  const router = useRouter();

  if (token) {
    router.push(AppRoutes.MY_PROFILE);
  }

  return (
    <MainLayout hideFooter>
      <SignUpSection />
    </MainLayout>
  );
}
