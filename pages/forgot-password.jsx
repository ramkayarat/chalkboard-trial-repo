import ForgotPasswordSection from '../components/forgot-password/forgot-password-section';
import MainLayout from '../components/helper/main-layout/main-layout';

export default function ForgotPassword() {
  return (
    <MainLayout hideFooter> 
        <ForgotPasswordSection />
    </MainLayout>
  );
}
