import Axios from 'axios';
import { GET_FAQS_URL } from '../utils/api-urls.utils';

import MainLayout from '../components/helper/main-layout/main-layout';
import LandingSection from '../components/home/landing-section/landing-section.component';
import CbaUSPs from '../components/home/cba-usps-section/cba-usps-section.component';
import LearningCategories from '../components/home/learning-categories-section/learning-categories.component';
import Teachers from '../components/home/teachers-section/teachers-section.component';
import Footer from '../components/home/footer/footer.component';
import FAQSection from '../components/home/faq-section/faq-section.component';

function Home({ faqs, err }) {
  return (
    <MainLayout hideFooter>
      <LandingSection />
      <CbaUSPs />
      <LearningCategories />
      <FAQSection faqs={err ? [] : faqs} />
      {/* TODO:Hide until futrther discussions */}
      {/* <Teachers /> */}
      <Footer />
    </MainLayout>
  );
}

export default Home;

export async function getStaticProps() {
  try {
    const res = await Axios.get(
      `${process.env.SSG_STRAPI_BASE_URL}${GET_FAQS_URL}`
    );
    if (res.status != 200) throw Error(res);
    return { props: { faqs: res.data }, revalidate: 2 * 60 };
  } catch (err) {
    return { props: { err: err.message } };
  }
}
