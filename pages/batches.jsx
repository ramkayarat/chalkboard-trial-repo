import { useRouter } from 'next/router';
import Axios from 'axios';
import { useState, useEffect } from 'react';
import {
  filterCourseByBoard,
  filterCourseByClass,
  filterCourseBySubject,
  filterCourseByDate,
} from '../utils/course.utils';
import { boards, classes, subjects } from '../constants/filter.options';
import { GET_BATCHES_URL } from '../utils/api-urls.utils';

import MainLayout from '../components/helper/main-layout/main-layout';
import BackToTop from '../components/helper/back-to-top/back-to-top';
import utilStyles from '../styles/utils.module.css';
import CoursesFilter from '../components/all-courses/courses-filter/courses-filter.component';
import AllCoursesSection from '../components/all-courses/all-courses-section/all-courses-section.component';
import Loader from '../components/helper/loader/loader';
import Error from '../components/error/error.component';
import CustomCardButton from '../components/helper/custom-card-button/custom-card-button';
import { getSelectedFilter } from '../utils/utils';

export default function AllCourses({ batches, err }) {
  const router = useRouter();
  const [selectedBoard, setSelectedBoard] = useState(null);
  const [selectedClass, setSelectedClass] = useState(null);
  const [selectedSubject, setSelectedSubject] = useState(null);
  const [selectedDate, setSelectedDate] = useState(null);

  const filterOptions = {
    educationBoard: boards,
    class: classes,
    subject: subjects,
  };

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  useEffect(() => {
    const { category } = router.query;
    if (category) {
      let educationBoardFilter = filterOptions['educationBoard'];
      const filteredBoard = educationBoardFilter.find(
        (board) => board.value.toLowerCase() === category.toLowerCase()
      );
      toggleSelected({ id: filteredBoard.id, key: 'educationBoard' });
    }
  }, [router.query]);

  if (err) {
    return <Error />;
  }

  if (!batches) {
    return <Loader />;
  }

  const toggleSelected = ({ id, key, value }) => {
    if (key === 'date') {
      setSelected({ key, date: value });
      return;
    }

    let filterOption = filterOptions[key];
    filterOption = filterOption.map((element) => {
      let temp = element;
      temp.id == id
        ? (temp.selected = !temp.selected)
        : (temp.selected = false);

      return temp;
    });

    setSelected({ key, option: filterOption, id });
  };

  const setSelected = ({ key, option, id, date }) => {
    switch (key) {
      case 'educationBoard':
        setSelectedBoard(option[id].selected ? option[id].value : null);
        break;
      case 'class':
        setSelectedClass(option[id].selected ? option[id].value : null);
        break;
      case 'subject':
        setSelectedSubject(option[id].selected ? option[id].value : null);
      case 'date':
        setSelectedDate(date === selectedDate ? null : date);
        break;
      default:
        return;
    }
  };

  const filterBatches = () => {
    let filteredBatches = batches ? [...batches] : [];
    if (selectedBoard) {
      filteredBatches = filterCourseByBoard(filteredBatches, selectedBoard);
    }

    if (selectedClass) {
      filteredBatches = filterCourseByClass(filteredBatches, selectedClass);
    }

    if (selectedSubject) {
      filteredBatches = filterCourseBySubject(filteredBatches, selectedSubject);
    }

    if (selectedDate) {
      filteredBatches = filterCourseByDate(filteredBatches, selectedDate);
    }

    //return on batches that are not stale
    return filteredBatches.filter(
      (batch) => Date.parse(batch.endDate) > Date.now()
    );
  };

  const resetFilters = () => {
    if (selectedBoard) {
      let key = 'educationBoard';
      const filteredBoard = getSelectedFilter({
        key,
        filterOptions,
        selectedValue: selectedBoard,
      });

      toggleSelected({ id: filteredBoard.id, key });
    }

    if (selectedClass) {
      let key = 'class';
      const filteredBoard = getSelectedFilter({
        key,
        filterOptions,
        selectedValue: selectedClass,
      });

      toggleSelected({ id: filteredBoard.id, key });
    }

    if (selectedSubject) {
      let key = 'subject';
      const filteredBoard = getSelectedFilter({
        key,
        filterOptions,
        selectedValue: selectedSubject,
      });

      toggleSelected({ id: filteredBoard.id, key });
    }

    if (selectedDate) {
      toggleSelected({ key: 'date', value: selectedDate });
    }
  };

  return (
    <MainLayout>
      <h4 className={`${utilStyles.heading4} my-8 text-center capitalize`}>
        Upcoming Batches
      </h4>

      <CoursesFilter
        filterOptions={filterOptions}
        toggleSelected={toggleSelected}
        selectedBoard={selectedBoard}
        selectedClass={selectedClass}
        selectedDate={selectedDate}
        selectedSubject={selectedSubject}
      />

      {selectedBoard || selectedClass || selectedDate || selectedSubject ? (
        <>
          <div className='h-5' />
          <div className='text-center'>
            <CustomCardButton onClick={() => resetFilters()}>
              Reset Filters
            </CustomCardButton>
          </div>
        </>
      ) : null}

      <div className='h-12' />

      <AllCoursesSection batches={batches} filteredBatches={filterBatches()} />
      <div className='lg:hidden'>
        <BackToTop />
      </div>
    </MainLayout>
  );
}

export async function getStaticProps() {
  try {
    const res = await Axios.get(
      `${process.env.SSG_STRAPI_BASE_URL}${GET_BATCHES_URL}`
    );

    return { props: { batches: res.data }, revalidate: 5 * 60 };
  } catch (err) {
    console.log(err.message, 'Error Found');
    return { props: { batches: null }, revalidate: 2 * 60 };
  }
}
