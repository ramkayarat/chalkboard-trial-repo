import { useRouter } from 'next/router';

import Error from '../components/error/error.component';

export default function ErrorPage() {
  const router = useRouter();
  const { paymentError ,slug} = router.query;

  return <Error paymentError={paymentError ? paymentError : false} slug={slug?slug:''}/>;
}
