import MainLayout from '../components/helper/main-layout/main-layout'
import ResetPasswordSection from '../components/reset-password/reset-password'

export default function ResetPassword(){
    return(
        <MainLayout>
            <ResetPasswordSection/>
        </MainLayout>
    )
}