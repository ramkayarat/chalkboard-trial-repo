import useSWR from 'swr';
import { AuthToken } from '../services/auth-token.service';
import { getStudentInfo } from '../services/profile.service.js';
import { GET_STUDENT_DATA_URL } from '../utils/api-urls.utils';

import MainLayout from '../components/helper/main-layout/main-layout';
import MyProfileSection from '../components/my-profile/my-profile-section.component';
import withProtection from '../components/hoc/protected-route';
import FetchingInvoice from '../components/helper/fetching-invoice/fetching-invoice.component';

function MyProfile() {
  let token = AuthToken.getToken();
  const { data, error } = useSWR(GET_STUDENT_DATA_URL, getStudentInfo);

  return (
    <MainLayout hideFooter>
      {data && <MyProfileSection data={data} token={token} />}
    </MainLayout>
  );
}

export default withProtection(MyProfile)