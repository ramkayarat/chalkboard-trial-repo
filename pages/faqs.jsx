import Axios from 'axios';
import { useState } from 'react';
import { GET_FAQS_URL } from '../utils/api-urls.utils';

import BackButton from '../components/helper/back-button/back-button';
import SelectFaqCategorySection from '../components/all-faqs/select-faq-category-section/select-faq-category-section.component';
import SelectedCategoryFaqs from '../components/all-faqs/selected-category-faqs/selected-category-faqs.component';

import MainLayout from '../components/helper/main-layout/main-layout';
import ColumnLayout from '../components/helper/column-layout/column-layout';
import BackToTop from '../components/helper/back-to-top/back-to-top';

import utilStyles from '../styles/utils.module.css';

function AllFAQs({ faqs, err }) {
  const [category, setCategory] = useState('students');

  const studentFaqs = err
    ? []
    : faqs.filter((faq) => faq.category === 'students');
  const teacherFaqs = err
    ? []
    : faqs.filter((faq) => faq.category === 'teachers');

  return (
    <MainLayout>
      <h4
        className={`${utilStyles.heading4} hidden lg:block my-8 text-center text-cba-dark-gray`}
      >
        FAQs
      </h4>
      <div className='ml-8 my-8 w-20 md:mx-auto lg:hidden'>
        <BackButton />
      </div>
      <ColumnLayout>
        <SelectFaqCategorySection
          selectedCategory={category}
          setCategory={setCategory}
        />
        <SelectedCategoryFaqs
          faqs={category === 'students' ? studentFaqs : teacherFaqs}
        />
        <div className='lg:hidden'>
          <BackToTop />
        </div>
      </ColumnLayout>
    </MainLayout>
  );
}

export default AllFAQs;

export async function getStaticProps() {
  try {
    const res = await Axios.get(
      `${process.env.SSG_STRAPI_BASE_URL}${GET_FAQS_URL}`
    );

    return { props: { faqs: res.data }, revalidate: 2 * 60 };
  } catch (err) {
    return { props: { err: err.message } };
  }
}
