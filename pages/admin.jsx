import useSWR from 'swr';
import { useContext, useState } from 'react';
import { getUserInfo } from '../services/profile.service';
import { getPayments } from '../services/payment.service';
import { getTasks } from '../services/task.service';
import { adminTaskSections } from '../components/admin/admin-task-sectoin.type';

import { JWTContext } from '../providers/jwt.provider';
import { AdminNoteContext } from '../providers/admin.provider';
import {
  GET_TASKS_URL,
  GET_USER_DATA_URL,
  GET_PAYMENTS_URL,
} from '../utils/api-urls.utils';

import AdminHeader from '../components/admin/admin-header/admin-header.component';
import AdminFooter from '../components/admin/admin-footer/admin-footer.component';
import AddStudent from '../components/admin/add-student/add-student.component';
import EnrollStudent from '../components/admin/enroll-student/enroll-student.component';
import PendingPayments from '../components/admin/pending-payments/pending-payments-column.component';
import AdminDesktop from '../components/admin/admin-desktop/admin-desktop.component';
import AdminNote from '../components/pop-ups/admin-note/admin-note';
import Loader from '../components/helper/loader/loader';
import NotLoggedIn from '../components/admin/not-logged-in/not-logged-in.component';
import NotAdmin from '../components/admin/not-admin/not-admin.component';

export default function Admin() {
  const token = useContext(JWTContext);

  const { data: user, getUserError } = useSWR(GET_USER_DATA_URL, getUserInfo);
  const { data: tasks, getTaskError } = useSWR(GET_TASKS_URL, getTasks);
  const { data: payments = [], getPaymentsError } = useSWR(
    GET_PAYMENTS_URL,
    getPayments
  );
  const { noteOpen } = useContext(AdminNoteContext);

  const [activeSection, setActiveSection] = useState(adminTaskSections.ADD_STUDENTS);

  if (!token) {
    return <NotLoggedIn />;
  }

  if (!user || !user.role) {
    return <Loader />;
  }

  //if not admin
  const { type } = user.role;
  if (type !== 'admin') {
    return <NotAdmin />;
  }

  const addStudentTasks = tasks
    ? tasks.filter((task) => task.type == adminTaskSections.ADD_STUDENTS)
    : [];
  const enrollStudentTasks = tasks
    ? tasks.filter((task) => task.type == adminTaskSections.ENROLL_STUDENTS)
    : [];

  const displaySection = (section) => {
    switch (section) {
      case adminTaskSections.ADD_STUDENTS:
        return <AddStudent addStudentTasks={addStudentTasks} />;
      case adminTaskSections.ENROLL_STUDENTS:
        return <EnrollStudent enrollStudentTasks={enrollStudentTasks} />;
      case adminTaskSections.PENDING_PAYMENTS:
        return <PendingPayments payments={payments} />;
      default:
    }
  };

  return (
    <>
      {noteOpen && <AdminNote />}

      <AdminHeader />
      <div className={`lg:hidden `}>
        {displaySection(activeSection)}
        <AdminFooter
          activeSection={activeSection}
          setActiveSection={setActiveSection}
        />
      </div>
      <div className='hidden lg:block lg:mt-10'>
        <AdminDesktop tasks={tasks} payments={payments} />
      </div>
    </>
  );
}
