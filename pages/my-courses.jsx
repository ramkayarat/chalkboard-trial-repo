import useSWR from 'swr';
import { getStudentInfo } from '../services/profile.service';
import { myCourseTypes } from '../constants/my-courses.type';
import { useState } from 'react';

import withProtection from '../components/hoc/protected-route';
import MainLayout from '../components/helper/main-layout/main-layout';
import CoursesSection from '../components/my-courses/courses-section/courses-section.component';
import MyCoursesSwitch from '../components/my-courses/my-courses-switch/my-courses-switch.component';
import BackButton from '../components/helper/back-button/back-button';
import Loader from '../components/helper/loader/loader';
import {
  getOnGoingSubscriptions,
  getPastSubscriptions,
} from '../utils/course.utils';
import { GET_BATCHES_URL, GET_STUDENT_DATA_URL } from '../utils/api-urls.utils';
import { get } from '../services/helpers/helpers';

import utilStyles from '../styles/utils.module.css';

function MyCourses() {
  const [activeCourses, setActiveCourses] = useState(myCourseTypes.ON_GOING);

  // destructuring subscriptions from data
  // PS: default value of subscriptions is empty array
  const {
    data: { subscriptions, payments } = { subscriptions: [], payments: [] },
    error,
  } = useSWR(GET_STUDENT_DATA_URL, getStudentInfo);
  const { data: batches, getBatchError } = useSWR(GET_BATCHES_URL, get);

  if (!batches) {
    return <Loader />;
  }

  const onGoingSubscriptions = getOnGoingSubscriptions({
    subscriptions,
    batches,
    payments,
  });

  const pastSubscriptions = getPastSubscriptions({
    subscriptions,
    batches,
    payments,
  });

  return (
    <MainLayout hideFooter>
      <div className='relative xl:w-4/5 lg:w-95% xxl:w-2/3 mx-auto'>
        <h4 className={`${utilStyles.heading4} my-8 text-center`}>
          My Courses
        </h4>
        <div className='hidden lg:block absolute top-0 mt-6'>
          <BackButton text='my profile' href='/my-profile' />
        </div>
      </div>
      <MyCoursesSwitch
        activeCourses={activeCourses}
        setActiveCourses={setActiveCourses}
      />

      {/* spacer */}
      <div className='h-12' />
      <CoursesSection
        past={activeCourses === myCourseTypes.PAST}
        payments={payments}
        subscriptions={
          activeCourses === myCourseTypes.ON_GOING
            ? onGoingSubscriptions
            : pastSubscriptions
        }
      />
    </MainLayout>
  );
}

export default withProtection(MyCourses);
