import useSWR from 'swr';
import { getStudentInfo } from '../services/profile.service';

import Error from '../components/error/error.component';
import Loader from '../components/helper/loader/loader';
import MainLayout from '../components/helper/main-layout/main-layout';
import withProtection from '../components/hoc/protected-route';
import MyPaymentsSection from '../components/my-payments/my-payments-section.component';

import { GET_STUDENT_DATA_URL } from '../utils/api-urls.utils';
import UnsubscribeProvider from '../providers/unsubscribe.provider';

function MyPayments() {
  const { data: { payments } = { payments: [] }, error } = useSWR(
    GET_STUDENT_DATA_URL,
    getStudentInfo
  );

  if (error) return <Error />;

  return (
    <div className='relative'>
      <MainLayout hideFooter>
        <UnsubscribeProvider>
          <MyPaymentsSection payments={payments} />
        </UnsubscribeProvider>
      </MainLayout>
    </div>
  );
}

export default withProtection(MyPayments);
