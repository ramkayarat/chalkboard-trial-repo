import { useRouter } from 'next/router';
import { useContext, useState } from 'react';
import { InvoiceContext } from '../../../providers/invoice.provider';

import BackButton from '../../../components/helper/back-button/back-button';
import FetchingInvoice from '../../../components/helper/fetching-invoice/fetching-invoice.component';
import MainLayout from '../../../components/helper/main-layout/main-layout';
import PaymentOptionSwitch from '../../../components/payment-plan/payment-option-switch/payment-option-switch';
import PaymentSection from '../../../components/payment-plan/payment-setion.component';

import utilStyles from '../../../styles/utils.module.css';
import { AppRoutes } from '../../../constants/app-routes';
import Axios from 'axios';
import { GET_BATCHES_URL } from '../../../utils/api-urls.utils';
import { getBatchSlugs } from '../../../utils/course.utils';
import { getPathsFromSlugs } from '../../../utils/utils';
import { CheckoutContext } from '../../../providers/checkout.provider';
import CheckoutPopUp from '../../../components/pop-ups/checkout/checkout-pop-up.component';

export default function PaymentPlan({ slug }) {
  // user can select from 2 payment plan one time or monthly
  const [paymentOption, setPaymentOption] = useState('one-time');
  const { fetchingInvoice } = useContext(InvoiceContext);
  const { checkoutVisible } = useContext(CheckoutContext);

  if (fetchingInvoice) {
    return <FetchingInvoice />;
  }

  return (
    <section className={`${utilStyles['yellow-pattern-background']}`}>
      <MainLayout hideFooter>
        {checkoutVisible && <CheckoutPopUp />}
        <div className={`relative w-4/5 xl:w-4/5 lg:w-95% xxl:w-2/3 mx-auto`}>
          <h4 className={`${utilStyles.heading4} my-8 text-center`}>
            Choose Your Payment Plan
          </h4>
          <div className='hidden lg:block absolute top-0 mt-6'>
            <BackButton text='back' href={`${AppRoutes.BATCHES}/${slug}`} />
          </div>
        </div>
        <PaymentOptionSwitch
          paymentOption={paymentOption}
          setPaymentOption={setPaymentOption}
        />
        <PaymentSection paymentOption={paymentOption} slug={slug} />
      </MainLayout>
    </section>
  );
}

export async function getStaticProps({ params }) {
  return { props: { slug: params.slug } };
}

export async function getStaticPaths() {
  const res = await Axios.get(
    `${process.env.SSG_STRAPI_BASE_URL}${GET_BATCHES_URL}`
  );

  const slugs = getBatchSlugs(res.data);

  const paths = getPathsFromSlugs(slugs);
  return {
    paths,
    fallback: false,
  };
}
