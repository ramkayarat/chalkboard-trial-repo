import Axios from 'axios';
import useSWR from 'swr';
import {
  GET_BATCHES_URL,
  GET_REVIEWS_URL,
  GET_STUDENT_DATA_URL,
} from '../../utils/api-urls.utils';
import { getStudentInfo } from '../../services/profile.service';
import { checkIsSubscribed, getPathsFromSlugs } from '../../utils/utils';
import { getBatchSlugs } from '../../utils/course.utils';

import Loader from '../../components/helper/loader/loader';
import MainLayout from '../../components/helper/main-layout/main-layout';
import CourseImage from '../../components/course-details/course-image/course-image';
import DetailsColumn from '../../components/course-details/detail-column/detail-column.component';
import ReviewSection from '../../components/course-details/review-section/review-section.component';
import Schedule from '../../components/course-details/schedule-section/schedule-section.component';
import Error from '../../components/error/error.component';

export default function CourseDetail({ batch, reviews, err }) {
  const { data: student, getStudentError } = useSWR(
    GET_STUDENT_DATA_URL,
    getStudentInfo
  );

  if (err || getStudentError) {
    console.log({err,getStudentError})
    return <Error />;
  }

  if (!student) {
    return <Loader />;
  }

  const { course, id: batchId } = batch;

  // const courseReviews = reviews.filter(
  //   (review) => review.courseId == course.id
  // );

  const isSubscribed = checkIsSubscribed(student.subscriptions, batchId);

  return (
    <MainLayout>
      <CourseImage featureImageUrl={course.featureImage.url} />
      <DetailsColumn batch={batch} isSubscribed={isSubscribed} />
      <Schedule {...batch} />
      {/* <ReviewSection
        reviews={courseReviews}
        isSubscribed={isSubscribed}
        {...batch}
      /> */}
    </MainLayout>
  );
}

export async function getStaticProps({ params }) {
  try {
    const baseURL = process.env.SSG_STRAPI_BASE_URL;

    //when using slug as query, strapi returns a list
    const { data: batches } = await Axios.get(
      `${baseURL}${GET_BATCHES_URL}?slug=${params.slug}`
    );

    // const { data: reviews } = await Axios.get(`${baseURL}${GET_REVIEWS_URL}`);

    return { props: { batch: batches[0] }, revalidate: 2 * 60 };
  } catch (err) {
    return { props: { err: err.message } };
  }
}

export async function getStaticPaths() {
  const res = await Axios.get(
    `${process.env.SSG_STRAPI_BASE_URL}${GET_BATCHES_URL}`
  );

  const slugs = getBatchSlugs(res.data);

  const paths = getPathsFromSlugs(slugs);
  return {
    paths,
    fallback: false,
  };
}
