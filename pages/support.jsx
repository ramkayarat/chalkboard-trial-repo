import MainLayout from '../components/helper/main-layout/main-layout';
import SupportSection from '../components/support/support-section.component';

import utilStyles from '../styles/utils.module.css';

export default function Support() {
  return (
    <section className={`${utilStyles['yellow-pattern-background']} h-screen`}>
      <MainLayout hideFooter>
        <SupportSection />
      </MainLayout>
    </section>
  );
}
