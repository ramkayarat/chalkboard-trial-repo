import Link from 'next/link';
import { AppRoutes } from '../constants/app-routes';

import { URIs } from '../constants/image-uri';
import utilStyles from '../styles/utils.module.css';

export default function InvoiceFetching() {
  return (
    <div
      className={`${utilStyles['yellow-pattern-background']} relative h-screen px-10 w-screen max-w-full flex flex-col justify-center items-center`}
    >
      <img src={URIs.LOGO_URI} width='150' />
      {/* 20px * 4 spacer */}
      <div className={utilStyles.divider} />
      <div className={utilStyles.divider} />
      <h4 className={`${utilStyles.heading4} m-0 text-center text-cba-yellow`}>
        Success!
      </h4>
      <div className={utilStyles.divider} />
      <div className={utilStyles.divider} />
      <div className={utilStyles.divider} />
      <div className={utilStyles.divider} />
      <Link href={AppRoutes.MY_PROFILE}>
        <a className={`${utilStyles.heading5} text-cba-dark-gray text-center`}>
          My Profile &#10093;
        </a>
      </Link>
    </div>
  );
}
