import '../styles/global.css';
import '../styles/fonts.css';
import '../styles/styles.css';
import 'react-circular-progressbar/dist/styles.css';
import 'react-calendar/dist/Calendar.css';
import 'font-awesome/css/font-awesome.min.css';
import Head from 'next/head';
import RootProvider from '../providers/root.provider';
import { COMPANY_TAGLINE } from '../constants/strings';

function MyApp({ Component, pageProps }) {
  return (
    <RootProvider>
      <Head>
        <title>Chalkboard Academy - {COMPANY_TAGLINE}</title>
        <link rel='icon' href='/favicon.ico' />
        <meta
          name='viewport'
          content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no'
        />
      </Head>
      <Component {...pageProps} />
    </RootProvider>
  );
}

export default MyApp;
