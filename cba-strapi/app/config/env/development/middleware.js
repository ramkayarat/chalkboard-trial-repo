module.exports = function ({ env }) {
    return ({
        load: {
            before: ["sentry", "responseTime", "logger"],
        },
        settings: {
            sentry: {
                enabled: false,
            },
            cors: {
                origin: [
                  'http://localhost:3000',
                  'http://localhost:1337', 
                  'https://test.payu.in/_payment',
                  'https://secure.payu.in/_payment',
                  'http://testtxncdn.payubiz.in',
                  'null' 
                ],
                headers: ["Content-Type", "Authorization", "X-Frame-Options", "Access-Control-Allow-Origin", "Access-Control-Allow-Header"],
              },
        },
    });
};
