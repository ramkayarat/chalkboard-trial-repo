module.exports = ({ env }) => ({
  defaultConnection: env('DATABASE_CONNECTION','sql'),
  connections: {
    sql: {
      "connector": "bookshelf",
      "settings": {
        "client": "sqlite",
        "filename": ".tmp/data.db"
      },
      "options": {
        "useNullAsDefault": true
      }
    },
  },
});