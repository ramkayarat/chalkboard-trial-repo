module.exports = function ({ env }) {
    return ({
        load: {
            before: ["sentry", "responseTime", "logger"],
        },
        settings: {
            sentry: {
                enabled: false,
            },
            cors: {
                origin: [
                  'http://localhost:1337', 
                  'https://chalkboard.org.in',
                  'https://stage.chalkboard.org.in',
                  'https://*.chalkboard.org.in',
                  'https://cms.chalkboard.org.in',
                  'https://prod.chalkboard.org.in',
                  'https://test.payu.in/_payment',
                  'https://secure.payu.in/_payment',
                  'http://testtxncdn.payubiz.in',
                  'null' 
                ],
                headers: ["Content-Type", "Authorization", "X-Frame-Options", "Access-Control-Allow-Origin", "Access-Control-Allow-Header"],
              },
        },
    });
};
