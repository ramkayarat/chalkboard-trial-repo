module.exports = ({ env }) => ({
    upload: {
        provider: 'aws-s3',
        providerOptions: {
            accessKeyId: env('AWS_ACCESS_KEY_ID'),
            secretAccessKey: env('AWS_SECRET_ACCESS_KEY'),
            region: env('AWS_REGION','ap-south-1'),
            params: {
                Bucket: env('BUCKET_NAME'),
            },
        },
    },
    email: {
        provider: 'sendgrid',
        providerOptions: {
            apiKey: env('SENDGRID_API_KEY'),
        },
        settings: {
            defaultFrom: env('DEFAULT_EMAIL_ID'),
            defaultReplyTo: env('DEFAULT_EMAIL_ID'),
        },
    },
});