module.exports = ({ env }) => ({
  
  host: env('HOST', '0.0.0.0'),
  port: env.int('PORT', 1337),
  admin: {
    auth: {
      secret: env('ADMIN_JWT_SECRET', '35a3b5fb7c932a68da3cc08257b01c32'),
    },
  },
  //TODO : Enable cronjs when needed
  cron: {
    enabled: false
  }
});
