"use strict";

/**
 * Cron config that gives you an opportunity
 * to run scheduled jobs.
 *
 * The cron format consists of:
 * [SECOND (optional)] [MINUTE] [HOUR] [DAY OF MONTH] [MONTH OF YEAR] [DAY OF WEEK]
 *
 * See more details here: https://strapi.io/documentation/v3.x/concepts/configurations.html#cron-tasks
 */

module.exports = {
  /**
   * Simple example.
   * Every monday at 1am.
   */
  // '0 1 * * 1': () => {
  //
  // }

  //Everyday 6am
  "0 6 * * *": async () => {
    var currentTime = new Date();
    console.log("Triggered cronjs at ");
    console.log(currentTime.toLocaleString());
    var currentDate= new Date.now()
    const result = await strapi.query("payments").find({ status: 'pending' });
    result.forEach(async element => {
      var dueDate  = Date(element.dueDate);
      const diffTime = Math.abs(dueDate - currentDate);
      const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
      if(diffDays<7){
        var studentId=element.id;
        const student = await strapi.query("students").find({ id: studentId });
        var  email =student.email;
        var phoneNumber = student.phoneNumber;
        var amount = element.amount;
        var batchId = element.batchId;
        console.log(`email : ${email}`);
        console.log(`phoneNumber : ${phoneNumber}`);
        console.log(`amount : ${amount}`);
        console.log(`batchId : ${batchId}`);
        strapi.services["notification-service"].sendSms();
        try {
          // strapi.services["notification-service"].sendEmail(
          //   "biswajit@cookytech.in",
          //   email,
          //   "Testing",
          //   "This is a testing email of sendgrid api"
          // );
        } catch (e) {
          console.log(e);
        }
      } 
  });
  },
};
