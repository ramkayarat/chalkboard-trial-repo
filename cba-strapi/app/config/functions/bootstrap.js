"use strict";
var faker = require("faker");

/**
 * An asynchronous bootstrap function that runs before
 * your application gets started.
 *
 * This gives you an opportunity to set up your data model,
 * run jobs, or perform some special logic.
 *
 * See more details here: https://strapi.io/documentation/v3.x/concepts/configurations.html#bootstrap
 */

module.exports = async () => {
  var generateData = false;
  if (generateData) {
    faker.locale = "en_IND";
    for (let index = 0; index < 6; index++) {
      // Create demo faqs Data
      const categories = ["students", "teachers"];
      const randomCategory = faker.helpers.randomize(categories);
      const faqData = {
        question: faker.random.words(8) + " ?",
        answer: faker.lorem.sentences(),
        category: randomCategory,
        isImportant: faker.random.boolean(),
      };
      console.log(faqData);
      await strapi.services.faqs.create(faqData);

      // Create demo teachers
      const teacherData = {
        name: faker.name.findName(),
        imageUrl: faker.image.avatar(),
        about: "faculty , A B C School",
      };
      console.log(teacherData);
      await strapi.services.teachers.create(teacherData);

      // Create demo Course  data
      // Current Subjects
      const subjects = [
        "Science",
        "Mathematics",
        "Social_Sciences",
        "Language",
        "Programming",
        "Others",
      ];
      const randomSubject = faker.helpers.randomize(subjects);
      const randomClass = Math.floor(Math.random() * 5) + 5;
      // Current Boards
      const boards = [
        "CBSE",
        "ICSE",
        "MSBSHSE",
        "Competitive_Exam_Prep",
        "Language_OR_Culture",
        "Advanced_Skill_Development",
      ];
      const randomBoard = faker.helpers.randomize(boards);
      let courseData = {
        featureImageUrl: faker.image.nature(),
        title: faker.random.words(),
        description: faker.commerce.productDescription(),
        class: randomClass.toString(),
        subject: randomSubject,
        board: randomBoard,
      };
      console.log(courseData);
      await strapi.services.courses.create(courseData);

      // Create demo reviews
      const courseForReview = await strapi.services.courses.findOne();
      const reviewData = {
        name: faker.name.findName(),
        text: faker.lorem.sentences(),
        courseId: courseForReview.id.toString(),
        time: Date.now(),
      };
      console.log(reviewData);
      await strapi.services.reviews.create(reviewData);

      // Create demo batches
      const teacher = await strapi.services.teachers.findOne();
      const course = await strapi.services.courses.findOne();
      // singlePaymentPriceInPaise will 5000 to 10000
      const singlePaymentPriceInPaise =
        (Math.floor(Math.random() * 5) * 1000 + 5000) * 100;
      // moths between 1 to 5
      const months = Math.floor(Math.random() * 5) + 1;
      // total seats between 50 to 100
      const totalSeats = Math.floor(Math.random() * 5) * 10 + 50;
      // seats booked between 1 to tal seats
      const seatsBooked = Math.floor(Math.random() * totalSeats) + 1;
      const weekNames = [
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday",
      ];
      let weekConfiguration = [];
      const noOfSessionsPerWeek = Math.floor(Math.random() * 6) + 1;
      for (var i = 0; i < noOfSessionsPerWeek; i++) {
        const randomWeekName = faker.helpers.randomize(weekNames);
        weekNames.pop(randomWeekName);
        const weekConfigurationData = {
          startTime: "14:00:00.00",
          endTime: "16:00:00.00",
          weekday: randomWeekName,
        };
        weekConfiguration.push(weekConfigurationData);
      }
      let monthlyPayments = [];
      for (var i = 0; i < months; i++) {
        const monthlyPaymentsData = {
          amountInPaise: singlePaymentPriceInPaise / months,
          dueDate: faker.date.future(),
        };
        monthlyPayments.push(monthlyPaymentsData);
      }
      let batchData = {
        startDate: faker.date.future(),
        endDate: faker.date.future(),
        teacher: teacher,
        course: course,
        weekConfiguration: weekConfiguration,
        singlePaymentPriceInPaise: singlePaymentPriceInPaise,
        totalSeats: totalSeats,
        seatsBooked: seatsBooked,
        monthlyPayments: monthlyPayments,
        durationInMonths: months,
      };
      console.log(batchData);
      await strapi.services.batches.create(batchData);
    }
  }
};
