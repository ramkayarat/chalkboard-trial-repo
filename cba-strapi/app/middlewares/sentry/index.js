const Sentry = require("@sentry/node");
Sentry.init({
  dsn:
    "https://bf467511428e41fea9d18bcb16c48737@o449801.ingest.sentry.io/5433400",
  environment: strapi.config.environment,
});

module.exports = (strapi) => {
  // Sentry.captureException(Error("testing"));
  return {
    initialize() {
      console.log("Initialized sentry");
      strapi.app.use(async (ctx, next) => {
        try {
          await next();
        } catch (error) {
          console.log("Sending to sentry ...");
          Sentry.captureException(error);
          throw error;
        }
      });
    },
  };
};
