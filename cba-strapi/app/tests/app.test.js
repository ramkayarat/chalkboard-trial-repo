const fs = require("fs");
const { setupStrapi } = require("./helpers/strapi");
const { init } = require("./helpers/init");
const { generateDemoData } = require("./helpers/generate-demo-data");

/** this code is called once before any test is called */
beforeAll(async (done) => {
  // strapi need more than 5000 ms (default) for start so changing the default time
  jest.setTimeout(30000);
  // singleton so it can be called many times
  await setupStrapi();
  // init will register and login the admin and modify the permission for
  // authenticated user so that we can call our apis
  await init();
  const batch = await strapi.services.batches.findOne();
  if (!batch) await generateDemoData();
  done();
});

/** this code is called once before all the tested are finished */
afterAll(async (done) => {
  const dbSettings = strapi.config.get("database.connections.default.settings");
  //delete test database after all tests
  // if (dbSettings && dbSettings.filename) {
  //   const tmpDbFile = `${__dirname}/../${dbSettings.filename}`;
  //   if (fs.existsSync(tmpDbFile)) {
  //     fs.unlinkSync(tmpDbFile);
  //   }
  // }
  done();
});

it("strapi is defined", async (done) => {
  expect(strapi).toBeDefined();
  done();
});

//individual test files
// require("./user");
// require("./admin");
// require("./student");
// require("./notification");
require("./payment");
