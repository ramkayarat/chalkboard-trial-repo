const {
  registerAndGetToken,
  removeUserAndStudent,
} = require("../helpers/auth");

const { makePostRequest } = require("../helpers/make-request");
const { mockUserData } = require("../helpers/constants");

describe("send verification sms  API", () => {
  let jwt;
  const phoneNumber = "1234567890";
  beforeAll(async () => {
    jwt = await registerAndGetToken();
  });

  afterAll(async () => {
    await removeUserAndStudent();
  });

  it("should able to send verification sms", async (done) => {
    const res = await makePostRequest({
      jwt,
      url: "/send-verification-sms",
      data: {
        phoneNumber,
      },
    });

    expect(res.statusCode).toBe(200);
    expect(res.text).toBe("success");
    done();
  });

  it("should generate new verification info ", async () => {
    const student = await strapi.services.student.findOne({
      email: mockUserData.email,
    });
    expect(student).toMatchObject({
      verificationInfo: {
        phoneNumber: phoneNumber,
        isVerified: false,
        verificationTime: null,
      },
    });
  });
});
