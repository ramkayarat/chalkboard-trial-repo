const {
  registerAndGetToken,
  removeUserAndStudent,
} = require("../helpers/auth");
const { mockUserData } = require("../helpers/constants");
const { makePostRequest } = require("../helpers/make-request");

describe("verify otp API", () => {
  let jwt, student;

  beforeAll(async () => {
    jwt = await registerAndGetToken();
    student = await strapi.services.student.findOne({
      email: mockUserData.email,
    });
  });

  afterAll(async () => {
    await removeUserAndStudent();
  });

  it("should throw error if otp length is not 6", async (done) => {
    const otp = 12345;
    const res = await makePostRequest({
      url: "/verify-otp",
      jwt,
      data: {
        otp: otp,
      },
    });

    expect(res.statusCode).toBe(400);
    const { message } = JSON.parse(res.text);
    expect(message).toBe("otp length is not 6");
    done();
  });

  it("should throw error if otp is incorrect ", async (done) => {
    const otp = 123456;
    const res = await makePostRequest({
      url: "/verify-otp",
      jwt,
      data: {
        otp: otp,
      },
    });

    expect(res.statusCode).toBe(400);
    const { message } = JSON.parse(res.text);
    expect(message).toBe("Incorrect otp");
    done();
  });

  it("should able to verify with correct otp", async (done) => {
    const res = await makePostRequest({
      url: "/verify-otp",
      jwt,
      data: {
        otp: student.verificationInfo.otp,
      },
    });

    expect(res.statusCode).toBe(200);
    expect(res.text).toBe("success");
    done();
  });

  it("should update the student data", async () => {
    const updatedStudent = await strapi.services.student.findOne({
      email: mockUserData.email,
    });
    expect(updatedStudent).toMatchObject({
      phoneNumber: student.verificationInfo.phoneNumber,
      verificationInfo: {
        isVerified: true,
        verificationTime: expect.anything(),
      },
    });
  });
});
