const { makePostRequest } = require("../helpers/make-request");
const crypto = require("crypto");
const {
  registerAndGetToken,
  removeUserAndStudent,
} = require("../helpers/auth");
const { mockUserData } = require("../helpers/constants");

describe("fulfill orders API", () => {
  let jwt, invoice, student, batch;

  beforeAll(async () => {
    jwt = await registerAndGetToken();
    student = await strapi.services.student.findOne({
      email: mockUserData.email,
    });
  });

  afterAll(async () => {
    await removeUserAndStudent();
  });

  it("should get error for incorrect hash", async (done) => {
    // get a random batch to create a task
    invoice = await strapi.services.invoices.findOne();

    const res = await makePostRequest({
      jwt,
      url: "/fulfill-order",
      data: {
        txnId: invoice.txnId,
        status: "unpaid",
        responseHash: "wrong hash",
      },
    });

    expect(res.statusCode).toBe(400);
    expect(res.body.message).toBe("Error in payment verification");
    done();
  });

  it("should ful fill the order for one_time", async (done) => {
    const paymentPlan = "one_time";
    batch = await strapi.services.batches.findOne();
    // get a random batch to create a task
    invoice = await strapi.services.invoices.findOne({
      productInfo: `${batch.id}_${paymentPlan}`,
    });
    const status = "paid";
    const salt = process.env.PAYU_SALT;
    const productinfo = `${invoice.batchId}_${invoice.paymentPlan}`;
    var keyString =
      invoice.key +
      "|" +
      invoice.txnId +
      "|" +
      invoice.amount +
      "|" +
      productinfo +
      "|" +
      student.firstName +
      "|" +
      student.email +
      "|||||" +
      invoice.udf5 +
      "|||||";
    var keyArray = keyString.split("|");
    var reverseKeyArray = keyArray.reverse();
    var reverseKeyString =
      salt + "|" + status + "|" + reverseKeyArray.join("|");

    var cryp = crypto.createHash("sha512");
    cryp.update(reverseKeyString);
    var calchash = cryp.digest("hex");
    const res = await makePostRequest({
      jwt,
      url: "/fulfill-order",
      data: {
        txnId: invoice.txnId,
        status,
        responseHash: calchash,
      },
    });

    expect(res.statusCode).toBe(200);
    expect(res.text).toBe("success");
    done();
  });

  it("should ful fill the order for monthly", async (done) => {
    const paymentPlan = "monthly";
    batch = await strapi.services.batches.findOne();
    // get a random batch to create a task
    invoice = await strapi.services.invoices.findOne({
      productInfo: `${batch.id}_${paymentPlan}`,
    });
    const status = "paid";
    const salt = process.env.PAYU_SALT;
    const productinfo = `${invoice.batchId}_${invoice.paymentPlan}`;
    var keyString =
      invoice.key +
      "|" +
      invoice.txnId +
      "|" +
      invoice.amount +
      "|" +
      productinfo +
      "|" +
      student.firstName +
      "|" +
      student.email +
      "|||||" +
      invoice.udf5 +
      "|||||";
    var keyArray = keyString.split("|");
    var reverseKeyArray = keyArray.reverse();
    var reverseKeyString =
      salt + "|" + status + "|" + reverseKeyArray.join("|");

    var cryp = crypto.createHash("sha512");
    cryp.update(reverseKeyString);
    var calchash = cryp.digest("hex");
    const res = await makePostRequest({
      jwt,
      url: "/fulfill-order",
      data: {
        txnId: invoice.txnId,
        status,
        responseHash: calchash,
      },
    });

    expect(res.statusCode).toBe(200);
    expect(res.text).toBe("success");
    done();
  });
});
