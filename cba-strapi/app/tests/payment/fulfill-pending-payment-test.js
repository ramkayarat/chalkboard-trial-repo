const { makePostRequest } = require("../helpers/make-request");
const crypto = require("crypto");
const {
  registerAndGetToken,
  removeUserAndStudent,
} = require("../helpers/auth");
const { mockUserData } = require("../helpers/constants");

describe("fulfill pending payments API", () => {
  let jwt, invoice, student, batch, payment;

  beforeAll(async () => {
    jwt = await registerAndGetToken();
    student = await strapi.services.student.findOne({
      email: mockUserData.email,
    });
  });

  afterAll(async () => {
    await removeUserAndStudent();
  });
  it("should get error for incorrect hash", async (done) => {
    // get a random batch to create a task
    batch = await strapi.services.batches.findOne();
    invoice = await strapi.services.invoices.findOne();
    payment = await strapi.services.payments.create({
      batchId: batch.id.toString(),
      isLmsAdminFulfilled: false,
      student: student,
      dueDate: Date.now(),
      status: "pending",
      amount: 10000,
      pdfUrl: null,
      invoice: invoice.id,
    });
    const res = await makePostRequest({
      jwt,
      url: "/fulfill-pending-payment",
      data: {
        txnId: invoice.txnId,
        status: "unpaid",
        responseHash: "wrong hash",
        paymentId: payment.id,
      },
    });

    expect(res.statusCode).toBe(400);
    expect(res.body.message).toBe("Error in payment verification");
    done();
  });
  it("should ful fill the pending payment", async (done) => {
    batch = await strapi.services.batches.findOne();

    // get a random batch to create a task
    invoice = await strapi.services.invoices.findOne();
    payment = await strapi.services.payments.create({
      batchId: batch.id.toString(),
      isLmsAdminFulfilled: false,
      student: student,
      dueDate: Date.now(),
      status: "pending",
      amount: 10000,
      pdfUrl: null,
      invoice: invoice.id,
    });

    const status = "paid";
    const salt = process.env.PAYU_SALT;
    const productinfo = `${invoice.batchId}_${invoice.paymentPlan}`;
    var keyString =
      invoice.key +
      "|" +
      invoice.txnId +
      "|" +
      invoice.amount +
      "|" +
      productinfo +
      "|" +
      student.firstName +
      "|" +
      student.email +
      "|||||" +
      invoice.udf5 +
      "|||||";
    var keyArray = keyString.split("|");
    var reverseKeyArray = keyArray.reverse();
    var reverseKeyString =
      salt + "|" + status + "|" + reverseKeyArray.join("|");

    var cryp = crypto.createHash("sha512");
    cryp.update(reverseKeyString);
    var calchash = cryp.digest("hex");
    const res = await makePostRequest({
      jwt,
      url: "/fulfill-pending-payment",
      data: {
        status,
        responseHash: calchash,
        paymentId: payment.id,
      },
    });

    expect(res.statusCode).toBe(200);
    expect(res.text).toBe("success");
    done();
  });
});
