const {
  registerAndGetToken,
  removeUserAndStudent,
} = require("../helpers/auth");
const { mockUserData } = require("../helpers/constants");
const { makeGetRequest } = require("../helpers/make-request");

describe("get invoice test API", () => {
  let jwt;
  beforeAll(async () => {
    jwt = await registerAndGetToken();
  });
  afterAll(async () => {
    await removeUserAndStudent();
  });
  it("should able to get invoice", async (done) => {
    const res = await makeGetRequest({
      url: "/get-invoice",
      jwt,
    });
    // TODO : write this test when aws credential is found
    done();
  });
});
