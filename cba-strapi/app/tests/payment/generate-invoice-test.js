const { makePostRequest } = require("../helpers/make-request");
const {
  registerAndGetToken,
  removeUserAndStudent,
} = require("../helpers/auth");
const { mockUserData } = require("../helpers/constants");

describe("generate invoice API", () => {
  let jwt, batch, student, txnId;

  beforeAll(async () => {
    jwt = await registerAndGetToken();
    student = await strapi.services.student.findOne({
      email: mockUserData.email,
    });
  });

  afterAll(async () => {
    await removeUserAndStudent();
  });

  it("should get invoice for one_time plan", async (done) => {
    // get a random batch to create a task
    batch = await strapi.services.batches.findOne();
    const paymentPlan = "one_time";

    const res = await makePostRequest({
      jwt,
      url: "/generate-invoice",
      data: {
        batchId: batch.id,
        paymentPlan,
      },
    });

    expect(res.statusCode).toBe(200);
    expect(res.body).toMatchObject({
      txnId: expect.any(String),
      timeCreated: expect.anything(),
      timePaid: null,
      amount: batch.singlePaymentPriceInPaise,
      productInfo: `${batch.id}_${paymentPlan}`,
      studentId: student.id.toString(),
      firstName: student.firstName,
      email: student.email,
      isPaid: false,
      hash: expect.any(String),
      key: expect.any(String),
    });
    txnId = res.text.txnId;
    done();
  });

  it("should get invoice for monthly plan", async (done) => {
    // get a random batch to create a task
    batch = await strapi.services.batches.findOne();
    const paymentPlan = "monthly";
    let monthlyPayments = batch.monthlyPayments;
    monthlyPayments.sort(
      (a, b) => Date.parse(a.dueDate) - Date.parse(b.dueDate)
    );
    amount = monthlyPayments[0].amountInPaise;
    const res = await makePostRequest({
      jwt,
      url: "/generate-invoice",
      data: {
        batchId: batch.id,
        paymentPlan,
      },
    });

    expect(res.statusCode).toBe(200);
    expect(res.body).toMatchObject({
      txnId: expect.any(String),
      timeCreated: expect.anything(),
      timePaid: null,
      amount: amount,
      productInfo: `${batch.id}_${paymentPlan}`,
      studentId: student.id.toString(),
      firstName: student.firstName,
      email: student.email,
      isPaid: false,
      hash: expect.any(String),
      key: expect.any(String),
    });
    done();
  });
  it("should generate Invoice", async (done) => {
    // get a random batch to create a task
    const invoice = await strapi.services.invoices.findOne({ txnId });
    expect(invoice).toBeDefined();
    done();
  });
});
