const { makePostRequest } = require("../helpers/make-request");
const crypto = require("crypto");
const {
  registerAndGetToken,
  removeUserAndStudent,
} = require("../helpers/auth");
const { mockUserData } = require("../helpers/constants");

describe("cancel subscriptions API", () => {
  let jwt, student, batch;

  beforeAll(async () => {
    jwt = await registerAndGetToken();
    student = await strapi.services.student.findOne({
      email: mockUserData.email,
    });
  });

  afterAll(async () => {
    await removeUserAndStudent();
  });

  it("should cancel the subscriptions", async (done) => {
    batch = await strapi.services.batches.findOne();
    var subscriptions = student.subscriptions;
    subscriptions.push({ batchId: batch.id });
    await strapi
      .query("student")
      .update({ id: student.id }, { subscriptions: subscriptions });
    const res = await makePostRequest({
      jwt,
      url: "/cancel-subscription",
      data: {
        batchId: batch.id,
      },
    });
    expect(res.statusCode).toBe(200);
    expect(res.text).toBe("success");
    done();
  });


  it("should change the payment status to cancelled", async (done) => {
    const updatedpayments = await strapi.services.payments.find({
      batchId: batch.id.toString(),
      student: student.id,
    });
    updatedpayments.forEach((payment) => {
      expect(payment.status).toBe("cancelled");
    });
    done();
  });

});
