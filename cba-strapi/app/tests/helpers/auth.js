const { mockUserData, mockAdminData } = require("./constants");

async function registerAndGetToken() {
  /** Gets the default user role */
  const defaultRole = await strapi
    .query("role", "users-permissions")
    .findOne({}, []);

  const role = defaultRole ? defaultRole.id : null;
  /** Creates a new user an push to database */
  const user = await strapi.plugins["users-permissions"].services.user.add({
    ...mockUserData,
    role,
  });

  const jwt = strapi.plugins["users-permissions"].services.jwt.issue({
    id: user.id,
  });
  return jwt;
}
async function removeUserAndStudent() {
  const { email } = mockUserData;
  await strapi.services.student.delete({ email });
  await strapi.plugins["users-permissions"].services.user.remove({
    email: email,
  });
}

async function registerAdminAndGetToken() {
  // role 3 is for admin
  const role = 3;
  /** Creates a new user an push to database */
  const user = await strapi.plugins["users-permissions"].services.user.add({
    ...mockAdminData,
    role,
  });

  const jwt = strapi.plugins["users-permissions"].services.jwt.issue({
    id: user.id,
  });
  return jwt;
}
async function removeUserAndAdmin() {
  const { email } = mockAdminData;
  await strapi.services.admin.delete({ email });
  await strapi.plugins["users-permissions"].services.user.remove({
    email: email,
  });
}

module.exports = { registerAndGetToken, removeUserAndStudent, registerAdminAndGetToken, removeUserAndAdmin };
