const request = require("supertest");
const makeGetRequest = ({ url, jwt }) => {
  return request(strapi.server) // app server is an instance of Class: http.Server
    .get(url)
    .set("accept", "application/json")
    .set("Content-Type", "application/json")
    .set("Authorization", "Bearer " + jwt);
};

const makePostRequest = ({ url, jwt, data }) => {
  return request(strapi.server) // app server is an instance of Class: http.Server
    .post(url)
    .set("accept", "application/json")
    .set("Content-Type", "application/json")
    .set("Authorization", "Bearer " + jwt)
    .send(data);
};
module.exports = { makeGetRequest, makePostRequest };
