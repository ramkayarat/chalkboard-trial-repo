const generateDemoData = async () => {
  // Create demo teachers
  const teacherData = {
    name: "teacher name",
    imageUrl: "image Url",
    about: "faculty , A B C School",
  };
  await strapi.services.teachers.create(teacherData);
  // Create demo Course  data
  // Current Subjects

  let courseData = {
    featureImageUrl: "featureImageUrl",
    title: "title",
    description: "description",
    class: "8",
    subject: "Science",
    board: "CBSE",
  };
  await strapi.services.courses.create(courseData);

  // Create demo batches
  const teacher = await strapi.services.teachers.findOne(teacherData);
  const course = await strapi.services.courses.findOne(courseData);
  let weekConfiguration = [];
  for (var i = 0; i < 1; i++) {
    const weekConfigurationData = {
      startTime: "14:00:00.00",
      endTime: "16:00:00.00",
      weekday: "Monday",
    };
    weekConfiguration.push(weekConfigurationData);
  }
  let monthlyPayments = [];
  for (var i = 0; i < 2; i++) {
    const monthlyPaymentsData = {
      amountInPaise: 1000,
      dueDate: `2020-1${2 - i}-09T06:19:29.000Z`,
    };
    monthlyPayments.push(monthlyPaymentsData);
  }
  let batchData = {
    startDate: Date.now(),
    endDate: Date.now(),
    teacher: teacher,
    course: course,
    weekConfiguration: weekConfiguration,
    singlePaymentPriceInPaise: 2000,
    totalSeats: 100,
    seatsBooked: 0,
    monthlyPayments: monthlyPayments,
  };
  await strapi.services.batches.create(batchData);
};

module.exports = { generateDemoData };
