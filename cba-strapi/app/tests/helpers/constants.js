const adminAuth = {
  email: "admin@strapi.io",
  firstname: "admin",
  lastname: "admin",
  password: "Password123",
};

// user mock data
const mockUserData = {
  username: "tester",
  email: "tester@strapi.com",
  provider: "local",
  password: "1234abc",
  confirmed: true,
  blocked: null,
  phoneNumber: "99123456789",
  firstName: "test",
  familyName: "kumar",
};

const mockAdminData = {
  username: "admin",
  email: "admin@strapi.com",
  provider: "local",
  password: "1234abc",
  confirmed: true,
  blocked: null,
};

const updatePermissionData = {
  permissions: {
    application: {
      controllers: {
        "cancel-subscription": {
          cancelsubscription: {
            enabled: true,
          },
        },
        "fulfill-order": {
          fulfillorder: {
            enabled: true,
          },
        },
        "fulfill-pending-payment": {
          fulfillpendingpayment: {
            enabled: true,
          },
        },
        "generate-invoice": {
          generateinvoice: {
            enabled: true,
          },
        },
        "get-invoice": {
          getinvoice: {
            enabled: true,
          },
        },
        "get-student-data": {
          getstudentdata: {
            enabled: true,
          },
        },
        "send-verification-sms": {
          sendverificationsms: {
            enabled: true,
          },
        },
        "update-student-profile": {
          updatestudentprofile: {
            enabled: true,
          },
        },
        "verify-otp": {
          verifyotp: {
            enabled: true,
          },
        },
      },
    },
  },
};

const updatePermissionDataAdmin = {
  permissions: {
    application: {
      controllers: {
        "enroll-student": {
          enrollstudent: {
            enabled: true,
          },
        },
        "mark-added": {
          markadded: {
            enabled: true,
          },
        },
        "mark-cancelled-payment": {
          markcancelledpayment: {
            enabled: true,
          },
        },
        "mark-paid-payment": {
          markpaidpayment: {
            enabled: true,
          },
        },
        "un-enroll-student": {
          unenrollstudent: {
            enabled: true,
          },
        },
        "undo-enroll-student": {
          undoenrollstudent: {
            enabled: true,
          },
        },
      },
    },
  },
};
const adminRoleData = {
  name: "Admin",
  description: "Admin Role",
  ...updatePermissionDataAdmin,

};

module.exports = {
  adminAuth,
  updatePermissionData,
  mockUserData,
  mockAdminData,
  updatePermissionDataAdmin,
  adminRoleData,
};
