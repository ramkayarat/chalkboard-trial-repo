const request = require("supertest");
const {
  adminAuth,
  updatePermissionData,
  adminRoleData,
} = require("./constants");

const createAdmin = async () => {
  await request(strapi.server) // app server is an instance of Class: http.Server
    .post("/admin/register-admin")
    .set("accept", "application/json")
    .set("Content-Type", "application/json")
    .send(adminAuth);
};

const loginAdmin = async () => {
  const { body } = await request(strapi.server) // app server is an instance of Class: http.Server
    .post("/admin/login")
    .set("accept", "application/json")
    .set("Content-Type", "application/json")
    .send({
      email: adminAuth.email,
      password: adminAuth.password,
    });
  return body.data.token;
};

const updateRoleInfo = async (jwt) => {
  const authenticatedId = 1;
  await request(strapi.server) // app server is an instance of Class: http.Server
    .put(`/users-permissions/roles/${authenticatedId}`)
    .set("accept", "application/json")
    .set("Content-Type", "application/json")
    .set("Authorization", "Bearer " + jwt)
    .send(updatePermissionData);
};

const createAdminRole = async () => {
  await strapi.plugins[
    "users-permissions"
  ].services.userspermissions.createRole(adminRoleData);
};
const init = async () => {
  await createAdmin();
  const token = await loginAdmin();
  console.log(`admin token : ${token}`);
  await updateRoleInfo(token);
  const roles = await strapi.plugins[
    "users-permissions"
  ].services.userspermissions.getRoles();
  const adminRole = roles.find((role) => role.name == "Admin");
  if (!adminRole) await createAdminRole();
};

module.exports = {
  init,
};
