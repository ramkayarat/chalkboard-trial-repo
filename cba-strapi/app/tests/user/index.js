const request = require("supertest");

const {
  registerAndGetToken,
  removeUserAndStudent,
} = require("../helpers/auth");
const { mockUserData } = require("../helpers/constants");

describe("Users API", () => {
  it("should login user and return jwt token and student", async (done) => {
    /** Creates a new user and save it to the database */
    await registerAndGetToken();
    await request(strapi.server) // app server is an instance of Class: http.Server
      .post("/auth/local")
      .set("accept", "application/json")
      .set("Content-Type", "application/json")
      .send({
        identifier: mockUserData.email,
        password: mockUserData.password,
      })
      .expect("Content-Type", /json/)
      .expect(200)
      .then((data) => {
        expect(data.body.jwt).toBeDefined();
        expect(data.body.user.student).not.toBeNull();
        expect(data.body.user.student.id).not.toBeNull();
      });
    await removeUserAndStudent();
    done();
  });

  it("should return users data for authenticated user", async (done) => {
    const jwt = await registerAndGetToken();

    await request(strapi.server) // app server is an instance of Class: http.Server
      .get("/users/me")
      .set("accept", "application/json")
      .set("Content-Type", "application/json")
      .set("Authorization", "Bearer " + jwt)
      .expect("Content-Type", /json/)
      .expect(200)
      .then((data) => {
        expect(data.body).toBeDefined();
        expect(data.body.username).toBe(mockUserData.username);
        expect(data.body.email).toBe(mockUserData.email);
      });
    await removeUserAndStudent();
    done();
  });

  it("should signup user and return jwt token and student", async (done) => {
    await request(strapi.server) // app server is an instance of Class: http.Server
      .post("/auth/local/register")
      .set("accept", "application/json")
      .set("Content-Type", "application/json")
      .send(mockUserData)
      .expect("Content-Type", /json/)
      .expect(200)
      .then((data) => {
        expect(data.body.jwt).toBeDefined();
        expect(data.body.user.student).not.toBeNull();
        expect(data.body.user.student.id).not.toBeNull();
      });
    await removeUserAndStudent();
    done();
  });
});
