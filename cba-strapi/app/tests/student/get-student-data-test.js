const {
  registerAndGetToken,
  removeUserAndStudent,
} = require("../helpers/auth");
const { mockUserData } = require("../helpers/constants");
const { makeGetRequest } = require("../helpers/make-request");

describe("get student data API", () => {
  let jwt;
  beforeAll(async () => {
    jwt = await registerAndGetToken();
  });
  afterAll(async () => {
    await removeUserAndStudent();
  });
  it("should able to get student data", async (done) => {
    const res = await makeGetRequest({
      url: "/get-student-data",
      jwt,
    });

    expect(res.statusCode).toBe(200);
    expect(res.body).toMatchObject({
      firstName: mockUserData.firstName,
      familyName: mockUserData.familyName,
      email: mockUserData.email,
      payments: expect.any(Array),
      subscriptions: expect.any(Array),
    });
    done();
  });
});
