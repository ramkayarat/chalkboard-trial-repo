const {
  registerAndGetToken,
  removeUserAndStudent,
} = require("../helpers/auth");
const { mockUserData } = require("../helpers/constants");
const { makePostRequest } = require("../helpers/make-request");

describe("update student profile API", () => {
  let jwt;
  const classData = "8";
  const educationBoard = "CBSE";
  const phoneNumber = "9898989899";
  beforeAll(async () => {
    jwt = await registerAndGetToken();
  });
  afterAll(async () => {
    await removeUserAndStudent();
  });
  it("should get Error if invalid field sended", async (done) => {
    const res = await makePostRequest({
      url: "/update-student-profile",
      jwt,
      data: {
        newData: {
          email: "mynewemial@strapi.com",
        },
      },
    });

    expect(res.statusCode).toBe(400);
    const { message } = JSON.parse(res.text);
    expect(message).toBe("Invalid Field email");
    done();
  });

  it("should able to update student profile", async (done) => {
    const res = await makePostRequest({
      url: "/update-student-profile",
      jwt,
      data: {
        newData: {
          class: classData,
          educationBoard,
          phoneNumber,
        },
      },
    });

    expect(res.statusCode).toBe(200);
    expect(res.text).toBe("success");
    done();
  });

  it("should update the student data", async () => {
    const student = await strapi.services.student.findOne({
      email: mockUserData.email,
    });
    expect(student).toMatchObject({
      class: classData,
      educationBoard: educationBoard,
    });
  });

  it("should generate new verification info for updation of phoneNumber", async () => {
    const student = await strapi.services.student.findOne({
      email: mockUserData.email,
    });
    expect(student).toMatchObject({
      verificationInfo: {
        phoneNumber: phoneNumber,
        isVerified: false,
        verificationTime: null,
      },
    });
  });

  it("should not update the student phone number without otp verification", async () => {
    const student = await strapi.services.student.findOne({
      email: mockUserData.email,
    });
    expect(student.phoneNumber).not.toBe(phoneNumber);
  });
});
