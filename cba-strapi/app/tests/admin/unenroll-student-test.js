const { makePostRequest } = require("../helpers/make-request");
const {
  registerAdminAndGetToken,
  registerAndGetToken,
  removeUserAndAdmin,
  removeUserAndStudent,
} = require("../helpers/auth");
const { mockUserData, mockAdminData } = require("../helpers/constants");

describe("un enroll studentAPI", () => {
  let jwt, noteText, batch, student;
  beforeAll(async () => {
    jwt = await registerAdminAndGetToken();
    await registerAndGetToken();
  });
  afterAll(async () => {
    await removeUserAndAdmin();
    await removeUserAndStudent();
  });
  it("should able to unenroll student", async (done) => {
    // get a random batch to create a task
    batch = await strapi.services.batches.findOne();

    // create a demo tasks
    student = await strapi.services.student.findOne({
      email: mockUserData.email,
    });
    //create demo payments
    payment = await strapi.services.payments.create({
      batchId: batch.id.toString(),
      isLmsAdminFulfilled: true,
      student: student,
      dueDate: Date.now(),
      status: "cancelled",
      amount: 10000,
      pdfUrl: null,
      invoice: null,
    });
    // a unique note text to identify our note
    noteText = Date.now().toString();
    const res = await makePostRequest({
      jwt,
      url: "/un-enroll-student",
      data: {
        note: noteText,
        batchId: batch.id.toString(),
        studentId: student.id,
      },
    });

    expect(res.statusCode).toBe(200);
    expect(res.text).toBe("success");
    done();
  });

  it("should change the lms admin fulfilled status", async (done) => {
    const updatedpayments = await strapi.services.payments.find({
      batchId: batch.id.toString(),
      student: student,
      status: "cancelled",
    });
    updatedpayments.forEach((payment) => {
      expect(payment.isLmsAdminFulfilled).toBe(false);
    });
    done();
  });

  it("should create notes", async (done) => {
    const note = await strapi.services.notes.findOne({
      note: noteText,
    });

    expect(note).toMatchObject({
      time: expect.any(String),
      admin: expect.anything(),
    });
    done();
  });
});
