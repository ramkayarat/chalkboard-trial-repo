const { makePostRequest } = require("../helpers/make-request");
const {
  registerAdminAndGetToken,
  registerAndGetToken,
  removeUserAndAdmin,
  removeUserAndStudent,
} = require("../helpers/auth");
const { mockUserData, mockAdminData } = require("../helpers/constants");

describe("mark cancelled payment API", () => {
  let jwt, admin, noteText, batch, student;
  beforeAll(async () => {
    jwt = await registerAdminAndGetToken();
    await registerAndGetToken();
    admin = await strapi
      .query("admins")
      .findOne({ email: mockAdminData.email });
  });
  afterAll(async () => {
    await removeUserAndAdmin();
    await removeUserAndStudent();
  });
  it("should able to mark payment cancelled", async (done) => {
    // get a random batch to create a task
    batch = await strapi.services.batches.findOne();

    // create a demo tasks
    student = await strapi.services.student.findOne({
      email: mockUserData.email,
    });
    //create demo payments
    payment = await strapi.services.payments.create({
      batchId: batch.id.toString(),
      isLmsAdminFulfilled: false,
      student: student,
      dueDate: Date.now(),
      status: "pending",
      amount: 10000,
      pdfUrl: null,
      invoice: null,
    });
    // a unique note text to identify our note
    noteText = Date.now().toString();
    const res = await makePostRequest({
      jwt,
      url: "/mark-cancelled-payment",
      data: {
        note: noteText,
        batchId: batch.id.toString(),
        studentId: student.id,
      },
    });

    expect(res.statusCode).toBe(200);
    expect(res.text).toBe("success");
    done();
  });

  it("should change the payment status to cancelled", async (done) => {
    const updatedpayments = await strapi.services.payments.find({
      batchId: batch.id.toString(),
      student: student.id,
    });
    updatedpayments.forEach((payment) => {
      expect(payment.status).toBe("cancelled");
      expect(payment.isLmsAdminFulfilled).toBe(false);
    });
    done();
  });

  it("should create notes", async (done) => {
    const note = await strapi.services.notes.findOne({
      note: noteText,
    });

    expect(note).toMatchObject({
      time: expect.any(String),
      admin: expect.anything(),
    });
    done();
  });
});
