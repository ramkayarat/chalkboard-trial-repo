const { makePostRequest } = require("../helpers/make-request");
const {
  registerAdminAndGetToken,
  registerAndGetToken,
  removeUserAndAdmin,
  removeUserAndStudent,
} = require("../helpers/auth");
const { mockUserData, mockAdminData } = require("../helpers/constants");

describe("enroll student API", () => {
  let jwt, task, admin, noteText, batch;
  beforeAll(async () => {
    jwt = await registerAdminAndGetToken();
    await registerAndGetToken();
    admin = await strapi
      .query("admins")
      .findOne({ email: mockAdminData.email });
  });
  afterAll(async () => {
    await removeUserAndAdmin();
    await removeUserAndStudent();
  });
  it("should able to enroll student", async (done) => {
    // get a random batch to create a task
    batch = await strapi.services.batches.findOne();

    // create a demo tasks
    const student = await strapi.services.student.findOne({
      email: mockUserData.email,
    });
    //create demo payments
    await strapi.services.payments.create({
      batchId: batch.id.toString(),
      isLmsAdminFulfilled: false,
      student: student,
      dueDate: Date.now(),
      status: "paid",
      amount: 10000,
      pdfUrl: null,
      invoice: null,
    });
    const studentData = {
      studentId: student.id,
      name: student.firstName + " " + student.familyName,
      email: student.email,
      phoneNumber: student.phoneNumber,
    };
    const courseData = {
      ...studentData,
      batchId: batch.id,
      course: batch.course,
    };
    const enrollStudentTaskData = {
      type: "enroll_student",
      isDone: false,
      creationTime: Date.now(),
      data: courseData,
    };
    task = await strapi.services.tasks.create(enrollStudentTaskData);

    // a unique note text to identify our note
    noteText = Date.now().toString();
    const res = await makePostRequest({
      jwt,
      url: "/enroll-student",
      data: {
        note: noteText,
        taskId: task.id.toString(),
      },
    });

    expect(res.statusCode).toBe(200);
    expect(res.text).toBe("success");

    done();
  });

  it("should update the task to done", async (done) => {
    const updateTask = await strapi.services.tasks.findOne({ id: task.id });
    expect(updateTask).toMatchObject({
      isDone: true,
      completionTime: expect.any(String),
      doneBy: expect.anything(),
    });
    done();
  });

  it("should create notes", async (done) => {
    const note = await strapi.services.notes.findOne({
      note: noteText,
    });

    expect(note).toMatchObject({
      time: expect.any(String),
      admin: expect.anything(),
    });
    done();
  });
});
