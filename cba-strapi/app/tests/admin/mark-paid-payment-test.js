const { makePostRequest } = require("../helpers/make-request");
const {
  registerAdminAndGetToken,
  registerAndGetToken,
  removeUserAndAdmin,
  removeUserAndStudent,
} = require("../helpers/auth");
const { mockUserData, mockAdminData } = require("../helpers/constants");

describe("mark paid payment API", () => {
  let jwt, admin, noteText, batch;
  beforeAll(async () => {
    jwt = await registerAdminAndGetToken();
    await registerAndGetToken();
    admin = await strapi
      .query("admins")
      .findOne({ email: mockAdminData.email });
  });
  afterAll(async () => {
    await removeUserAndAdmin();
    await removeUserAndStudent();
  });
  it("should able to mark payment paid", async (done) => {
    // get a random batch to create a task
    batch = await strapi.services.batches.findOne();

    // create a demo tasks
    const student = await strapi.services.student.findOne({
      email: mockUserData.email,
    });
    //create demo payments
    payment = await strapi.services.payments.create({
      batchId: batch.id.toString(),
      isLmsAdminFulfilled: false,
      student: student,
      dueDate: Date.now(),
      status: "pending",
      amount: 10000,
      pdfUrl: null,
      invoice: null,
    });
    // a unique note text to identify our note
    noteText = Date.now().toString();
    const res = await makePostRequest({
      jwt,
      url: "/mark-paid-payment",
      data: {
        note: noteText,
        paymentId: payment.id.toString(),
      },
    });

    expect(res.statusCode).toBe(200);
    expect(res.text).toBe("success");

    done();
  });

  it("should change the payment status to paid", async (done) => {
    const updatedpayment = await strapi.services.payments.findOne({
      id: payment.id,
    });
    expect(updatedpayment.status).toBe("paid");
    done();
  });

  it("should create notes", async (done) => {
    const note = await strapi.services.notes.findOne({
      note: noteText,
    });

    expect(note).toMatchObject({
      time: expect.any(String),
      admin: expect.anything(),
    });
    done();
  });
});
