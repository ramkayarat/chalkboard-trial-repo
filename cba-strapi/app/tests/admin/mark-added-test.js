const { makePostRequest } = require("../helpers/make-request");
const {
  registerAdminAndGetToken,
  registerAndGetToken,
  removeUserAndAdmin,
  removeUserAndStudent,
} = require("../helpers/auth");
const { mockUserData, mockAdminData } = require("../helpers/constants");

describe("mark added API", () => {
  let jwt, task, admin, noteText;
  beforeAll(async () => {
    jwt = await registerAdminAndGetToken();
    await registerAndGetToken();
    admin = await strapi
      .query("admins")
      .findOne({ email: mockAdminData.email });
  });
  afterAll(async () => {
    await removeUserAndAdmin();
    await removeUserAndStudent();
  });
  it("should able to mark added", async (done) => {
    // create a demo tasks
    const student = await strapi.services.student.findOne({
      email: mockUserData.email,
    });
    const studentData = {
      name: student.firstName + " " + student.familyName,
      email: student.email,
      phoneNumber: student.phoneNumber,
      studentId: student.id,
    };
    const addStudentTaskData = {
      type: "add_student",
      isDone: false,
      creationTime: Date.now(),
      data: studentData,
    };
    task = await strapi.services.tasks.create(addStudentTaskData);
    // a unique note text to identify our note
    noteText = Date.now().toString();
    const res = await makePostRequest({
      jwt,
      url: "/mark-added",
      data: {
        note: noteText,
        taskId: task.id,
        studentId: student.id,
      },
    });
    expect(res.statusCode).toBe(200);
    expect(res.text).toBe("success");

    done();
  });

  it("should update the task to done", async (done) => {
    const updateTask = await strapi.services.tasks.findOne({ id: task.id });
    console.log(updateTask);
    expect(updateTask).toMatchObject({
      isDone: true,
      completionTime: expect.any(String),
      doneBy: expect.anything(),
    });
    done();
  });

  it("should update student to notify isAdded", async (done) => {
    const updateStudent = await strapi.services.student.findOne({
      email: mockUserData.email,
    });
    expect(updateStudent.isAdded).toBe(true);
    done();
  });

  it("should create notes", async (done) => {
    const note = await strapi.query("notes").findOne({ note: noteText });
    expect(note).toMatchObject({
      time: expect.any(String),
      admin: expect.anything(),
    });
    done();
  });
});
