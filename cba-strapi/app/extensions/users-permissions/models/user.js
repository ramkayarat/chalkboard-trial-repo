module.exports = {
  lifecycles: {
    //this will create a new student with every new user and assign it to the user
    async beforeCreate(data) {
      console.log("creating user");

      // logging the details
      console.log("data :");
      console.log(data);
      // create admin if admin is registered
      if (data.role == 3) {
        // role 3 means Admin
        const adminData = {
          email: data.email,
          name: data.userName,
        };
        await strapi.services.admins.create(adminData);
        return;
      }
      // we are not verifying phone number
      // //generate data for student and send sms
      // const verificationInfo = strapi.services[
      //   "verification-service"
      // ].generateVerificationInfo(data.phoneNumber);
      // console.log("verification info : ");
      // console.log(verificationInfo);

      //create student
      var studentData = {
        email: data.email,
        firstName: data.firstName,
        familyName: data.familyName,
        // verificationInfo: verificationInfo,
        phoneNumber: data.phoneNumber,
        subscriptions: [],
        payments: [],
      };
      const student = await strapi.services.student.create(studentData);

      //update the student in user
      data.student = student;
    },
  },
};
