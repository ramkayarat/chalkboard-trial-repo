"use strict";

const AWS = require("aws-sdk");

const upload = (params) => {
  AWS.config.update({
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
  });

  var s3 = new AWS.S3();
  return new Promise((resolve, reject) => {
    s3.upload(params, function (err, res) {
      if (err) {
        reject(err);
      }
      resolve(res);
    });
  });
};

module.exports = {
  // The name of the bucket that you have created

  uploadToAwsS3: async (body, filename) => {
    var params = {
      Body: body,
      Bucket: process.env.BUCKET_NAME,
      Key: filename,
      ContentType: "application/pdf",
    };
    console.log({ params });
    const response = await upload(params);
    const pdfUrl = response.Location;
    return pdfUrl;
  },
};
