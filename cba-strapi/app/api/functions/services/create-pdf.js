"use strict";
// const pdf = require("pdf-creator-node");
const fs = require("fs");
const pdf = require("html-pdf");
var Handlebars = require("handlebars");

const createPdf = (html, options) => {
  return new Promise((resolve, reject) => {
    pdf.create(html, options).toStream(async function (err, stream) {
      if (err) {
        return reject(error);
      }
      resolve(stream);
    });
  });
};

module.exports = {
  createPdf: async (template, fileName, data) => {
    console.log({ template });
    const isExists = fs.existsSync(template);
    console.log({ isExists });
    if (!isExists) {
      throw new Error(" Template file not exists");
    }
    const baseHtml = fs.readFileSync(template, "utf8");
    const html = Handlebars.compile(baseHtml)(data);
    const options = {
      format: "A4",
      orientation: "portrait",
    };
    const stream = await createPdf(html, options);
    // // for testing locally
    // stream.pipe(fs.createWriteStream("./foo.pdf"));
    // return "hello";
    const pdfUrl = await strapi.services["upload-to-awss3"].uploadToAwsS3(
      stream,
      fileName
    );
    return pdfUrl;
  },
};
