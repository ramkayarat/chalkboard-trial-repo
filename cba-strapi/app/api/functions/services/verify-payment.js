"use strict";

const crypto = require("crypto");
const request = require("request");

const postRequest = (options) => {
  return new Promise((resolve, reject) => {
    request(options)
      .on("response", function (resp) {
        console.log("STATUS:" + resp.statusCode);
        resp.setEncoding("utf8");
        resp.on("data", function (chunk) {
          const vdata = JSON.parse(chunk);
          resolve(vdata);
        });
      })
      .on("error", function (err) {
        reject(err);
      });
  });
};

const getData = (response) => {
  return new Promise((resolve, reject) => {
    response
      .on("data", function (data) {
        resolve(data);
      })
      .on("error", function (err) {
        reject(err);
      });
  });
};

module.exports = {
  verifyPayment: async ({
    responseHash,
    status,
    txnId,
    email,
    amount,
    productInfo,
    firstName,
    additionalCharges,
    mihpayid,
    udf1=""
  }) => {
    //verify payment
    //get the keys from environment
    const payuSalt = process.env.PAYU_SALT;
    const payuKey = process.env.PAYU_KEY;
    console.log({ payuSalt, payuKey });

    //validateHash
    const validateHash = await strapi.services["payu"].validateHash(
      responseHash,
      {
        key: payuKey,
        salt: payuSalt,
        txnid: txnId,
        amount: amount,
        productinfo: productInfo,
        firstname: firstName,
        email: email,
        status: status,
        additionalCharges,
        udf1:udf1
      }
    );
    console.log({ validateHash });
    if (!validateHash) {
      return false;
    }

    //Verify Payment routine to double check payment
    var command = "verify_payment";
    var hash_str = payuKey + "|" + command + "|" + txnId + "|" + payuSalt;
    var vcryp = crypto.createHash("sha512");
    vcryp.update(hash_str);
    var vhash = vcryp.digest("hex");
    const verifyPaymentUrl = process.env.VERIFY_PAYMENT_URL;
    var options = {
      method: "POST",
      uri: verifyPaymentUrl,
      form: {
        key: payuKey,
        hash: vhash,
        var1: txnId,
        command: command,
      },
      headers: {
        /* 'content-type': 'application/x-www-form-urlencoded' */
        // Is set automatically
      },
    };
    console.log({ options });
    try {
      const vdata =  await postRequest(options);
      console.log({vdata});
      if (vdata.status == "1") {
        const details = vdata.transaction_details[txnId];
        console.log(details["status"] + "   " + details["mihpayid"]);
        if (details["mihpayid"] == mihpayid) {
          if (details["status"] != "success") {
            return false;
          }
        } else {
          console.log("mihpayid does not matched");
          return false;
        }
        console.log("returned true");
      }
    } catch (e) {
      return false;
    }
    return true;
  },
};
