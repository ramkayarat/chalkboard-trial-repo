"use strict";
function _generateOtp() {
  //generate number between 1 to 9
  const firstDigit = Math.floor(Math.random() * 9) + 1;
  //generate number between 0 to 99999
  const remainingDigits = Math.floor(Math.random() * 100000);
  //generate otp and return
  var otp = firstDigit * 100000 + remainingDigits;
  return otp;
}

function _generateMessage(otp) {
  return `Your otp for cba learning is ${otp}`;
}

module.exports = {
  generateVerificationInfo: (phoneNumber) => {
    // Generate a message and otp
    var otp = _generateOtp();
    console.log(`otp : ${otp}`);
    var message = _generateMessage(otp);
    console.log(`message : ${message}`);
    var verificationInfo = {
      generatedTime: Date.now(),
      otp: otp,
      message: message,
      isVerified: false,
      verificationTime: null,
      phoneNumber: phoneNumber,
    };
    //send sms
    strapi.services["notification-service"].sendSms({
      to: phoneNumber,
      text: message,
    });

    return verificationInfo;
  },
};
