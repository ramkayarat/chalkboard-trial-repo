"use strict";

module.exports = {
  async sendVerificationSms(ctx) {
    console.log("triggered sendVerificationSms");

    //fetch the data provided by frontend
    const { user } = ctx.state;
    const { phoneNumber } = ctx.request.body;

    //log the related data
    console.log({ ctx });
    console.log({ user });
    console.log({ phoneNumber });

    //check for required data
    ctx.assert(phoneNumber, 400, "phoneNumber required");

    //generateVerificationInfo
    const verificationInfo = strapi.services[
      "verification-service"
    ].generateVerificationInfo(phoneNumber);

    //update the student
    var updatedStudent = await strapi.query("student").update(
      { id: user.student },
      {
        verificationInfo: verificationInfo,
      }
    );
    console.log({ updatedStudent });

    //send a success reponse
    ctx.send("success");
  },
};
