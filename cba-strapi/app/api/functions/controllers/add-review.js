"use strict";

// we will use this in future

module.exports = {
  async addReview(ctx) {
    console.log("triggered addReview");

    //check for required data
    ctx.assert(ctx.request.body.text, 400, "text required");
    ctx.assert(ctx.request.body.courseId, 400, "courseId required");
    //fetch the  data sent by fontend
    var user = ctx.state.user;
    var text = ctx.request.body.text;
    var courseId = ctx.request.body.courseId;

    //log the related data
    console.log("context object : ");
    console.log(ctx);
    console.log("user : ");
    console.log(user);
    console.log(`courseid : ${courseId}`);
    console.log(`text : ${text}`);

    const student = await strapi.services.student.findStudent(user.student);
    const name = `${student.firstName} ${student.familyName}`;
    const reviewData = {
      text: text,
      course: courseId,
      name: name,
      student: user.student
    }
    await strapi.services.reviews.create(reviewData);
    //send successfull message
    ctx.send("success");
  },
};
