"use strict";
var numberToWords = require("number-to-words");
module.exports = {
  async getInvoice(ctx) {
    console.log("triggered getInvoice");

    //fetch the data provided by frontend
    const { user } = ctx.state;
    const { paymentId } = ctx.request.body;

    //log the related data
    console.log({ ctx });
    console.log({ user });
    console.log({ paymentId });

    //check for required data
    ctx.assert(paymentId, 400, "paymentId required");

    //check if invoice user is the requested user
    const payment = await strapi.query("payments").findOne({ id: paymentId });
    const batch = await strapi.query("batches").findOne({ id: payment.batchId });
    console.log({ payment });
    if (payment.student.id != user.student) {
      ctx.throw(401, "The payment does not belongs to the authenticated user");
    }

    // if user has already generated the pdf
    // it will be better if frontend display the pdf if present no need to call the api
    // if (payment.pdfUrl != null) {
    //   return ctx.send(payment.pdfUrl);
    // }

    const invoicePdfName = `invoice_${payment.invoice.txnId}.pdf`;
    const productInfo = payment.invoice.productInfo;
    const paymentType = productInfo.includes("one_time")
      ? "One Time"
      : "Monthly";
      const date = new Date(payment.invoice.timePaid);
      const formatedDate =date.toUTCString();
    let pdfUrl;
    try {
      pdfUrl = await strapi.services["create-pdf"].createPdf(
        "./invoice_template/invoice_template.html",
        invoicePdfName,
        {
          course_name: batch.course.title,
          price: payment.amount / 100,
          total: payment.amount / 100,
          invoice_number: payment.invoice.txnId,
          date: formatedDate,
          price_in_text: numberToWords.toWords(payment.amount / 100),
          payment_type: paymentType,
        }
      );
      if (pdfUrl == null || pdfUrl == undefined)
        ctx.throw(400, "Generted pdf url is null or undefined");
    } catch (e) {
      console.log("Error in creating pdf");
      console.error(e);
      ctx.throw(400, `Error in creating pdf ${e}`);
    }

    //updated the pdfUrl in payment
    var updatedPayment = await strapi.query("payments").update(
      { id: payment.id },
      {
        pdfUrl: pdfUrl,
      }
    );
    console.log({ updatedPayment });

    //return invoice
    ctx.send(pdfUrl);
  },
};
