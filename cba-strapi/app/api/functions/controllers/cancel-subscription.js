"use strict";

module.exports = {
  async cancelSubscription(ctx) {
    console.log("triggered cancelSubscription");

    //check for required data
    ctx.assert(ctx.request.body.batchId, 400, "batchId required");

    //fetch the data provided by frontend
    const user = ctx.state.user;
    const batchId = ctx.request.body.batchId;

    //log the related data
    console.log("context object : ");
    console.log(ctx);
    console.log("user : ");
    console.log(user);
    console.log(`batchId : ${batchId}`);
    
    // counting the number of pending payments of given batchId
    const count = await strapi.query("payments").count(
      { batchId: batchId, status: "pending", student: user.student });
    console.log(count);

   for(var i=0;i< count;i++)
   {
     // mark all the pending payments with the batchid paid
     await strapi.services.payments.update(
       { batchId: batchId, status: "pending", student: user.student },
       {
         status: "cancelled",
       }
     );
   }
    //send a success response
    ctx.send("success");
  },
};
