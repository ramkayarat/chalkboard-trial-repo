"use strict";

module.exports = {
  async getStudentData(ctx) {
    console.log("triggered getStudentData");

    //fetch the data provided by frontend
    const { user } = ctx.state;

    //log the related data
    console.log({ ctx });
    console.log({ user });

    //fetch student data
    const student = await strapi.services.student.findStudent(user.student);

    //send the student data
    ctx.send(student);
  },
};
