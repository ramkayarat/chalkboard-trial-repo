"use strict";

module.exports = {
  // this will be called by payu after payment is done
  async fulfillOrder(ctx) {
    console.log("triggered fulfillOrder");

    console.log(ctx.request.body);

    //fetch the data provided by payu
    const {
      hash: responseHash,
      status,
      txnid: txnId,
      email,
      amount,
      productinfo: productInfo,
      firstname: firstName,
      additionalCharges,
      mihpayid,
    } = ctx.request.body;

    //log the  data
    console.log({
      responseHash,
      status,
      txnId,
      email,
      amount,
      productInfo,
      firstName,
      additionalCharges,
      mihpayid,
    });

    // get the redirection urls from environment
    const successUrl = process.env.PAYMENT_SUCCESS_URL;

    // fetch the invoice and student
    const invoice = await strapi.query("invoices").findOne({ txnId });
    if (invoice.isPaid) {
      //redirect to success page if already paid
      return ctx.redirect(successUrl);
    }
    if (!invoice) {
      console.log("no invoice found for given txnId");
      return ctx.redirect(failedUrl);
    }
    const student = await strapi.services.student.findOne({ email });
    if (!student) {
      console.log("no student found for given email");
      return ctx.redirect(failedUrl);
    }

    //double verify the payment with verifying hash and payu server
    const isPaymentVerified = await strapi.services[
      "verify-payment"
    ].verifyPayment({
      responseHash,
      status,
      txnId,
      email,
      amount,
      productInfo,
      firstName,
      additionalCharges,
      mihpayid,
    });
    if (!isPaymentVerified) return ctx.redirect(failedUrl);

    //update the invoice
    let updatedInvoice = await strapi.query("invoices").update(
      { txnId },
      {
        isPaid: true,
        timePaid: Date.now(),
      }
    );
    console.log({ updatedInvoice });

    //fetch the batch for info
    let payments = student.payments;
    const batchId = productInfo.split("_")[0];
    console.log({ batchId });
    const batch = await strapi.query("batches").findOne({ id: batchId });
    const failedUrl = `${process.env.PAYMENT_FAILED_URL}&slug=${batch.slug}`;
    console.log({ batch });
    if (productInfo.includes("one_time")) {
      //create a new payment from the invoice()
      const payment = await strapi.services.payments.create({
        batchId,
        amount: invoice.amount,
        dueDate: Date.now(),
        status: "paid",
        student: student,
        invoice: invoice.id,
      });
      console.log({ payment });
      payments.push(payment);
    } else if (productInfo.includes("monthly")) {
      // sorting the monthly payments by date to avoid mistakes
      let monthlyPayments = batch.monthlyPayments;
      monthlyPayments.sort(
        (a, b) => Date.parse(a.dueDate) - Date.parse(b.dueDate)
      );

      //create a new payment from the invoice
      const payment = await strapi.services.payments.create({
        batchId: batch.id.toString(),
        amount: invoice.amount,
        dueDate: monthlyPayments[0].dueDate,
        status: "paid",
        studentId: student.id,
        invoice: invoice.id,
      });
      console.log(`created payment ${payment}`);

      //add this to the student
      payments.push(payment);
      for (var i = 1; i < monthlyPayments.length; i++) {
        //create a new payment from the invoice
        var duePayment = await strapi.services.payments.create({
          batchId: batch.id.toString(),
          amount: monthlyPayments[i].amountInPaise,
          dueDate: monthlyPayments[i].dueDate,
          status: "pending",
          studentId: student.id,
        });
        console.log(`added payment ${duePayment}`);

        //add this to the student
        payments.push(duePayment);
      }
    } else {
      return ctx.redirect(failedUrl);
    }

    // update the subscriptions
    var subscriptions = student.subscriptions;
    subscriptions.push({ batchId: batch.id });
    await strapi.query("student").update(
      { email },
      {
        subscriptions: subscriptions,
        payments: payments,
      }
    );

    // increase the booked number
    await strapi.query("batches").update(
      { id: batch.id },
      {
        seatsBooked: batch.seatsBooked + 1,
      }
    );

    // add tasks for add student
    const studentData = {
      name: student.firstName + " " + student.familyName,
      studentId: student.id,
      email: student.email,
      phoneNumber: student.phoneNumber,
    };
    if (!student.isAdded) {
      // add the tasks for lms admin
      const addStudentTaskData = {
        type: "add_student",
        isDone: false,
        creationTime: Date.now(),
        data: studentData,
      };
      await strapi.services.tasks.create(addStudentTaskData);
    }

    // add tasks for enroll student in course
    const courseData = {
      ...studentData,
      batchId: batch.id,
      course: batch.course,
    };
    const enrollStudentTaskData = {
      type: "enroll_student",
      isDone: false,
      creationTime: Date.now(),
      data: courseData,
    };
    await strapi.services.tasks.create(enrollStudentTaskData);

    //redirect to success page
    return ctx.redirect(successUrl);
  },
};
