"use strict";

var crypto = require("crypto");

module.exports = {
  async fulfillPendingPayment(ctx) {
    console.log("triggered fulfillPendingOrder");

    
    console.log(ctx.request.body);

    //fetch the data provided by payu
    const {
      hash: responseHash,
      status,
      txnid: txnId,
      email,
      amount,
      productinfo: productInfo,
      firstname: firstName,
      additionalCharges,
      mihpayid,
      udf1:paymentId
    } = ctx.request.body;

    //log the  data
    console.log({
      responseHash,
      status,
      txnId,
      email,
      amount,
      productInfo,
      firstName,
      additionalCharges,
      mihpayid,
    });

    // get the redirection urls from environment
    const successUrl = process.env.PAYMENT_SUCCESS_URL;

    //fetch the payment
    let payment = await strapi.query("payments").findOne({ id: paymentId });
    console.log({ payment });
    const batch = await strapi.query("batches").findOne({ id: payment.batchId });
    const failedUrl = `${process.env.PAYMENT_FAILED_URL}&slug=${batch.slug}`;
    // get the invoice
    const invoice = await strapi.query("invoices").findOne({ txnId: txnId });

    //double verify the payment with verifying hash and payu server
    const isPaymentVerified = await strapi.services[
      "verify-payment"
    ].verifyPayment({
      responseHash,
      status,
      txnId,
      email,
      amount,
      productInfo,
      firstName,
      additionalCharges,
      mihpayid,
      udf1:paymentId
    });
    if (!isPaymentVerified) return ctx.redirect(failedUrl);

    // update the invoice to paid
    await strapi.query("invoices").update(
      { txnId: invoice.txnId },
      {
        isPaid: true,
        timePaid: Date.now(),
      }
    );
    //mark the payment paid
    await strapi.query("payments").update(
      { id: paymentId },
      {
        status: "paid",
        invoice: invoice.id,
      }
    );

       //redirect to success page
       return ctx.redirect(successUrl);
  },
};
