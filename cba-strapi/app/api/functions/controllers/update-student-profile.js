"use strict";

module.exports = {
  async updateStudentProfile(ctx) {
    console.log("triggered updateStudentProfile");

    //fetch the data provided by frontend
    const { user } = ctx.state;
    const { newData } = ctx.request.body;

    //log the related data
    console.log({ ctx });
    console.log({ user });
    console.log({ newData });

    //check for required data
    ctx.assert(newData, 400, "newData required");

    const allowedFields = [
      "firstName",
      "familyName",
      "phoneNumber",
      "educationBoard",
      "class",
    ];

    // check for invalid fields
    for (const property in newData) {
      if (!allowedFields.includes(property))
        ctx.throw(400, `Invalid Field ${property}`);
    }

    //send otp if user want to update phoneNumber
    const updatedData = newData;
    // we are not verifying phone number
    // const fields = Object.keys(newData);
    // if (fields.includes("phoneNumber")) {
    //   const verificationInfo = strapi.services[
    //     "verification-service"
    //   ].generateVerificationInfo(newData.phoneNumber);

    //   console.log({ verificationInfo });

    //   //add the verfication info to verify
    //   updatedData.verificationInfo = verificationInfo;

    //   //remove phoneNumber as it will update the phone Number without verification
    //   delete updatedData.phoneNumber;
    // }
    //update the student profile
    var updatedStudent = await strapi
      .query("student")
      .update({ id: user.student }, updatedData);

    console.log({ updatedStudent });

    //send successfull message
    ctx.send("success");
  },
};
