"use strict";

const { v4: uuidv4 } = require("uuid");
var crypto = require("crypto");

module.exports = {
  // whenever user want to do a payment it will called
  async generatePendingPaymentInvoice(ctx) {
    console.log("triggered generatePendingPaymentInvoice");

    //fetch the data provided by frontend
    const { user } = ctx.state;
    const { paymentId } = ctx.request.body;

    //log the related data
    console.log({ ctx });
    console.log({ paymentId });

    //check for required data
    ctx.assert(paymentId, 400, "paymentId required");

    const student = await strapi.services.student.findStudent(user.student);

    let payment = await strapi.query("payments").findOne({ id: paymentId });
    const batchId = payment.batchId;
    const batch = await strapi.query("batches").findOne({ id: batchId });
    let amount = payment.amount;

    //Generate Invoice
    const currentTime = Date.now();
    const transactionId = uuidv4();

    // transactionId id can be 25 character long
    const invoice = {
      txnId: transactionId.slice(0, 25),
      timeCreated: currentTime,
      timePaid: null,
      amount: amount,
      productInfo: `${batch.id}_monthly`,
      studentId: student.id.toString(),
      firstName: student.firstName,
      email: student.email,
      phoneNumber: student.phoneNumber,
    };

    //get the keys from environment
    var payuKey = process.env.PAYU_KEY;
    var payuSalt = process.env.PAYU_SALT;
    console.log({ payuKey, payuSalt });
    const hash = await strapi.services["payu"].generateHash({
      key: payuKey,
      salt: payuSalt,
      txnid: invoice.txnId,
      // the amount for which user will payment will be original not in paise
      amount: (invoice.amount / 100).toString(),
      productinfo: invoice.productInfo,
      firstname: invoice.firstName,
      email: invoice.email,
      udf1:paymentId.toString()
    });

    //update invoice
    invoice.hash = hash;
    invoice.key = payuKey;
    invoice.isPaid = false;
    await strapi.services.invoices.create(invoice);
    console.log(invoice);
    ctx.send(invoice);
  },
};
