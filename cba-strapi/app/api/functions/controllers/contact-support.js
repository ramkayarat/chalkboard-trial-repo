"use strict";

module.exports = {
  async contactSupport(ctx) {
    console.log("triggered contactSupport");

    //fetch the data provided by frontend
    const { user } = ctx.state;
    const { email, message, name } = ctx.request.body;

    //log the related data
    console.log({ ctx });
    console.log({ user });
    console.log({ email, message, name });
    //check for required data
    ctx.assert(email, 400, "email required");
    ctx.assert(message, 400, "message required");
    ctx.assert(name, 400, "name required");

    await strapi.plugins["email"].services.email.send({
      to: process.env.SUPPORT_EMAIL_ID,
      replyTo: email,
      subject: "Use strapi email provider successfully",
      text: ` i am ${name} and i need support ${message}`,
      html: ` i am ${name} and i need support ${message}`,
    });
    //send a success response
    ctx.send("success");
  },
};
