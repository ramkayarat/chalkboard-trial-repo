"use strict";

module.exports = {
  async verifyOtp(ctx) {
    console.log("triggered  verifyOtp");

    //fetch the data provided by frontend
    const { user } = ctx.state;
    const { otp } = ctx.request.body;

    //log the related data
    console.log({ ctx });
    console.log({ user });
    console.log({ otp });

    //check for required data
    ctx.assert(otp, 400, "otp required");

    // check for invalid data
    if (otp.toString().length != 6) ctx.throw(400, "otp length is not 6");

    //fetch the verification info to verify OTP
    const { verificationInfo } = await strapi.services.student.findStudent(
      user.student
    );
    console.log({ verificationInfo });

    // throw error if incorrect otp
    if (otp != verificationInfo.otp) ctx.throw(400, "Incorrect otp");

    //otp is verified update student data
    verificationInfo.isVerified = true;
    verificationInfo.verificationTime = Date.now();
    var updatedStudent = await strapi.query("student").update(
      { id: user.student },
      {
        verificationInfo: verificationInfo,
        phoneNumber: verificationInfo.phoneNumber,
      }
    );
    console.log({ updatedStudent });

    //send a success response
    ctx.send("success");
  },
};
