"use strict";

const { v4: uuidv4 } = require("uuid");

module.exports = {
  // whenever user want to do a payment it will called
  async generateInvoice(ctx) {
    console.log("triggered generateInvoice");

    //fetch the data provided by frontend
    const { user } = ctx.state;
    const { batchId, paymentPlan } = ctx.request.body;

    //log the related data
    console.log({ ctx });
    console.log({ batchId, paymentPlan });

    //check for required data
    ctx.assert(batchId, 400, "batchId required");
    ctx.assert(paymentPlan, 400, "paymentPlan required");

    //find tge batch
    const batch = await strapi.query("batches").findOne({ id: batchId });
    // can't genertae invoice if no seats available
    if (batch.seatsBooked + 1 > batch.totalSeats)
      ctx.throw(400, " No seats available");
    const student = await strapi.services.student.findStudent(user.student);

    //calculate the amountaccording to the payment plan
    let amount;
    if (paymentPlan == "one_time") {
      amount = batch.singlePaymentPriceInPaise;
    } else if (paymentPlan == "monthly") {
      let monthlyPayments = batch.monthlyPayments;
      monthlyPayments.sort(
        (a, b) => Date.parse(a.dueDate) - Date.parse(b.dueDate)
      );
      amount = monthlyPayments[0].amountInPaise;
    } else {
      ctx.throw(401, "invalid paymentPlan");
    }

    //Generate Invoice
    const currentTime = Date.now();
    const transactionId = uuidv4();
    const invoice = {
      txnId: transactionId.slice(0, 25),
      timeCreated: currentTime,
      timePaid: null,
      amount: amount,
      productInfo: `${batch.id}_${paymentPlan}`,
      studentId: student.id.toString(),
      firstName: student.firstName,
      email: student.email,
      phoneNumber: student.phoneNumber,
    };

    //get the keys from environment
    var payuKey = process.env.PAYU_KEY;
    var payuSalt = process.env.PAYU_SALT;
    console.log({ payuKey, payuSalt });
    const hash = await strapi.services["payu"].generateHash({
      key: payuKey,
      salt: payuSalt,
      txnid: invoice.txnId,
      // the amount for which user will payment will be original not in paise
      amount: (invoice.amount / 100).toString(),
      productinfo: invoice.productInfo,
      firstname: invoice.firstName,
      email: invoice.email,
    });
    console.log(`hash : ${hash}`);

    //update invoice
    invoice.hash = hash;
    invoice.key = payuKey;
    invoice.isPaid = false;
    await strapi.services.invoices.create(invoice);
    console.log(invoice);
    ctx.send(invoice);
  },
};
