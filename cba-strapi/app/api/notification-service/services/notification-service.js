"use strict";
/**
 * `notification-service` service.
 */

module.exports = {
  //api for sending SMS
  sendSms: ({ from, to, text }) => {
    const defaultNumber = process.env.DEFAULT_NUMBER;
    if (from == undefined) from = defaultNumber;
    return console.log(`Sending SMS ${from} to ${to} with text ${text}`);
  },
};
