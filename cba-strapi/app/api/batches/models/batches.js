const slugify = require("slugify");

module.exports = {
  lifecycles: {
    //this will create a new slug when a batch is created or updated
    async beforeCreate(data) {
      console.log("beforeCreate batch");
      console.log(data);
      if (data.course && data.teacher && data.startDate) {
        const date = new Date(data.startDate);
        const formatedDate = date.toDateString();
        const course = await strapi
          .query("courses")
          .findOne({ id: data.course });
        console.log({ course });
        const teacher = await strapi
          .query("teachers")
          .findOne({ id: data.teacher });
        console.log({ teacher });
        const slugString = `${course.title} ${formatedDate} ${teacher.name}`;
        const slug = slugify(slugString, { lower: true });
        console.log({ slug });
        data.slug = slug;
      }
    },
    async beforeUpdate(params, data) {
      console.log("beforeUpdate batch");
      console.log({ data });
      console.log({ params });
      if (data.course && data.teacher && data.startDate) {
        const date = new Date(data.startDate);
        const formatedDate = date.toDateString();
        const course = await strapi
          .query("courses")
          .findOne({ id: data.course });
        console.log({ course });
        const teacher = await strapi
          .query("teachers")
          .findOne({ id: data.teacher });
        console.log({ teacher });
        const slugString = `${course.title} ${formatedDate} ${teacher.name}`;
        const slug = slugify(slugString, { lower: true });
        console.log({ slug });
        data.slug = slug;
      }
    },
  },
};
