"use strict";

module.exports = {
  async markPaidPayment(ctx) {
    console.log("triggered markPaidPayment");

    //fetch the data provided by frontend
    const { user } = ctx.state;
    const { note, paymentId } = ctx.request.body;

    //log the related data
    console.log({ ctx });
    console.log({ user });
    console.log({ note, paymentId });
    //check for required data
    ctx.assert(note, 400, "note required");
    ctx.assert(paymentId, 400, "paymentId required");

    //upadet the payment status
    await strapi.query("payments").update(
      { id: paymentId },
      {
        status: "paid",
        isLmsAdminFulfilled: true,
      }
    );
    // add the notes
    const admin = await strapi.query("admins").findOne({ email: user.email });
    const notesData = {
      note: note,
      admin: admin,
      time: Date.now(),
    };
    await strapi.services.notes.create(notesData);

    //send a success response
    ctx.send("success");
  },
};
