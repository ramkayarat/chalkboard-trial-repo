"use strict";

module.exports = {
  async markAdded(ctx) {
    console.log("triggered markAdded");

    //fetch the data provided by frontend
    const { user } = ctx.state;
    const { note, taskId, studentId } = ctx.request.body;

    //log the related data
    console.log({ ctx });
    console.log({ user });
    console.log({ note, taskId });
    //check for required data
    ctx.assert(taskId, 400, "taskId required");
    ctx.assert(studentId, 400, "studentId required");
    ctx.assert(note, 400, "note required");

    // mark  the task done
    const admin = await strapi.query("admins").findOne({ email: user.email });
    await strapi.query("tasks").update(
      { id: taskId },
      {
        isDone: true,
        doneBy: admin,
        completionTime: Date.now(),
      }
    );

    // mark student is added for further enrolments
    await strapi.query("student").update(
      { id: studentId },
      {
        isAdded: true,
      }
    );

    // add the notes
    const notesData = {
      note: note,
      admin: admin,
      time: Date.now(),
    };
    await strapi.services.notes.create(notesData);

    //send a success response
    ctx.send("success");
  },
};
