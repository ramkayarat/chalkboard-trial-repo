"use strict";

module.exports = {
  async undoEnrollStudent(ctx) {
    console.log("triggered undoEnrollStudent");

    //fetch the data provided by frontend
    const { user } = ctx.state;
    const { note, taskId } = ctx.request.body;

    //log the related data
    console.log({ ctx });
    console.log({ user });
    console.log({ note, taskId });

    //check for required data
    ctx.assert(taskId, 400, "taskId required");
    ctx.assert(note, 400, "note required");

    // undo the tasks
    const currentTime = Date.now();
    const admin = await strapi.query("admins").findOne({ email: user.email });
    await strapi.query("tasks").update(
      { id: taskId },
      {
        isDone: false,
        completionTime: null,
        doneBy: null,
      }
    );
    // add the notes
    const notesData = {
      note: note,
      admin: admin,
      time: currentTime,
    };
    await strapi.services.notes.create(notesData);

    //send a success response
    ctx.send("success");
  },
};
