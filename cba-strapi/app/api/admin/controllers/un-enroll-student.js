"use strict";

module.exports = {
  async unEnrollStudent(ctx) {
    console.log("triggered unEnrollStudent");

    //fetch the data provided by frontend
    const { user } = ctx.state;
    const { note, batchId, studentId } = ctx.request.body;

    //log the related data
    console.log({ ctx });
    console.log({ user });
    console.log({ note, batchId, studentId });
    //check for required data
    ctx.assert(note, 400, "note required");
    ctx.assert(batchId, 400, "batchID required");
    ctx.assert(studentId, 400, "studentId required");

    //update the payment of every pending payments of the student for the given batch
    // counting the number of pending payments of given batchId
    const count = await strapi
      .query("payments")
      .count({ batchId: batchId, status: "cancelled", student: studentId });
    console.log(`found ${count} pending payments`);
    
    for (var i = 0; i < count; i++) {
      await strapi.query("payments").update(
        { batchId: batchId, status: "cancelled", student: studentId },
        {
          isLmsAdminFulfilled: true,
        }
      );
    }

    // add the notes
    const admin = await strapi.query("admins").findOne({ email: user.email });
    const notesData = {
      note: note,
      admin: admin,
      time: Date.now(),
    };
    await strapi.services.notes.create(notesData);

    //send a success response to frontend
    ctx.send("success");
  },
};
