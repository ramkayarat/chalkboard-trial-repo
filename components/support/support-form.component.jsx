import { useState, useContext } from 'react';
import { PopUpContext } from '../../providers/pop-up.provider';
import { contactSupport } from '../../services/support.service';

import ColumnLayout from '../helper/column-layout/column-layout';
import CustomButton from '../helper/custom-button/custom-button';

import utilStyles from '../../styles/utils.module.css';

function SupportForm() {
  const [enabled, setEnable] = useState(false);
  const [mailingInfo, setMailingInfo] = useState({
    email: '',
    name: '',
    message: '',
  });
  const { togglePopUpState } = useContext(PopUpContext);

  const handleChange = (evt) => {
    let filled = false;
    const { id, value } = evt.target;

    setMailingInfo({ ...mailingInfo, [id]: value });

    Object.values(mailingInfo).forEach((value) =>
      value.length > 0 ? (filled = true) : (filled = false)
    );
    setEnable(filled);
  };

  const handleSubmit = async (evt) => {
    evt.preventDefault();

    const res = await contactSupport(mailingInfo);
    if (res === 'success') {
      togglePopUpState('success');
    }

    document.getElementById('support-form').reset();
  };

  return (
    <form
      className='my-8 md:w-auto md:mx-auto'
      id='support-form'
      onSubmit={(evt) => handleSubmit(evt)}
    >
      <ColumnLayout>
        <div className='md:flex md:justify-between md:my-8'>
          <input
            type='email'
            placeholder='My Email ID'
            id='email'
            required
            onChange={(evt) => handleChange(evt)}
            className={`${utilStyles['text-field']} placeholder-cba-blue placeholder-opacity-50`}
          />
          <div className='h-10px md:w-5' />
          <input
            type='text'
            placeholder='My Name'
            id='name'
            onChange={(evt) => handleChange(evt)}
            required
            className={`${utilStyles['text-field']} placeholder-cba-blue placeholder-opacity-50`}
          />
        </div>
        <div className='h-10px md:hidden' />
        <textarea
          id='message'
          placeholder='My Message/Query'
          onChange={(evt) => handleChange(evt)}
          className={`${utilStyles['text-area']} w-full placeholder-cba-blue placeholder-opacity-50`}
        />
        <div className='h-10' />
        <CustomButton type='submit' disabled={!enabled}>
          send us a message
        </CustomButton>
      </ColumnLayout>
    </form>
  );
}

export default SupportForm;
