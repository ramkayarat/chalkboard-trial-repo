import { useContext } from 'react';
import { PopUpContext } from '../../providers/pop-up.provider';
import { popUpStates } from '../../constants/pop-up-state.types';

import SuccessPopUp from '../pop-ups/success/success.component';
import SupportForm from './support-form.component';

import utilStyles from '../../styles/utils.module.css';

function SupportSection() {
  const { popUpState } = useContext(PopUpContext);

  if (popUpState === popUpStates.SUCCESS)
    return (
      <SuccessPopUp text='Your message has been sent, and you’ll hear from us soon!' />
    );

  return (
    <section>
      <h4
        className={`${utilStyles.heading4} my-8 text-cba-dark-gray text-center capitalize`}
      >
        contact support
      </h4>
      <p className={`${utilStyles.body1} text-black text-center`}>
        Write to us by filling in this form.
      </p>
      <SupportForm />
    </section>
  );
}

export default SupportSection;
