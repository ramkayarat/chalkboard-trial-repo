import Link from 'next/link';
import { useRouter } from 'next/router';

import CustomButton from '../../helper/custom-button/custom-button';

import { URIs } from '../../../constants/image-uri';
import utilStyles from '../../../styles/utils.module.css';
import styles from './landing-section.module.css';
import { COMPANY_TAGLINE, COMPANY_TAGLINE_2 } from '../../../constants/strings';
import { AppRoutes } from '../../../constants/app-routes';

export default function LandingSection() {
  const router = useRouter();

  return (
    <section className='h-full relative overflow-hidden'>
      <div className={`${styles.headSection} xl:w-3/4`}>
        <div
          className={`${styles.featureImage} w-full h-128 lg:h-lg lg:w-lg`}
          style={{ backgroundImage: `url(${URIs.LEARNING_IMAGE_URL})` }}
        />
        <div className={utilStyles.divider} />
        <div className='md:flex md:items-end lg:flex-col lg:pl-20 lg:items-start lg:w-1/2'>
          <div className=' md:ml-10 lg:m-0'>
            <h4
              className={`${utilStyles.heading4} m-0 md:text-left md:font-bold lg:font-normal lg:text-5.6xl`}
            >
              {COMPANY_TAGLINE}
            </h4>
            <div className={utilStyles.divider} />
            <p className={`${utilStyles.body1} md:text-left`}>
              {COMPANY_TAGLINE_2}
            </p>
            <div className={utilStyles.divider} />
          </div>
          <div className='flex flex-col items-center justify-center w-full lg:w-auto'>
            <CustomButton onClick={() => router.push(AppRoutes.BATCHES)}>
              start learning now
            </CustomButton>
            <div className={utilStyles.divider} />
            {/* TODO:after teacher section is decided. */}
            {/* <Link href={AppRoutes.FOR_TEACHERS}>
            <a className={`${utilStyles.caption} text-center text-cba-blue`}>
            Are you a teacher?
            </a>
          </Link> */}
            <div className={utilStyles.divider} />
            <div className={utilStyles.divider} />
          </div>
        </div>
      </div>
      {/* yellow wave */}
      <div
        className={styles.wave}
        style={{ backgroundImage: `url(${URIs.YELLOW_WAVE})` }}
      />
    </section>
  );
}
