import styles from './learning-categories.module.css';
import utilStyles from '../../../styles/utils.module.css';

import { URIs } from '../../../constants/image-uri';

import LearningCategory from './learning-category/learning-category.component';

const categories = [
  {
    category: 'ICSE',
    imageURI: URIs.CISCE_BOARD_IMAGE_URL,
    tagline: 'CISCE board Class 6-12',
    buttonText: 'SEE ALL CISCE COURSES',
  },
  {
    category: 'CBSE',
    imageURI: URIs.CBSE_BOARD_IMAGE_URL,
    tagline: 'CBSE board Class 6-12',
    buttonText: 'SEE ALL CBSE COURSES',
  },
  {
    category: 'MSBSHSE',
    imageURI: URIs.MAHARASHTRA_BOARD_IMAGE_URL,
    tagline: 'Maharashtra State Board Class 6-12',
    buttonText: 'SEE ALL MSBSHSE COURSES',
  },
  {
    category: 'Competitive_Exam_Prep',
    imageURI: URIs.DEFENCE_EXAMINATION_IMAGE_URL,
    tagline: 'Competitive Exam Prep',
    buttonText: 'SEE Comp. Exam COURSES',
  },
  {
    category: 'Advanced_Skill_Development',
    imageURI: URIs.SKILL_DEVELOPMENT_IMAGE_URL,
    tagline: 'Courses for Skill Development',
    buttonText: 'SEE RELEVANT COURSES',
  },
];

export default function LearningCategories() {
  return (
    <div className='relative overflow-hidden'>
      <div
        className={styles.wave}
        style={{ backgroundImage: `url(${URIs.YELLOW_WAVE})` }}
      />
      <div
        className={styles.wave}
        style={{ backgroundImage: `url(${URIs.YELLOW_WAVE})` }}
      />
      <section className={styles.learningCategories}>
        <h4 className={`${utilStyles.heading4} my-8 text-center`}>
          What can I learn here?
        </h4>
        <div className={utilStyles.divider} />
        <div className='md:flex md:flex-wrap md:row-gap-6 md:justify-center mb-12'>
          {categories.map((category, i) => (
            <LearningCategory {...category} key={i} />
          ))}
        </div>
      </section>
    </div>
  );
}
