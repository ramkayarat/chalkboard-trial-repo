import ColumnLayout from '../../../helper/column-layout/column-layout';

import styles from './learning-category.module.css';
import utilStyles from '../../../../styles/utils.module.css';
import CustomButton from '../../../helper/custom-button/custom-button';
import Link from 'next/link';
import { AppRoutes } from '../../../../constants/app-routes';

export default function LearningCategory({ imageURI, category, buttonText, tagline }) {
  
  return (
    <Link
      href={{
        pathname: AppRoutes.BATCHES,
        query: { category },
      }}
    >
      <div
        className={`${styles.learningCategory} lg:flex lg:flex-col lg:justify-end`}
      >
        <ColumnLayout>
          <div
            className={styles.image}
            style={{ backgroundImage: `url(${imageURI})` }}
          />
          <div className={utilStyles.divider} />
          <h6
            className={`${utilStyles.heading6} text-center text-cba-dark-gray m-0`}
          >
            {tagline}
          </h6>
          <div className={utilStyles.divider} />
          <CustomButton>{buttonText}</CustomButton>
        </ColumnLayout>
      </div>
    </Link>
  );
}
