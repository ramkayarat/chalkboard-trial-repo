import Link from 'next/link';

import styles from './footer-button.module.css';

export default function FooterButton({ children, linkTo, target }) {
  return (
    <Link href={linkTo}>
      <a className={styles.footerButton} target={target}>
        {children}
      </a>
    </Link>
  );
}
