import { URIs } from '../../../constants/image-uri';
import { useContext } from 'react';

import { JWTContext } from '../../../providers/jwt.provider';
import FooterButton from './footer-button/footer-button.component';

import styles from './footer.module.css';
import utilStyles from '../../../styles/utils.module.css';
import { AppRoutes } from '../../../constants/app-routes';
import {
  COMPANY_ADDRESS_LINE_1,
  COMPANY_ADDRESS_LINE_2,
} from '../../../constants/strings';

export default function Footer() {
  const token = useContext(JWTContext);

  return (
    <div className={styles.footerSection}>
      <div className='flex flex-col justify-center items-center'>
        {/* logo */}
        <div
          className={`${styles.logo} w-32 h-32 md:w-128 md:h-128 `}
          style={{ backgroundImage: `url(${URIs.LOGO_URI})` }}
        />
        <div className={utilStyles.divider} />
        <div className={utilStyles.divider} />
        {/* links */}
        <div className='md:flex md:items-center md:justify-evenly md:w-full'>
          <div className='flex flex-col md:items-start'>
            {token ? (
              <FooterButton linkTo={AppRoutes.MY_PROFILE}>
                MY PROFILE
              </FooterButton>
            ) : (
              <FooterButton linkTo={AppRoutes.LOGIN}>
                LOG-IN / SIGN-UP
              </FooterButton>
            )}
            <FooterButton linkTo={AppRoutes.BATCHES}>
              UPCOMING BATCHES
            </FooterButton>
            <FooterButton linkTo={AppRoutes.SUPPORT}>
              CONTACT SUPPORT
            </FooterButton>
            {/* TODO:reomve when teachers section is live */}
            {/* <FooterButton linkTo='/for-teachers'>FOR TEACHERS</FooterButton> */}
            <FooterButton linkTo={AppRoutes.FAQS}>FAQ</FooterButton>
          </div>
          <div className='flex flex-col md:items-end'>
            <FooterButton linkTo={AppRoutes.PRIVACY_POLICY} target='_blank'>
              PRIVACY POLICY
            </FooterButton>
            <FooterButton linkTo={AppRoutes.TERMS_OF_SERVICE} target='_blank'>
              TERMS OF SERVICE
            </FooterButton>
            <FooterButton linkTo={AppRoutes.HOME}>
              © CHALKBOARD ACADEMY
            </FooterButton>
          </div>
        </div>
        {/* address */}
        <div className={utilStyles.divider} />
        <p className={`${utilStyles.overline} text-cba-blue text-center`}>
          {COMPANY_ADDRESS_LINE_1}
          <br />
          {COMPANY_ADDRESS_LINE_2}
        </p>
        <div className={utilStyles.divider} />
      </div>
    </div>
  );
}
