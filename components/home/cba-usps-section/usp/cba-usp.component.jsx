import styles from './cba-usp.module.css';
import utilStyles from '../../../../styles/utils.module.css';

export default function USP({ imageURI, tagline, description, index }) {
  return (
    <>
      <div
        className={`${index % 2 === 0 ? styles.reason : styles.reasonReverse}`}
      >
        <div
          className={styles.image}
          style={{ backgroundImage: `url(${imageURI})` }}
        />
        <div className='flex flex-col justify-center items-center md:items-start md:m-5 lg:items-center'>
          <h6
            className={`${utilStyles.heading6} m-0 text-cba-dark-gray text-center md:text-left lg:text-center`}
          >
            {tagline}
          </h6>
          <div className={utilStyles.divider} />
          <p className={`${utilStyles.body2} text-cba-dark-gray text-center md:text-left lg:text-center`}>{description}</p>
        </div>
      </div>
    </>
  );
}
