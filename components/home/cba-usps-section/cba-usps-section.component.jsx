import styles from './cba-usps-section.module.css';
import utilStyles from '../../../styles/utils.module.css';

import USP from './usp/cba-usp.component';

import { URIs } from '../../../constants/image-uri';
import { reasonSectionStrings } from '../../../constants/strings';


const reasons = [
  {
    imageURI: URIs.PERSONALISED_LEARNING_IMAGE_URL,
    tagline: reasonSectionStrings.REASON_1_TAGLINE,
    description: reasonSectionStrings.REASON_1_DESCRIPTION,
  },
  {
    imageURI: URIs.BEST_IN_CLASS_IMAGE_URL,
    tagline: reasonSectionStrings.REASON_2_TAGLINE,
    description: reasonSectionStrings.REASON_2_DESCRIPTION,
  },
  {
    imageURI: URIs.EXPERT_TEACHERS_IMAGE_URL,
    tagline: reasonSectionStrings.REASON_3_TAGLINE,
    description: reasonSectionStrings.REASON_3_DESCRIPTION,
  },
  {
    imageURI: URIs.COVERAGE_ACROSS_IMAGE_URL,
    tagline: reasonSectionStrings.REASON_4_TAGLINE,
    description: reasonSectionStrings.REASON_4_DESCRIPTION,
  },
  {
    imageURI: URIs.AFFORDIBILITY_IMAGE_URL,
    tagline: reasonSectionStrings.REASON_5_TAGLINE,
    description: reasonSectionStrings.REASON_5_DESCRIPTION,
  },
];

export default function CbaUSPs() {
  return (
    <div className={styles.whyCBAWrap}>
      <section className={styles.whyCBA}>
        <h4
          className={`${utilStyles.heading4} text-white m-0 mb-8 text-center`}
        >
          Why Chalkboard Academy?
        </h4>
        <div className='mt-10  lg:flex lg:flex-wrap lg:justify-center'>
          {reasons.map((reason, i) => (
            <USP index={i} {...reason} key={i} />
          ))}
        </div>
      </section>
    </div>
  );
}
