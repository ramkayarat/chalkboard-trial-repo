import Link from 'next/link';

import ColumnLayout from '../../helper/column-layout/column-layout';
import CustomButton from '../../helper/custom-button/custom-button';

import utilStyles from '../../../styles/utils.module.css';
import { URIs } from '../../../constants/image-uri';
import styles from './teachers-section.module.css';
import { AppRoutes } from '../../../constants/app-routes';

export default function Teachers() {
  return (
    <ColumnLayout>
      <div className='relative w-full text-center overflow-hidden lg:pt-20'>
        <div
          className={styles.wave}
          style={{ backgroundImage: `url(${URIs.YELLOW_WAVE})` }}
        />

        {/* adds 80px divider */}
        <div className={utilStyles.divider} />
        <div className={utilStyles.divider} />
        <div className={utilStyles.divider} />
        <div className={utilStyles.divider} />
        <h4 className={`${utilStyles.heading4} m-0 text-center`}>
          Are you a Teacher?
        </h4>

        {/* adds 20px divider */}
        <div className={utilStyles.divider} />
        <p className={`${utilStyles.body1} text-cba-dark-gray text-center`}>
          Or, do you want to be one?
        </p>

        {/* adds 60px divider */}
        <div className={utilStyles.divider} />
        <div className={utilStyles.divider} />
        <div className={utilStyles.divider} />
        <CustomButton>fill form to apply</CustomButton>
        <div className={utilStyles.divider} />
        <Link href={AppRoutes.FOR_TEACHERS}>
          <span className={`${utilStyles.caption} text-center text-cba-blue`}>
            Learn more
          </span>
        </Link>
      </div>
    </ColumnLayout>
  );
}
