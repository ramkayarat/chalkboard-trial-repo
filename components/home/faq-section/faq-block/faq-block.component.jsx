import { useState } from 'react';

import { FaChevronUp, FaChevronDown } from 'react-icons/fa';

import styles from './faq-block.module.css';
import utilStyles from '../../../../styles/utils.module.css';

export default function FAQBlock({ question, answer, i }) {
  const [active, setActive] = useState(false);

  const getHeight = function (className) {
    var element = document.getElementsByClassName(className);
    return element[i].scrollHeight;
  };

  return (
    <div className='lg:flex lg:flex-col lg:items-center lg:w-1/3 lg:my-5'>
      <button
        className={`${styles.question} w-full lg:w-1/3`}
        onClick={() => {
          setActive(!active);
        }}
      >
        {question}
        <div className={styles['button-icon']}>
          {active ? (
            <FaChevronUp size={25} color='white' />
          ) : (
            <FaChevronDown size={25} color='white' />
          )}
        </div>
      </button>
      <div
        className={`${styles.panel} lg:w-1/3`}
        style={{ maxHeight: `${active ? getHeight(styles.panel) + 'px' : 0}` }}
      >
        <div className='py-2'>
          <p className={`${utilStyles.subtitleS} text-left`}>{answer}</p>
        </div>
      </div>
    </div>
  );
}
