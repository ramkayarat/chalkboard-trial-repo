import { useRouter } from 'next/router';

import FAQBlock from './faq-block/faq-block.component';
import CustomButton from '../../helper/custom-button/custom-button';

import styles from './faq-section.module.css';
import utilStyles from '../../../styles/utils.module.css';
import { AppRoutes } from '../../../constants/app-routes';

function FAQSection({ faqs }) {
  const router = useRouter();

  return (
    <section className={styles.faqSection}>
      <>
        <h4 className={`${utilStyles.heading4} text-white m-0 text-center`}>
          Frequently Asked Questions
        </h4>

        {/* adds 40px divider */}
        <div className={utilStyles.divider} />
        <div className='lg:flex lg:flex-wrap lg:justify-center'>
          {faqs &&
            faqs
              .filter((faq) => faq.isImportant)
              .map((faq, i) => <FAQBlock i={i} {...faq} key={i} />)}
        </div>

        {/* adds 60px divider */}
        <div className={utilStyles.divider} />
        <div className={utilStyles.divider} />
        <div className={utilStyles.divider} />
        <span
          className={`${utilStyles.caption} text-cba-dark-gray text-center`}
        >
          Is your question still unanswered?
        </span>
        <div className={utilStyles.divider} />
        <CustomButton onClick={() => router.push(AppRoutes.FAQS)}>
          see all faq's
        </CustomButton>
      </>
    </section>
  );
}

export default FAQSection;
