import { useEffect } from 'react';

import { AuthToken } from '../../services/auth-token.service';
import Router from 'next/router';
import { AppRoutes } from '../../constants/app-routes';

export default function withProtection(WrappredComponent) {
  return function ({ ...otherProps }) {
    const token = AuthToken.getToken();

    useEffect(() => {
      if (!token) {
        Router.push(AppRoutes.LOGIN);
      }
    }, [token]);

    return <WrappredComponent {...otherProps} />;
  };
}
