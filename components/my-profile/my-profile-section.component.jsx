import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import { boards, classes } from '../../constants/filter.options';
import { logout } from '../../services/auth.service';

import utilStyles from '../../styles/utils.module.css';
import DropDown from '../drop-down/drop-down';
import ColumnLayout from '../helper/column-layout/column-layout';
import CustomButton from '../helper/custom-button/custom-button';
import ProfileInfoColumn from './profile-info-column/profile-info-column.component';
import styles from './my-profile-section.component.module.css';
import { updateStudentProfile } from '../../services/profile.service';
import { AppRoutes } from '../../constants/app-routes';

export default function MyProfileSection({ data, token }) {
  const [initialUserInfo, setInitialUserInfo] = useState({});
  const [updatedFields, setUpdatedFields] = useState({});

  const { firstName, familyName, email, phoneNumber, educationBoard } = data;
  const router = useRouter();

  useEffect(() => {
    setInitialUserInfo({
      firstName: firstName,
      familyName: familyName,
      email: email,
      phoneNumber: phoneNumber ? phoneNumber : '',
      board: educationBoard,
      class: data.class,
      isEdited: false,
      isPhoneEdited: false,
    });
  }, [data]);

  const handleSubmit = async (evt) => {
    evt.preventDefault();

    //if phone is edited push to phone verify
    await updateStudentProfile({
      studentData: updatedFields,
      token,
    });

    // if (initialUserInfo.isPhoneEdited) {
    // router.push(AppRoutes.PHONE_VERIFY);
    // }

    setInitialUserInfo({
      ...initialUserInfo,
      isEdited: false,
      isPhoneEdited: false,
    });
  };

  const filterOptions = {
    educationBoard: boards,
    class: classes,
  };

  const toggleDropDownSelected = ({ id, key }) => {
    let option = filterOptions[key];
    option = option.map((element) => {
      let temp = element;
      temp.id === id
        ? (temp.selected = !temp.selected)
        : (temp.selected = false);

      return temp;
    });

    setInitialUserInfo({
      ...initialUserInfo,
      isEdited: true,
    });

    setUpdatedFields({
      ...updatedFields,
      [key]: option[id].selected ? option[id].value : null,
    });
  };

  return (
    <section className={styles.background}>
      <ColumnLayout>
        <h4 className={`${utilStyles.heading4} m-0 py-8`}>My Profile</h4>

        <div className='md:flex md'>
          <CustomButton onClick={() => router.push(AppRoutes.MY_COURSES)}>
            My Courses
          </CustomButton>

          {/* 20px spacer */}
          <div className={`${utilStyles.divider} md:w-5`} />
          <CustomButton onClick={() => router.push(AppRoutes.MY_PAYMENTS)}>
            My Payments
          </CustomButton>
        </div>

        <div className='grid gap-5 pt-10'>
          <ProfileInfoColumn
            updatedFields={updatedFields}
            setUpdatedFields={setUpdatedFields}
            initialUserInfo={initialUserInfo}
            setInitialUserInfo={setInitialUserInfo}
          />

          <div className='grid grid-cols-1 gap-5 md:grid-cols-2'>
            <DropDown
              list={filterOptions.educationBoard}
              title={educationBoard ? educationBoard : 'My Board'}
              toggleItem={toggleDropDownSelected}
            />
            <DropDown
              list={filterOptions.class}
              title={data.class ? `Class ${data.class}` : 'My Class'}
              toggleItem={toggleDropDownSelected}
            />
          </div>

          <div className='md:mx-auto'>
            <CustomButton
              disabled={!initialUserInfo.isEdited}
              onClick={(evt) => handleSubmit(evt)}
            >
              Save Changes
            </CustomButton>
          </div>
        </div>
        <div
          onClick={() => logout()}
          className={`${utilStyles.caption} cursor-pointer text-center text-cba-blue pt-10 pb-5 lg:pb-10`}
        >
          Logout
        </div>
      </ColumnLayout>
    </section>
  );
}
