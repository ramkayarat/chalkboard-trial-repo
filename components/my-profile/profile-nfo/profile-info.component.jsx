import { useState } from "react";

import { URIs } from "../../../constants/image-uri";

import utilStyles from "../../../styles/utils.module.css";
import styles from "./profile-info.module.css";

export default function ProfileInfo({
  isEditable=true,
  field,
  initialUserInfo,
  id,
  setInitialUserInfo,
  updatedFields,
  setUpdatedFields,
}) {
  const [edit, setEdit] = useState(false);

  const handleChange = (evt) => {
    const { id, value } = evt.target;

    if (id === "phoneNumber") {
      setUpdatedFields({
        ...updatedFields,
        [id]: value,
      });

      setInitialUserInfo({
        ...initialUserInfo,
        isEdited: true,
        isPhoneEdited: true,
      });
      return;
    }

    setUpdatedFields({
      ...updatedFields,
      [id]: value,
    });

    setInitialUserInfo({ ...initialUserInfo, isEdited: true });
  };

  return (
    <div className="md:w-7/12 h-auto relative md:mx-auto">
      <div className="flex flex-col w-64">
        <div className="uppercase text-sm text-cba-blue">{field}</div>
        {!edit ? (
          <div className="text-lg">{initialUserInfo[id]}</div>
        ) : (
          <input
            id={id}
            placeholder={initialUserInfo[id]}
            onChange={(evt) => handleChange(evt)}
            className={`${utilStyles["text-field"]} placeholder-cba-blue placeholder-opacity-50`}
          />
        )}
      </div>

     {isEditable && <div className="my-auto" onClick={() => setEdit(!edit)}>
        <img
          src={URIs.EDIT_ICON}
          className={`${styles["edit-icon"]} ${edit ? "mt-2 mr-3" : ""}`}
        />
      </div>}
    </div>
  );
}
