import ProfileInfo from '../profile-nfo/profile-info.component';

function ProfileInfoColumn({
   initialUserInfo,
  setInitialUserInfo,
  updatedFields,
  setUpdatedFields,
}) {
  return (
    <>
      <ProfileInfo
        field="First Name"
        id="firstName"
        updatedFields={updatedFields}
        setUpdatedFields={setUpdatedFields}
        initialUserInfo={initialUserInfo}
        setInitialUserInfo={setInitialUserInfo}
      />
      <ProfileInfo
        field="Family Name"
        id="familyName"
        updatedFields={updatedFields}
        setUpdatedFields={setUpdatedFields}
        initialUserInfo={initialUserInfo}
        setInitialUserInfo={setInitialUserInfo}
      />
      <ProfileInfo
        field="email"
        id="email"
        isEditable={false}
        updatedFields={updatedFields}
        setUpdatedFields={setUpdatedFields}
        initialUserInfo={initialUserInfo}
        setInitialUserInfo={setInitialUserInfo}
      />
      <ProfileInfo
        field="phone number"
        id="phoneNumber"
        updatedFields={updatedFields}
        setUpdatedFields={setUpdatedFields}
        initialUserInfo={initialUserInfo}
        setInitialUserInfo={setInitialUserInfo}
      />
    </>
  );
}

export default ProfileInfoColumn;
