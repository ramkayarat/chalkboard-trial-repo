import { useEffect, useState } from 'react';

import styles from './drop-down.module.css';
import utilStyles from '../../styles/utils.module.css';
import { FaChevronDown, FaChevronUp } from 'react-icons/fa';

export default function DropDown({ title, list, toggleItem, temporaryTitle }) {
  const [headerTitle, setHeaderTitle] = useState(title);
  const [listOpen, setListOpen] = useState(false);

  //temporary title is only added for when category is selected from the homepage
  useEffect(() => {
    if (temporaryTitle) {
      setHeaderTitle(headerTitle === title ? temporaryTitle : headerTitle);
    } else {
      setHeaderTitle(title);
    }
  }, [temporaryTitle]);

  const toggleOpen = () => {
    setListOpen(!listOpen);
  };

  return (
    <div className='md:flex md:flex-col relative'>
      <div className={`${styles['dd-header']}`} onClick={() => toggleOpen()}>
        <div
          className={`text-cba-blue text-lg ${
            title !== headerTitle ? '' : 'text-opacity-50'
          }`}
        >
          {headerTitle}
        </div>
        <div className={styles['arrow-bg']}>
          {listOpen ? (
            <FaChevronUp color='white' size={30} />
          ) : (
            <FaChevronDown color='white' size={30} />
          )}
        </div>
      </div>
      {listOpen && (
        <ul className={`${styles['dd-list']} lg:mx-auto`}>
          {list.map((item) => (
            <li
              className={`${styles['dd-list-item']} ${
                utilStyles.body1
              } text-cba-blue 
                ${item.selected ? styles.active : ''}`}
              onClick={() => {
                toggleItem({ id: item.id, key: item.key });
                toggleOpen();
                setHeaderTitle(item.selected ? item.title : title);
              }}
              key={item.id}
            >
              {item.title}
            </li>
          ))}
        </ul>
      )}
    </div>
  );
}
