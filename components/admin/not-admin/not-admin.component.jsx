import Link from 'next/link';
import { URIs } from '../../../constants/image-uri'

import CustomButton from '../../helper/custom-button/custom-button';

import utilStyles from '../../../styles/utils.module.css'
import { AppRoutes } from '../../../constants/app-routes';

function NotAdmin(){
    return <div className='flex flex-col h-screen w-full items-center justify-center'>
    <img src={URIs.LOGO_URI} width='150' />
    <h4 className={`${utilStyles.heading4} m-0 text-center`}>
      You are not an admin.
    </h4>
    <div className='mt-4'>
      <Link href={AppRoutes.HOME}>
        <a>
          <CustomButton>Go to HomePage</CustomButton>
        </a>
      </Link>
    </div>
  </div>
}

export default NotAdmin;