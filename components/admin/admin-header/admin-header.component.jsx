import Link from 'next/link';
import { useEffect, useState } from 'react';
import { AppRoutes } from '../../../constants/app-routes';

import { URIs } from '../../../constants/image-uri';
import { AuthToken } from '../../../services/auth-token.service';
import { logout } from '../../../services/auth.service';
import styles from './admin-header.module.css';

export default function AdminHeader() {
  const [adminLoggedIn, setLoggedIn] = useState(false);

  useEffect(() => {
    if (AuthToken.getToken()) {
      setLoggedIn(true);
    }
  }, []);

  return (
    <>
      <div className={`${styles.header}`}>
        <div className={styles[`brand-logo`]}>
          <Link href={AppRoutes.HOME}>
            <img src={URIs.HEADER_LOGO_URI} />
          </Link>
        </div>
        <div
          onClick={() => (adminLoggedIn ? logout() : null)}
          className='font-body cursor-pointer font-medium text-cba-blue uppercase'
        >
          {adminLoggedIn ? 'log-out' : ''}
        </div>
      </div>
      <div className='h-82px' />
    </>
  );
}
