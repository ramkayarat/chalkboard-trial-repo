import { FaUserPlus } from 'react-icons/fa';
import { HiCurrencyDollar } from 'react-icons/hi';
import { colors } from '../../../constants/colors';
import { URIs } from '../../../constants/image-uri';
import { adminTaskSections } from '../admin-task-sectoin.type';
import styles from './admin-footer.module.css';

export default function AdminFooter({ activeSection, setActiveSection }) {
  return (
    <div className='h-16'>
      <div className={`${styles.footer}`}>
        <FaUserPlus
          color={
            activeSection === adminTaskSections.ADD_STUDENTS
              ? colors.CBA_YELLOW
              : colors.CBA_DAR_GRAY
          }
          className={`${styles.addStudent} ${styles.footerIcon}`}
          onClick={() => setActiveSection(adminTaskSections.ADD_STUDENTS)}
        />
        <img
          src={
            activeSection === adminTaskSections.ENROLL_STUDENTS
              ? URIs.TV_YELLOW_ICON_URI
              : URIs.TV_ICON_URI
          }
          onClick={() => setActiveSection(adminTaskSections.ENROLL_STUDENTS)}
        />
        <HiCurrencyDollar
          color={
            activeSection === adminTaskSections.PENDING_PAYMENTS
              ? colors.CBA_YELLOW
              : colors.CBA_DAR_GRAY
          }
          className={`${styles.footerIcon}`}
          onClick={() => setActiveSection(adminTaskSections.PENDING_PAYMENTS)}
        />
      </div>
    </div>
  );
}
