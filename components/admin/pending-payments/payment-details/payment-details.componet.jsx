import useSWR from "swr";
import moment from "moment";
import { useContext } from "react";
import { get } from "../../../../services/helpers/helpers";

import { JWTContext } from "../../../../providers/jwt.provider";
import { AdminNoteContext } from "../../../../providers/admin.provider";

import CustomCardButton from "../../../helper/custom-card-button/custom-card-button";

import { paymentStatus } from "../../../../constants/payment-status.type";
import { adminTaskTypes } from "../../../../constants/admin-tasks.type";
import { GET_BATCHES_URL } from "../../../../utils/api-urls.utils";
import utilStyles from "../../../../styles/utils.module.css";

export default function PaymentDetails({ paid, payment }) {
  const { toggleNoteOpen, updateTask } = useContext(AdminNoteContext);
  const token = useContext(JWTContext);

  const {
    batchId,
    status,
    dueDate,
    amount,
    created_at,
    isLmsAdminFulfilled,
    id: paymentId,
  } = payment;
  const { firstName, familyName, id: studentId } = payment.student;
  const { data: { course } = { course: {} }, getCourseError } = useSWR(
    `${GET_BATCHES_URL}/${batchId}`,
    get
  );

  const { title, class: courseClass, subject } = course;

  return (
    <>
      <div className="lg:bg-cba-blue lg:bg-opacity-5">
        <div className="py-3 md:flex lg:flex-col">
          <div className="md:w-4/5 lg:w-auto">
            <span
              className={`${utilStyles.overline} text-cba-disabled text-left`}
            >
              {moment(created_at).format("MMMM Do YYYY, h:mm:ss a")}
            </span>
            <PaymentDetail
              detail="Name"
              detailContent={`"${firstName} ${familyName}"`}
              paid={paid}
            />
            <PaymentDetail
              detail="Course"
              detailContent={`"${subject} for ${courseClass}:${title}"`}
              paid={paid}
            />
            <PaymentDetail
              detail="Batch ID"
              detailContent={`"${batchId}"`}
              paid={paid}
            />
            <PaymentDetail
              detail="Amount"
              detailContent={`INR"${amount / 100}"`}
              paid={paid}
              color={status === "overdue" ? "alert-red" : "blue"}
            />
            <PaymentDetail
              detail="Status"
              detailContent={status.toUpperCase()}
              paid={paid}
              color={status === "overdue" ? "alert-red" : "blue"}
            />
            <PaymentDetail
              detail="Due Date"
              detailContent={`"${moment(dueDate).format("DD-MMM-YYYY")}"`}
              paid={paid}
            />
          </div>

          <div className="md:w-1/2 lg:w-auto">
            {!isLmsAdminFulfilled && (
              <div className="w-auto text-center mt-3 flex flex-wrap row-gap-2 col-gap-3 justify-center md:justify-end lg:justify-center">
                {status === paymentStatus.CANCELLED && (
                  <CustomCardButton
                    alert
                    onClick={() => {
                      updateTask({
                        type: adminTaskTypes.UNENROLL_STUDENT,
                        data: { token, batchId, studentId },
                      });

                      toggleNoteOpen();
                    }}
                  >
                    unenroll
                  </CustomCardButton>
                )}
                {status === paymentStatus.PENDING && (
                  <>
                    <CustomCardButton
                      onClick={() => {
                        updateTask({
                          type: adminTaskTypes.MARK_PAID_PAYMENT,
                          data: { token, paymentId },
                        });

                        toggleNoteOpen();
                      }}
                    >
                      mark paid
                    </CustomCardButton>
                    <CustomCardButton
                      alert
                      onClick={() => {
                        updateTask({
                          type: adminTaskTypes.MARK_CANCELLED_PAYMENT,
                          data: { token, batchId, studentId },
                        });

                        toggleNoteOpen();
                      }}
                    >
                      mark cancelled
                    </CustomCardButton>
                  </>
                )}
              </div>
            )}
          </div>
        </div>
      </div>

      <hr className="h-0.5px bg-cba-disabled" />
    </>
  );
}

const PaymentDetail = ({ detail, detailContent, paid, color }) => (
  <div
    className={`${utilStyles.subtitle1} text-left ${
      paid && !color ? `text-cba-disabled` : "text-cba-dark-gray"
    }`}
  >
    <span className={`${paid ? `text-cba-disabled` : "text-cba-dark-gray"}`}>
      {detail}:{" "}
    </span>
    <span
      className={`${
        color && !paid ? `text-cba-${color} font-bold` : "text-cba-disabled"
      }`}
    >
      {detailContent}
    </span>
  </div>
);
