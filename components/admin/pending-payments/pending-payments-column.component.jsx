import PaymentDetails from './payment-details/payment-details.componet';

import utilStyles from '../../../styles/utils.module.css';
import AdminBackToTop from '../../helper/admin-back-to-top/admin-back-to-top';

export default function PendingPayments({ payments }) {
  return (
    <>
      {/* pending taks */}
      <div className='mt-12 px-5 lg:mt-0 lg:px-0'>
        <h6 className={`${utilStyles.heading6} mb-6 lg:mb-0 text-cba-blue`}>
          Pending Payments
        </h6>
        <hr className='h-0.5px lg:h-1 bg-cba-disabled lg:bg-cba-yellow' />
        {payments
          .filter(
            (payment) =>
              !payment.isLmsAdminFulfilled && payment.status !== 'paid'
          )
          .map((payment) => (
            <PaymentDetails payment={payment} key={payment.id} />
          ))}
      </div>

      {/* completed tasks */}
      <div className='mt-12 px-5 lg:px-0'>
        <h6 className={`${utilStyles.heading6} mb-6 text-cba-blue`}>
          Completed
        </h6>
        <hr className='h-0.5px bg-cba-disabled' />
        {payments
          .filter(
            (payment) =>
              payment.status === 'paid' || payment.isLmsAdminFulfilled
          )
          .map((payment) => (
            <PaymentDetails payment={payment} key={payment.id} paid />
          ))}
      </div>

      <AdminBackToTop />
    </>
  );
}
