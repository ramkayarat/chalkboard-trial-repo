import AddStudent from '../add-student/add-student.component';
import EnrollStudent from '../enroll-student/enroll-student.component';
import PendingPayments from '../pending-payments/pending-payments-column.component';

export default function AdminDesktop({ tasks, payments }) {
  const addStudentTasks = tasks
    ? tasks.filter((task) => task.type == 'add_student')
    : [];
  const enrollStudentTasks = tasks
    ? tasks.filter((task) => task.type == 'enroll_student')
    : [];

  return (
    <section className='lg:px-5 xl:px-40'>
      <div className='grid grid-cols-3 w-full gap-3'>
        {/* enroll students */}
        <div>
          <AddStudent addStudentTasks={addStudentTasks}/>
        </div>

        {/* enroll student to course */}
        <div>
          <EnrollStudent enrollStudentTasks={enrollStudentTasks} />
        </div>

        {/* pending payments */}
        <div>
          <PendingPayments payments={payments} />
        </div>
      </div>
    </section>
  );
}
