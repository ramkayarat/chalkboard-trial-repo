import moment from 'moment';
import { useContext } from 'react';
import { JWTContext } from '../../../../providers/jwt.provider';
import { AdminNoteContext } from '../../../../providers/admin.provider';

import CustomCardButton from '../../../helper/custom-card-button/custom-card-button';

import { adminTaskTypes } from '../../../../constants/admin-tasks.type';
import utilStyles from '../../../../styles/utils.module.css';

export default function StudentDetails({ enrolled, task }) {
  const { toggleNoteOpen, updateTask } = useContext(AdminNoteContext);
  const token = useContext(JWTContext);

  const {
    name,
    email,
    phoneNumber,
    educationBoard,
    class: userClass,
    studentId,
    created_at
  } = task.data;

  return (
    <div className='lg:bg-cba-blue lg:bg-opacity-5 whitespace-normal'>
      <div className='py-3 md:flex lg:flex-col'>
        <div className='md:w-4/5 lg:w-full'>
          <span
            className={`${utilStyles.overline} text-cba-disabled text-left`}
          >
            {moment(created_at).format('MMMM Do YYYY, h:mm:ss a')}
          </span>
          <StudentDetail
            detail='Name'
            detailContent={name}
            enrolled={enrolled}
          />
          <StudentDetail
            detail='Email'
            detailContent={email}
            enrolled={enrolled}
          />
          <StudentDetail
            detail='Phone Number'
            detailContent={phoneNumber}
            enrolled={enrolled}
          />
          <StudentDetail
            detail='Education Board'
            detailContent={educationBoard}
            enrolled={enrolled}
          />
          <StudentDetail
            detail='Class'
            detailContent={userClass}
            enrolled={enrolled}
          />
        </div>
        {!enrolled && (
          <div className='w-auto text-center mt-3'>
            <CustomCardButton
              onClick={() => {
                updateTask({
                  type: adminTaskTypes.MARK_ADDED,
                  data: {
                    token,
                    studentId,
                    taskId: task.id,
                  },
                });

                toggleNoteOpen();
              }}
            >
              mark added
            </CustomCardButton>
          </div>
        )}
      </div>
      <hr className='h-0.5px bg-cba-disabled' />
    </div>
  );
}

const StudentDetail = ({ detail, detailContent, enrolled }) => (
  <p
    className={`${utilStyles.subtitle1} text-left ${
      enrolled ? `text-cba-disabled` : `text-cba-dark-gray`
    }`}
  >
    {detail}:{detailContent}
  </p>
);
