import AdminBackToTop from '../../helper/admin-back-to-top/admin-back-to-top';
import StudentDetails from './student-details/student-details.component';

import utilStyles from '../../../styles/utils.module.css';

export default function AddStudent({ addStudentTasks }) {
  const pendingTasks = addStudentTasks.filter((task) => !task.isDone);
  const completedTasks = addStudentTasks.filter((task) => task.isDone);

  return (
    <>
      {/* pending tasks */}
      <div className='mt-12 px-5 lg:mt-0 lg:px-0'>
        <h6
          className={`${utilStyles.heading6} mb-6 lg:mb-0 text-cba-blue lg:text-center`}
        >
          Add New Student
        </h6>
        <hr className='h-0.5px bg-cba-disabled lg:h-1 lg:bg-cba-yellow' />
        {pendingTasks.map((task) => (
          <StudentDetails task={task} key={task.id} />
        ))}
      </div>

      {/* Completed tasks */}
      <div className='mt-12 px-5 lg:px-0'>
        <h6 className={`${utilStyles.heading6} mb-6 text-cba-blue`}>
          Completed
        </h6>
        <hr className='h-0.5px bg-cba-disabled' />
        {completedTasks.map((task) => (
          <StudentDetails task={task} key={task.id} enrolled />
        ))}
      </div>

      <div className='text-center mt-5 lg:hidden'>
        <AdminBackToTop />
      </div>
    </>
  );
}
