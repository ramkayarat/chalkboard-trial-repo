import StudentDetails from './student-details/student-details.component';

import utilStyles from '../../../styles/utils.module.css';
import AdminBackToTop from '../../helper/admin-back-to-top/admin-back-to-top';

export default function EnrollStudent({ enrollStudentTasks }) {
  return (
    <>
      {/* pending tasks */}
      <div className='mt-12 px-5 lg:mt-0 lg:px-0'>
        <h6 className={`${utilStyles.heading6} mb-6 lg:mb-0 text-cba-blue`}>
          Enroll Student to Course
        </h6>
        <hr className='h-0.5px bg-cba-disabled lg:h-1 lg:bg-cba-yellow' />
        {enrollStudentTasks
          .filter((task) => !task.isDone)
          .map((task) => (
            <StudentDetails task={task} key={task.id} />
          ))}
      </div>

      {/* Completed taks */}
      <div className='mt-12 px-5 lg:px-0'>
        <h6 className={`${utilStyles.heading6} mb-6 text-cba-blue`}>
          Completed
        </h6>
        <hr className='h-0.5px bg-cba-disabled' />
        {enrollStudentTasks
          .filter((task) => task.isDone)
          .map((task) => (
            <StudentDetails task={task} key={task.id} enrolled />
          ))}
      </div>

      <AdminBackToTop />
    </>
  );
}
