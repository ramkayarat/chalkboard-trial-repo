import { useContext } from 'react';
import moment from 'moment';
import { adminTaskTypes } from '../../../../constants/admin-tasks.type';
import { AdminNoteContext } from '../../../../providers/admin.provider';
import { JWTContext } from '../../../../providers/jwt.provider';

import CustomCardButton from '../../../helper/custom-card-button/custom-card-button';

import utilStyles from '../../../../styles/utils.module.css';
import { capitalizeString } from '../../../../utils/utils';

export default function StudentDetails({ enrolled, task }) {
  const { toggleNoteOpen, updateTask } = useContext(AdminNoteContext);
  const token = useContext(JWTContext);
  
  const {
    course: { title, class: courseClass, subject },
    created_at,
    batchId,
    name,
  } = task.data;
  return (
    <>
      <div className='lg:bg-cba-blue lg:bg-opacity-5 '>
        <div className='py-3 md:flex lg:flex-col'>
          <div className='md:w-4/5 lg:w-auto'>
            <span
              className={`${utilStyles.overline} text-cba-disabled text-left`}
            >
              {moment(created_at).format('MMMM Do YYYY, h:mm:ss a')}
            </span>
            <StudentDetail
              detail='Name'
              detailContent={name}
              enrolled={enrolled}
            />
            <StudentDetail
              detail='Course'
              detailContent={`${capitalizeString(
                subject
              )} for ${courseClass}:${title}`}
              enrolled={enrolled}
            />
          </div>
          <div className='w-auto text-center mt-3'>
            <CustomCardButton
              onClick={() => {
                updateTask({
                  type: enrolled
                    ? adminTaskTypes.UNDO_ENROLL_STUDENT
                    : adminTaskTypes.ENROLL_STUDENT,
                  data: {
                    token,
                    batchId,
                    taskId: task.id,
                  },
                });

                toggleNoteOpen();
              }}
            >
              {!enrolled ? `enroll` : `undo enroll`}
            </CustomCardButton>
          </div>
        </div>
      </div>
      <hr className='h-0.5px bg-cba-disabled' />
    </>
  );
}

const StudentDetail = ({ detail, detailContent, enrolled }) => (
  <p
    className={`${utilStyles.subtitle1} text-left ${
      enrolled ? `text-cba-disabled` : `text-cba-dark-gray`
    }`}
  >
    {detail}:{` "${detailContent}"`}
  </p>
);
