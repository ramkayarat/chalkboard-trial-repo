import Link from 'next/link';
import { URIs } from '../../../constants/image-uri';

import CustomButton from '../../helper/custom-button/custom-button';

import utilStyles from '../../../styles/utils.module.css';
import { AppRoutes } from '../../../constants/app-routes';

function NotLoggedIn() {
  return (
    <div className='flex flex-col h-screen w-full items-center justify-center'>
      <img src={URIs.LOGO_URI} width='150' />
      <h4 className={`${utilStyles.heading4} m-0 text-center`}>
        You need to login first
      </h4>
      <div className='mt-4'>
        <Link href={AppRoutes.LOGIN}>
          <a>
            <CustomButton>Go to Login</CustomButton>
          </a>
        </Link>
      </div>
    </div>
  );
}

export default NotLoggedIn;
