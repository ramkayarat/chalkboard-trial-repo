import moment from 'moment';
import { useRouter } from 'next/router';
import { capitalizeString } from '../../../utils/utils';
import { JWTContext } from '../../../providers/jwt.provider';

import CustomButton from '../../helper/custom-button/custom-button';
import MonthlyPaymentSchedule from '../monthly-payment-schedule/monthly-payment-schedule.component';

import utilStyles from '../../../styles/utils.module.css';
import { useContext } from 'react';
import { AppRoutes } from '../../../constants/app-routes';
import { generateInvoice } from '../../../services/payment.service';
import { CheckoutContext } from '../../../providers/checkout.provider';

export default function MonthlyPayment({
  startDate,
  durationInMonths,
  course,
  monthlyPayments,
  id: batchId,
  slug
}) {
  const token = useContext(JWTContext);
  const router = useRouter();
  const { setCheckoutData, toggleCheckoutPopUp } = useContext(CheckoutContext);

  const onClick = async () => {
    if (!token) {
      alert('login first');
      router.push(AppRoutes.LOGIN);
      return;
    }

    const invoice = await generateInvoice({ batchId, paymentPlan: 'monthly' });
    setCheckoutData({ course, invoice ,slug});
    toggleCheckoutPopUp();
  };

  const { subject, class: courseClass, title } = course;

  return (
    <>
      <div className='lg:flex lg:my-12 lg:justify-center'>
        <div className='h-auto mx-auto w-128 md:w-lg lg:w-1/3'>
          <p
            className={`${utilStyles.body1} text-left text-cba-dark-gray inline-block`}
          >
            {capitalizeString(subject)} for Class {courseClass}: {title}
          </p>
          <br />
          <p
            className={`${utilStyles.body1} text-left text-cba-dark-gray inline-block`}
          >
            Duration: {durationInMonths} months
          </p>
          <br />
          <p
            className={`${utilStyles.body1} text-left text-cba-dark-gray inline-block`}
          >
            Starts: {moment(startDate).format('DD-MM-YYYY')}
          </p>
        </div>
        <MonthlyPaymentSchedule monthlyPayments={monthlyPayments} />
      </div>

      <div className='flex justify-center'>
        <CustomButton onClick={() => onClick()}>pay for 1 month</CustomButton>
      </div>
      <div className={utilStyles.divider} />

      <span
        className={`${utilStyles.caption} text-center block text-cba-dark-gray`}
      >
        Remaining payments in
      </span>
      <span
        className={`${utilStyles.caption} text-center block text-cba-dark-gray`}
      >
        {'My Profile > My Payments'}
      </span>
      <div className={utilStyles.divider} />
    </>
  );
}
