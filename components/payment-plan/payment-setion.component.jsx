import useSWR from 'swr';
import { get } from '../../services/helpers/helpers';
import { GET_BATCHES_URL } from '../../utils/api-urls.utils';

import OneTimePayment from './one-time/one-time.component';
import MonthlyPayment from './monthly/monthly.component';
import Loader from '../helper/loader/loader';
import Error from '../error/error.component';

import utilStyles from '../../styles/utils.module.css';

export default function PaymentSection({ paymentOption, slug }) {
  const { data: batches, getBatchError } = useSWR(
    `${GET_BATCHES_URL}?slug=${slug}`,
    get
  );

  if (process.env.NEXT_PUBLIC_IS_RELEASE_KEY_AVAILABE === 'false') {
    return (
      <h3 className={`${utilStyles.heading3} text-cba-yellow mt-32`}>
        <center>COMING SOON!</center>
      </h3>
    );
  }

  if (getBatchError) {
    return <Error />;
  }

  if (!batches) return <Loader />;

  const batch = batches[0];
  return (
    <section className='px-5 h-full mt-12 lg:px-32 xxl:w-65% xxl:mx-auto'>
      {paymentOption === 'one-time' ? (
        <OneTimePayment {...batch} />
      ) : (
        <MonthlyPayment {...batch} />
      )}
    </section>
  );
}
