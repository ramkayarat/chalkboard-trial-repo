import moment from 'moment';

import utilStyles from '../../../styles/utils.module.css';

export default function MonthlyPaymentSchedule({ monthlyPayments }) {
  return (
    <div className='mx-auto w-128 md:w-lg lg:w-1/3'>
      <h6 className={`${utilStyles.heading6} mb-4 text-left lg:text-center`}>
        Payment Schedule
      </h6>
      <ul>
        {monthlyPayments.map(({dueDate, amountInPaise}, i) => (
          <li
            className={`${utilStyles.body2} text-cba-dark-gray flex w-full justify-between`}
            style={{ marginBottom: '10px' }}
             key={i}
          >
            <p>Month {i+1} - By {moment(dueDate).format('DD-MM-YYYY')}:</p>
            <p>INR {amountInPaise/100}</p>
          </li>
        ))}
      </ul>
    </div>
  );
}
