import styles from './payment-option-switch.module.css';

export default function PaymentOptionSwitch({
  paymentOption,
  setPaymentOption,
}) {
  return (
    <div className={`${styles[`switch-container`]}`}>
      <button
        className={`${styles['switch-button']} ${styles['switch-left']} ${
            paymentOption === 'one-time' ? styles.active : ''
        }`}
        onClick={() => setPaymentOption('one-time')}
      >
        one-time
      </button>
      <button
        className={`${styles['switch-button']} ${styles['switch-right']} ${
          paymentOption === 'monthly' ? styles.active : ''
        }`}
        onClick={() => setPaymentOption('monthly')}
      >
        monthly
      </button>
    </div>
  );
}
