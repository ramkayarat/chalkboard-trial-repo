import { useRouter } from 'next/router';
import moment from 'moment';
import { useContext } from 'react';
import { JWTContext } from '../../../providers/jwt.provider';

import CustomButton from '../../helper/custom-button/custom-button';

import utilStyles from '../../../styles/utils.module.css';

import { capitalizeString } from '../../../utils/utils';
import { handlePayment } from '../../../utils/payment.utils';
import { InvoiceContext } from '../../../providers/invoice.provider';
import { AppRoutes } from '../../../constants/app-routes';
import { generateInvoice } from '../../../services/payment.service';
import { CheckoutContext } from '../../../providers/checkout.provider';

export default function OneTimePayment({
  startDate,
  durationInMonths,
  course,
  singlePaymentPriceInPaise,
  id: batchId,
  slug
}) {
  const token = useContext(JWTContext);
  const router = useRouter();
  const { toggleCheckoutPopUp, setCheckoutData } = useContext(CheckoutContext);

  const onClick = async () => {
    if (!token) {
      alert('login first');
      router.push(AppRoutes.LOGIN);
      return;
    }

    const invoice = await generateInvoice({ batchId, paymentPlan: 'one_time' });

    setCheckoutData({ course, invoice ,slug});
    toggleCheckoutPopUp();
  };

  const { subject, class: courseClass, title } = course;

  return (
    <div className='lg:flex lg:justify-center'>
      <div className='lg:flex lg:flex-col lg:justify-center h-auto mx-auto w-128 md:w-lg lg:w-1/4'>
        <p
          className={`${utilStyles.body1} text-left text-cba-dark-gray inline-block`}
        >
          {capitalizeString(subject)} for Class {courseClass}: {title}
        </p>
        <br className='lg:hidden' />
        <p
          className={`${utilStyles.body1} text-left text-cba-dark-gray inline-block`}
        >
          Duration: {durationInMonths} months
        </p>
        <br className='lg:hidden' />
        <p
          className={`${utilStyles.body1} text-left text-cba-dark-gray inline-block`}
        >
          Starts: {moment(startDate).format('DD-MM-YYYY')}
        </p>
      </div>
      <div className={utilStyles.divider} />
      <div className={utilStyles.divider} />

      <div className='lg:w-1/2'>
        <p className={`${utilStyles.overline} text-cba-disabled text-center`}>
          One-time Purchase Price:
        </p>
        <div className='lg:h-10' />
        <div className='flex w-full items-center justify-center'>
          <p className={`${utilStyles.overline} text-cba-disabled text-center`}>
            INR
          </p>
          <span className={`${utilStyles.heading4} m-0`}>
            {singlePaymentPriceInPaise / 100}
          </span>
        </div>

        <div className='flex justify-center'>
          <CustomButton onClick={() => onClick()}>proceed to pay</CustomButton>
        </div>
      </div>
    </div>
  );
}
