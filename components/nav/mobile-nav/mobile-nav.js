import Link from 'next/link';

import NavLink from '../nav-link/nav-link';

import styles from './mobile-nav.module.css';
import utilStyles from '../../../styles/utils.module.css';
import { AppRoutes } from '../../../constants/app-routes';

export default function MobileNav({
  mobileNavOpen,
  setMobileNavOpen,
  loggedIn,
}) {
  return (
    <div
      className={styles.mobileNav}
      style={{
        pointerEvents: `${mobileNavOpen ? 'auto' : 'none'}`,
        opacity: `${mobileNavOpen ? 1 : 0}`,
      }}
    >
      <div
        className='absolute top-0 right-0 cursor-pointer text-2xl mt-6 mr-5'
        onClick={() => setMobileNavOpen(false)}
      >
        &#10005;
      </div>
      <div className={styles['nav-links-wrap']}>
        <div className=' flex flex-col w-auto h-auto'>
          {!loggedIn ? (
            <NavLink
              route={AppRoutes.SIGN_UP}
              setMobileNavOpen={setMobileNavOpen}
              mobile
            >
              Log-in / Sign-up &#10095;
            </NavLink>
          ) : (
            <NavLink
              route={AppRoutes.MY_PROFILE}
              setMobileNavOpen={setMobileNavOpen}
              mobile
            >
              My Profile &#10095;
            </NavLink>
          )}
          <NavLink
            route={AppRoutes.BATCHES}
            setMobileNavOpen={setMobileNavOpen}
            mobile
          >
            Upcoming Batches &#10095;
          </NavLink>
          <NavLink
            route={AppRoutes.SUPPORT}
            setMobileNavOpen={setMobileNavOpen}
            mobile
          >
            Contact Support &#10095;
          </NavLink>
          {/* <NavLink
            route={AppRoutes.FOR_TEACHERS}
            setMobileNavOpen={setMobileNavOpen}
            mobile
          >
            For Teachers &#10095;
          </NavLink> */}
          <NavLink
            route={AppRoutes.FAQS}
            setMobileNavOpen={setMobileNavOpen}
            mobile
          >
            FAQ &#10095;
          </NavLink>
        </div>

        <div className='flex flex-col w-auto h-auto'>
          <Link href={AppRoutes.PRIVACY_POLICY}>
            <a
              className={`${utilStyles.subtitleS} text-center mb-2`}
              onClick={() => setMobileNavOpen(false)}
              target='_blank'
            >
              Privacy Policy
            </a>
          </Link>
          <Link href={AppRoutes.TERMS_OF_SERVICE}>
            <a
              className={`${utilStyles.subtitleS} text-center mb-2`}
              onClick={() => setMobileNavOpen(false)}
              target='_blank'
            >
              Terms of Service
            </a>
          </Link>
          <p
            className={`${utilStyles.subtitleS} text-center mb-2`}
            onClick={() => setMobileNavOpen(false)}
          >
            &#169; ChalkBoard Academy
          </p>
        </div>
      </div>
    </div>
  );
}
