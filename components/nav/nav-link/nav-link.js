import Link from 'next/link';
import { useRouter } from 'next/router';

import utilStyles from '../../../styles/utils.module.css';
import styles from './nav-link.module.css';

export default function NavLink({
  route,
  setMobileNavOpen,
  children,
  header,
  mobile,
  icon,
}) {
  const router = useRouter();

  return (
    <Link href={route}>
      <a
        className={`
      ${header ? styles.headerNav : ''}
      ${mobile ? `${utilStyles.heading5} text-cba-dark-gray text-center mb-5` : ''}
      ${icon ? styles['header-icon'] : ''}
      `}
        onClick={() => {
          setMobileNavOpen ? setMobileNavOpen(false) : null;
        }}
      >
        {children}
      </a>
    </Link>
  );
}
