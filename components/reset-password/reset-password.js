import ColumnLayout from '../helper/column-layout/column-layout';
import ResetPasswordForm from './input-column/reset-password-form';

import utilStyles from '../../styles/utils.module.css';
import styles from './reset-password.module.css';

export default function ResetPasswordSection() {
  
  return (
    <section className={styles.background}>
      <ColumnLayout>
        <h4 className={`${utilStyles.heading4} m-0 py-6`}>Reset Password</h4>
        <ResetPasswordForm />
      </ColumnLayout>
    </section>
  );
}
