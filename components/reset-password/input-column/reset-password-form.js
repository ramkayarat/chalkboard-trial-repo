import { useState } from 'react';
import {
  validateForm,
  validateInput,
} from '../../../services/form-validation.service';
import { resetPassword, login } from '../../../services/auth.service';

import CustomButton from '../../helper/custom-button/custom-button';

import utilStyles from '../../../styles/utils.module.css';
import { useRouter } from 'next/router';

export default function ResetPasswordForm() {
  const router = useRouter();
  const { code } = router.query;

  const [info, setInfo] = useState({
    email: '',
    password: '',
    confirmPassword: '',
  });
  const [formErrors, setFormErrors] = useState({
    email: '',
    password: '',
    confirmPassword: '',
  });

  const handleChange = (evt) => {
    const { id, value } = evt.target;
    setInfo({ ...info, [id]: value });

    setFormErrors(validateInput(id, value, { ...formErrors }, info.password));
  };

  const handleSubmit = async (evt) => {
    evt.preventDefault();

    if (validateForm(info, formErrors)) {
      const response = await resetPassword({ code, password: info.password });
    } else {
      alert('Entered values are invalid or form is empty');
    }
  };

  const { email, password, confirmPassword } = formErrors;

  return (
    <form
      className='flex flex-col w-auto'
      onSubmit={(evt) => handleSubmit(evt)}
    >
      <input
        type='email'
        id='email'
        onChange={(evt) => handleChange(evt)}
        className={`${utilStyles['text-field']} placeholder-cba-blue placeholder-opacity-50`}
        placeholder='Enter Email ID'
      />
      <span className={`${utilStyles.caption} text-cba-alert-red`}>
        {email !== '' ? email : null}
      </span>

      <input
        type='password'
        id='password'
        onChange={(evt) => handleChange(evt)}
        className={utilStyles['text-field'] + ' mt-3'}
        placeholder='Enter New Password'
      />
      <span className={`${utilStyles.caption} text-cba-alert-red`}>
        {password !== '' ? password : null}
      </span>

      <input
        type='password'
        id='confirmPassword'
        onChange={(evt) => handleChange(evt)}
        className={utilStyles['text-field'] + ' mt-3'}
        placeholder='Re-enter New Password'
      />
      <span className={`${utilStyles.caption} text-cba-alert-red mb-20`}>
        {confirmPassword !== '' ? confirmPassword : null}
      </span>

      <CustomButton type='submit'>confirm & log-in</CustomButton>
    </form>
  );
}
