import { URIs } from '../../../constants/image-uri';

import styles from './course-image.module.css';

export default function CourseImage({ featureImageUrl = '' }) {
  return (
    <div
      className={`${styles['image-bg']}`}
      style={{
        backgroundImage: `url(${featureImageUrl ? featureImageUrl : ''})`,
      }}
    >
      <div
        className={`${styles.wave}`}
        style={{ backgroundImage: `url(${URIs.WHITE_WAVE})` }}
      />
    </div>
  );
}
