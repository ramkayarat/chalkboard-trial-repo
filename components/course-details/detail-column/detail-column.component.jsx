import { CircularProgressbar, buildStyles } from 'react-circular-progressbar';

import BackButton from '../../helper/back-button/back-button';
import ColumnLayout from '../../helper/column-layout/column-layout';
import CustomButton from '../../helper/custom-button/custom-button';

import styles from './detail-column.module.css';
import utilStyles from '../../../styles/utils.module.css';
import { colors } from '../../../constants/colors';
import { capitalizeString } from '../../../utils/utils';
import Link from 'next/link';
import { AppRoutes, dynamicRoutes } from '../../../constants/app-routes';

export default function DetailsColumn({ batch, isSubscribed }) {
  const { seatsBooked, totalSeats, id: batchId, slug } = batch;
  const { board, subject, title, description } = batch.course;
  const { name, image, about } = batch.teacher;
  const {url:imageUrl,width,height} = image.formats.thumbnail;
  const percentageBooked = Math.round((seatsBooked / totalSeats) * 100);

  return (
    <div className=' mb-12 h-full lg:w-90vw xl:w-4/5 xxl:w-65% lg:mx-auto'>
      <section className='px-8 w-full md:px-12 lg:px-16 xl:px-32'>
        <ColumnLayout justify='start' align='start'>
          <BackButton href={AppRoutes.BATCHES} />
          {/* title and seats booked */}
          <div className='my-8 flex w-full justify-between items-center'>
            {/* batch title */}
            <div className='flex flex-col items-start'>
              <span
                className={`${utilStyles.overline} uppercase text-cba-blue`}
              >
                {`${board.replace(/_/g, ' ')} > ${subject} > class ${
                  batch.course.class
                }`}
              </span>
              <h6 className={`${utilStyles.heading6} text-cba-dark-gray m-0`}>
                {capitalizeString(subject)} for Class {batch.course.class}:{' '}
                {title}
              </h6>
            </div>

            {/* circular progressbar */}
            <div className='lg:w-1/2'>
              <div
                className={`${styles['circular-progressbar']} md:ml-4 lg:ml-10 xl:ml-16`}
              >
                <CircularProgressbar
                  value={percentageBooked}
                  text={`${percentageBooked}%`}
                  styles={buildStyles({
                    textColor: colors.CBA_DARK_GRAY,
                    textSize: '20px',
                    pathColor:
                      percentageBooked === 100
                        ? colors.CBA_ERROR_RED
                        : colors.CBA_BLUE,
                  })}
                />
                <span className={`${utilStyles.overline} text-cba-disabled`}>
                  booked
                </span>
              </div>
            </div>
          </div>
        </ColumnLayout>
        {/* description and teacher details */}
        <div className='md:flex md:items-center'>
          {/* description */}
          <p
            className={`${utilStyles.body2} text-justify text-cba-dark-gray md:w-1/2`}
          >
            {description}
          </p>
          <div className='h-16 md:w-10 xl:w-16' />
          {/* teacher details */}
          <ColumnLayout>
            <h6
              className={`${utilStyles.heading6} w-full text-left text-cba-dark-gray`}
            >
              Meet your Teacher
            </h6>
            <div className={styles['teacher-detail-row']}>
              <div
                className={`${styles['teacher-img']}`}
                style={{ backgroundImage: `url(${imageUrl})` }}
              />
              <ColumnLayout width='full' justify='end' align='start'>
                <p className={`${utilStyles.body1} text-cba-dark-gray`}>
                  {name}
                </p>
                <span className={`${utilStyles.overline} text-cba-dark-gray`}>
                  {about}
                </span>
              </ColumnLayout>
            </div>
            {/* enroll button */}
            {!isSubscribed && (
              <div className='mx-auto text-center'>
                <Link
                  href={{
                    pathname: `${AppRoutes.BATCHES}/[${dynamicRoutes.SLUG}]${AppRoutes.PAYMENT_PLAN}`,
                    query: { id: batchId },
                  }}
                  as={`${AppRoutes.BATCHES}/${slug}${AppRoutes.PAYMENT_PLAN}`}
                >
                  <a>
                    <CustomButton disabled={percentageBooked === 100}>
                      {percentageBooked === 100
                        ? 'check back later'
                        : 'enroll now'}
                    </CustomButton>
                  </a>
                </Link>
              </div>
            )}
          </ColumnLayout>
        </div>
      </section>
    </div>
  );
}
