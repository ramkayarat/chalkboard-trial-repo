import moment from 'moment';

import ColumnLayout from '../../helper/column-layout/column-layout';

import utilStyles from '../../../styles/utils.module.css';
import styles from './schedule-section.module.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faCalendarDay,
  faPlaneArrival,
  faPlaneDeparture,
  faStopwatch,
} from '@fortawesome/free-solid-svg-icons';
import { colors } from '../../../constants/colors';

export default function Schedule({
  durationInMonths,
  startDate,
  endDate,
  weekConfiguration,
}) {
  return (
    <div className='bg-cba-dark-gray bg-opacity-5'>
      <section className={`${styles['schedule-section']}`}>
        <div className='grid grid-cols-2 w-full md:w-1/2 h-auto gap-6'>
          <ScheduleItem
            img={faStopwatch}
            overline='duration'
            text={`${durationInMonths} month${durationInMonths > 1 ? 's' : ''}`}
          />
          <ScheduleItem
            img={faCalendarDay}
            overline='sessions'
            text={`${weekConfiguration.length} per week`}
          />
          <ScheduleItem
            img={faPlaneDeparture}
            overline='start date'
            text={`${moment(startDate).format('DD-MM-YYYY')}`}
          />
          <ScheduleItem
            img={faPlaneArrival}
            overline='end date'
            text={`${moment(endDate).format('DD-MM-YYYY')}`}
          />
        </div>

        {/* divider of 60px */}
        <div className={utilStyles.divider} />
        <div className={utilStyles.divider} />
        <div className={utilStyles.divider} />
        <div className='lg:w-1/3 md:ml-6 lg:ml-10 xl:ml-16'>
          <ColumnLayout>
            <h6
              className={`${utilStyles.heading6} m-0 text-cba-dark-gray w-full`}
            >
              Your Study Plan
            </h6>

            {/* spacer of 20px */}
            <div className={utilStyles.divider} />
            {weekConfiguration.map((config) => (
              <StudyPlanItem {...config} key={config.id} />
            ))}
          </ColumnLayout>
        </div>
      </section>
    </div>
  );
}

const ScheduleItem = ({ img, overline, text }) => (
  <div className={styles['schedule-item']}>
    <div className={`${styles['item-img']}`}>
      <FontAwesomeIcon icon={img} color={colors.CBA_BLUE} className='w-8 h-8' />
    </div>
    <ColumnLayout width='auto' justify='center' align='start'>
      <span className={`${utilStyles.overline} text-cba-disabled`}>
        {overline}
      </span>
      <h6 className={`${utilStyles.heading6} m-0 text-cba-dark-gray`}>
        {text}
      </h6>
    </ColumnLayout>
  </div>
);

const StudyPlanItem = ({ weekday, startTime, endTime }) => (
  <div className='grid grid-cols-2 w-full h-auto gap-6 lg:gap-0 mb-3'>
    <p className={`${utilStyles.body2} text-cba-dark-gray`}>
      Every {weekday.toUpperCase()} -
    </p>
    <p className={`${utilStyles.body2} text-cba-dark-gray`}>
      {moment(startTime, 'HH:mm:ss').format('LT')} -{' '}
      {moment(endTime, 'HH:mm:ss').format('LT')}
    </p>
  </div>
);
