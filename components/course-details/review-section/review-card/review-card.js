import styles from './review-card.module.css';
import utilStyles from '../../../../styles/utils.module.css'

export default function ReviewCard({name, text}) {
  return (
    <div className={styles.card}>
      <span className='mb-1 text-xs text-cba-yellow font-secondary'>
        {name}
      </span>
      <p className={`${utilStyles.body2} text-left text-cba-dark-gray`}>
        {text}
      </p>
    </div>
  );
}
