import Link from 'next/link';

import ReviewCard from './review-card/review-card';
import CustomButton from '../../helper/custom-button/custom-button';

import styles from './review-section.module.css';
import utilStyles from '../../../styles/utils.module.css';
import { AppRoutes, dynamicRoutes } from '../../../constants/app-routes';

export default function ReviewSection({
  reviews,
  totalSeats,
  seatsBooked,
  slug,
  id: batchId,
  isSubscribed,
}) {
  const percentageBooked = Math.round((seatsBooked / totalSeats) * 100);

  return (
    <section className='mt-8 px-8 xl:px-32'>
      <h6 className={`${utilStyles.heading6} text-left m-0 text-cba-dark-gray`}>
        {reviews.length > 0 ? 'Top Reviews' : ''}
      </h6>
      <div className='flex h-auto justify-center lg:justify-evenly lg:w-full mt-3'>
        <div
          className={`${styles['reviews-wrapper']} lg:grid lg:grid-cols-3 lg:row-gap-6 lg:w-90% xl:w-4/5 xxl:w-2/3`}
        >
          {reviews.map((review, i) => (
            <ReviewCard {...review} key={i} />
          ))}
        </div>
      </div>
      <span className={`${styles['booking-info']}`}>
        {percentageBooked}% seats filled
      </span>
      <div className='text-center w-full mb-5'>
        {!isSubscribed && (
          <Link
            href={{
              pathname: `${AppRoutes.BATCHES}/[${dynamicRoutes.SLUG}]${AppRoutes.PAYMENT_PLAN}`,
              query: { id: batchId },
            }}
            as={`${AppRoutes.BATCHES}/${slug}${AppRoutes.PAYMENT_PLAN}`}
          >
            <a>
              <CustomButton disabled={percentageBooked === 100}>
                {percentageBooked === 100 ? 'check back later' : 'enroll now'}
              </CustomButton>
            </a>
          </Link>
        )}
      </div>
    </section>
  );
}
