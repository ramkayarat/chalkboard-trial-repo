import Link from 'next/link';
import { useRouter } from 'next/router';
import { AppRoutes } from '../../constants/app-routes';
import { URIs } from '../../constants/image-uri';

import utilStyles from '../../styles/utils.module.css';
import CustomButton from '../helper/custom-button/custom-button';
import Loader from '../helper/loader/loader'

export default function Error({ paymentError,slug }) {
  const router = useRouter();
  const errorText = `Failed to Fetch ${paymentError ? 'invoice' : 'data'}`;
  const buttonText = `Back to ${paymentError ? 'checkout' : 'safety'}`;

  return (
    <div
      className={`${utilStyles['yellow-pattern-background']} relative h-screen px-10 w-screen max-w-full flex flex-col justify-center items-center`}
    >
      <img src={URIs.LOGO_URI} width="150" />
      {/* 20px * 4 spacer */}
      <div className={utilStyles.divider} />
      <div className={utilStyles.divider} />
      <h4
        className={`${utilStyles.heading4} m-0 text-center text-cba-alert-red`}
      >
        Error!
      </h4>
      <div className={utilStyles.divider} />
      <p className={`${utilStyles.body1} text-black text-center`}>
        {errorText}
      </p>
      <div className='absolute bottom-0 mb-8 w-screen max-w-full text-center'>
        {!paymentError?<CustomButton onClick={() => router.back()}>{buttonText}</CustomButton>:
        <CustomButton onClick={() => router.push(`/batches/${slug}/payment-plan`)}>{buttonText}</CustomButton>}
        {paymentError ? null : (
          <Link href={AppRoutes.HOME}>
            <p
              className={`${utilStyles.caption} mt-5 text-cba-blue text-center`}
            >
              Go to Homepage
            </p>
          </Link>
        )}
      </div>
    </div>
  );
}
