import { useRouter } from "next/router";
import { AppRoutes } from "../../../constants/app-routes";

import { getPaymentsByBatchId } from "../../../utils/payment.utils";

import CourseCard from "../../helper/course-card/course-card";
import CustomButton from "../../helper/custom-button/custom-button";

export default function CoursesSection({ subscriptions, payments }) {
  const route = useRouter();
  return (
    <>
      <div className="grid grid-cols-1 lg:row-gap-20 md:grid-cols-2 lg:grid-cols-3 w-full md:w-95% xl:w-4/5 xxl:w-2/3 mx-auto">
        {subscriptions &&
          subscriptions.map((subscription) => {
            let payment = getPaymentsByBatchId(payments, subscription.batchId);

            return (
              <CourseCard
                batch={subscription}
                status={payment.status}
                key={subscription.id}
              />
            );
          })}
      </div>
      <div className="text-center w-full lg:mt-32">
        <CustomButton
          onClick={() => {
            route.push(AppRoutes.BATCHES);
          }}
        >
          {`explore ${subscriptions <= 0 || !subscriptions ? "" : "more"}
           batches`}
        </CustomButton>
      </div>
    </>
  );
}
