import { myCourseTypes } from '../../../constants/my-courses.type';

import styles from './my-courses-switch.module.css';

export default function MyCoursesSwitch({ activeCourses, setActiveCourses }) {
  return (
    <div className={`${styles[`switch-container`]}`}>
      <button
        className={`${styles['switch-button']} ${styles['switch-left']} ${
          activeCourses === myCourseTypes.ON_GOING ? styles.active : ''
        }`}
        onClick={() => setActiveCourses(myCourseTypes.ON_GOING)}
      >
        On-going
      </button>
      <button
        className={`${styles['switch-button']} ${styles['switch-right']} ${
          activeCourses === myCourseTypes.PAST ? styles.active : ''
        }`}
        onClick={() => setActiveCourses(myCourseTypes.PAST)}
      >
        past
      </button>
    </div>
  );
}
