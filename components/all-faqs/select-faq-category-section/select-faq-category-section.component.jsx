import styles from './select-faq-category-section.module.css';

export default function SelectFaqCategorySection({
  selectedCategory,
  setCategory,
}) {
  return (
    <div className='flex justify-center mx-auto my-8 w-4/5 md:w-1/2'>
      <button
        className={
          styles['select-category-button'] +
          ' rounded-l-md border-r-0 ' +
          (selectedCategory == 'students'
            ? styles['selected-category-button']
            : styles['unselected-category-button'])
        }
        onClick={() => {
          setCategory('students');
        }}
      >
        For Students
      </button>
      <button
        className={
          styles['select-category-button'] +
          ' rounded-r-md border-l-0 ' +
          (selectedCategory == 'teachers'
            ? styles['selected-category-button']
            : styles['unselected-category-button'])
        }
        onClick={() => {
          setCategory('teachers');
        }}
      >
        For Teachers
      </button>
    </div>
  );
}
