import FAQBlock from '../../home/faq-section/faq-block/faq-block.component';

export default function SelectedCategoryFaqs({ faqs }) {
  return (
    <div className='lg:flex lg:flex-wrap lg:justify-center lg:w-4/5'>
      {faqs && faqs.map((faq, i) => (
        <FAQBlock {...faq} i={i} key={i} />
      ))}
    </div>
  );
}
