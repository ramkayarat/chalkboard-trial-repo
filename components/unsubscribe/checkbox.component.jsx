import utilStyles from '../../styles/utils.module.css'

export default function Checkbox({handleChange, isChecked}){
    return(<div className='flex justify-center items-start my-5 w-128 xs:mx-auto'>
    <label>
      <input
        type='checkbox'
        className='border-cba-blue border border-solid'
        id='declaration'
        value='unsubscribe'
        checked={isChecked}
        onChange={(evt) => {
          handleChange(evt);
        }}
      />
    </label>
    <span className={`${utilStyles.caption} text-cba-dark-gray ml-3`}>
      {' '}
      I understand the consequences & I wish to remove my access to this
      course.
    </span>
  </div>)
}