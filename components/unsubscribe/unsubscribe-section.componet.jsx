import useSWR from 'swr';
import { useContext, useState } from 'react';
import { get } from '../../services/helpers/helpers';
import { cancelSubscription } from '../../services/payment.service';
import { UnsubscribeContext } from '../../providers/unsubscribe.provider';
import { PopUpContext } from '../../providers/pop-up.provider';

import CustomButton from '../helper/custom-button/custom-button';
import UnsubscribeWarnings from './unsubscribe-warnings.component';
import Input from './input.component';
import Checkbox from './checkbox.component';
import SuccessPopUp from '../pop-ups/success/success.component';

import utilStyles from '../../styles/utils.module.css';
import styles from './unsubscribe-section.module.css';
import { GET_BATCHES_URL } from '../../utils/api-urls.utils';
import { popUpStates } from '../../constants/pop-up-state.types';

export default function UnsubscribeSection() {
  const [isChecked, setIsChecked] = useState(false);
  const { togglePopUpState } = useContext(PopUpContext);
  const { toggleVisible, batchId } = useContext(UnsubscribeContext);
  const { data: batch, getBatchError } = useSWR(
    `${GET_BATCHES_URL}/${batchId}`,
    get
  );

  const handleChange = (evt) => {
    setIsChecked(evt.target.checked);
  };

  const handleSubmit = async (evt) => {
    try {
      evt.preventDefault();

      const inputText = document.getElementById('unsubscribe');
      if (!inputText) return alert('Please enter "Unsubscribe"');

      if (inputText.value === 'Unsubscribe') {
        const res = await cancelSubscription({ batchId });

        if (res === 'success') {
          toggleVisible();
          togglePopUpState(popUpStates.UNSUBSCRIBE_SUCCESS);
        }
        return;
      }

      alert('Please enter "Unsubscribe"');
    } catch (error) {
      alert('Something went wrong! Please try again later.');
      toggleVisible();
    }
  };

  return (
    <div className={`${styles['unsubscribe-section-container']}`}>
      <section className={`${styles['unsubscribe-section']}`}>
        <div className='relative md:w-4/5 mx-auto'>
          <h4
            className={`${utilStyles.heading4} lg:mb-8 text-center text-cba-dark-gray`}
          >
            Are You Sure?
          </h4>
          <p
            className={`hidden md:block absolute top-0 cursor-pointer text-cba-blue mt-5 uppercase`}
            onClick={() => toggleVisible()}
          >
            &#10096; Back
          </p>
        </div>
        <p className={`${utilStyles.body1} text-center text-cba-dark-gray`}>
          If you unsubscribe from
        </p>
        {batch && (
          <h6
            className={`${utilStyles.heading6} text-center m-0 text-cba-dark-gray`}
          >
            {batch.course.subject} for Class {batch.course.class}:{' '}
            {batch.course.title}
          </h6>
        )}

        <div className='lg:flex lg:justify-center lg:items-center lg:px-5'>
          <UnsubscribeWarnings />
          <Input />
        </div>

        <Checkbox handleChange={handleChange} isChecked={isChecked} />
        <div className='w-full text-center'>
          <CustomButton
            alert={isChecked}
            disabled={!isChecked}
            onClick={(evt) => {
              handleSubmit(evt);
            }}
          >
            unsubscribe
          </CustomButton>
        </div>
      </section>
    </div>
  );
}
