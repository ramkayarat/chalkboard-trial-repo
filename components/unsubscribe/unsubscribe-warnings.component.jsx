import utilStyles from '../../styles/utils.module.css'

export default function UnsubscribeWarnings(){
    return(
        <ul className='my-12 w-128 mx-auto lg:w-1/2'>
          <li className={`${utilStyles.body2} text-cba-blue`}>
            -You will lose all access to the course.
          </li>
          <li className={`${utilStyles.body2} text-cba-blue`}>
            -Your pending payments will be cancelled.
          </li>
          <li className={`${utilStyles.body2} text-cba-blue`}>
            -You will have to restart from Month 1 in the next batch, should you
            want to retake this course.
          </li>
        </ul>
    )
}