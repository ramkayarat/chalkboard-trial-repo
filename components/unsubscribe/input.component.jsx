import utilStyles from '../../styles/utils.module.css'

export default function Input() {
  return (
    <div className='lg:flex lg:flex-col lg:w-1/2'>
      <p className={`${utilStyles.body2} text-cba-dark-gray text-center pb-3`}>
        Please type “<strong>Unsubscribe</strong>” to confirm.
      </p>
      <div className='w-full text-center'>
        <input
          type='text'
          id='unsubscribe'
          className={`${utilStyles['text-field']} placeholder-cba-blue placeholder-opacity-50`}
          placeholder='Type here'
        />
      </div>
    </div>
  );
}
