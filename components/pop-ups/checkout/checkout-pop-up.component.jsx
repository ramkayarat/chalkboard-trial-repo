import styles from './checkout-pop-up.module.css';
import utilStyles from '../../../styles/utils.module.css';
import { CheckoutContext } from '../../../providers/checkout.provider';
import { useContext } from 'react';
import { capitalizeString } from '../../../utils/utils';
import CustomCardButton from '../../helper/custom-card-button/custom-card-button';
import { AppRoutes } from '../../../constants/app-routes';
import {
  FULFILL_ORDER_URL,
  FULFILL_PENDING_PAYMENT_URL,
} from '../../../utils/api-urls.utils';

function CheckoutPopUp() {
  const { checkoutData, toggleCheckoutPopUp, setCheckoutData } = useContext(
    CheckoutContext
  );
  const { subject, class: courseClass, title } = checkoutData.course;
  const {
    hash,
    key,
    txnId,
    amount,
    firstName,
    productInfo,
    email,
    phoneNumber,
  } = checkoutData.invoice;

  const { isPendingPayment = false,paymentId="" ,slug} = checkoutData;

  const payuUrl = process.env.NEXT_PUBLIC_PAYU_URL;
  const clientUrl = process.env.NEXT_PUBLIC_CLIENT_BASE_URL;
  const strapiUrl = process.env.NEXT_PUBLIC_CSR_STRAPI_BASE_URL;
  return (
    <div className={styles['pop-up-container']}>
      <div className={styles['pop-up']}>
        <h5 className={`${utilStyles.heading5} text-cba-yellow text-center`}>
          Details
        </h5>
        <div className={`${utilStyles.body2} text-cba-dark-gray text-left`}>
          <p>
            <strong className='text-cba-blue'>Course: </strong>
            {capitalizeString(subject)} for Class {courseClass}: {title}
          </p>
          <p>
            <strong className='text-cba-blue'>Name: </strong>
            {firstName}
          </p>
          <p>
            <strong className='text-cba-blue'>Phone Number: </strong>
            {phoneNumber}
          </p>
          <p>
            <strong className='text-cba-blue'>Email ID: </strong>
            {email}
          </p>
        </div>
        <h6 className={`${utilStyles.heading6} text-cba-dark-gray text-center`}>
          INR {amount / 100}
        </h6>
        <form action={payuUrl} method='post'>
          <input value={hash} id='hash' name='hash' type='hidden' />
          <input value={txnId} id='txnid' name='txnid' type='hidden' />
          <input type='hidden' id='key' name='key' value={key} />
          <input
            type='hidden'
            id='firstname'
            name='firstname'
            value={firstName}
          />
          <input type='hidden' id='email' name='email' value={email} />
          <input type='hidden' id='phone' name='phone' value={phoneNumber} />
          <input
            type='hidden'
            id='amount'
            name='amount'
            value={(amount / 100).toString()}
          />
          <input
            type='hidden'
            id='productinfo'
            name='productinfo'
            value={productInfo}
          />
          <input type='hidden' id='udf1' name='udf1' value={paymentId} />
          <input
            type='hidden'
            id='surl'
            name='surl'
            value={`${strapiUrl}${
              isPendingPayment ? FULFILL_PENDING_PAYMENT_URL : FULFILL_ORDER_URL
            }`}
          />
          <input
            type='hidden'
            id='furl'
            name='furl'
            value={`${clientUrl}${AppRoutes.ERROR}?paymentError=true&slug=${slug}`}
          />
          <input
            type='hidden'
            id='curl'
            name='curl'
            value={`${clientUrl}${AppRoutes.BATCHES}/${slug}${AppRoutes.PAYMENT_PLAN}`}
          />
          <CustomCardButton type='submit'>checkout</CustomCardButton>
          <span
            className={`${utilStyles.caption} block text-center text-cba-blue cursor-pointer mt-3`}
            onClick={() => {
              toggleCheckoutPopUp();
              setCheckoutData(null);
            }}
          >
            Go Back
          </span>
        </form>
      </div>
    </div>
  );
}

export default CheckoutPopUp;
