import { useContext } from 'react';
import { PopUpContext } from '../../../providers/pop-up.provider';

import styles from './error.module.css';
import utilStyles from '../../../styles/utils.module.css';

export default function ErrorPopUp({ text }) {
  const { togglePopUpState } = useContext(PopUpContext);

  return (
    <div className={styles['pop-up-container']}>
      <div className={styles['pop-up']}>
        <h6
          className={`${utilStyles.heading6} m-0 text-center text-cba-alert-red`}
        >
          Error!
        </h6>
        <p className={`${utilStyles.body1} text-center text-cba-dark-gray`}>
          {text}
        </p>
        <span
          onClick={() => togglePopUpState(null)}
          className={`${utilStyles.caption} cursor-pointer text-cba-blue text-center`}
        >
          Close this pop-up
        </span>
      </div>
    </div>
  );
}
