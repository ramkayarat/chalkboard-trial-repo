import { useContext, useState } from 'react';

import CustomButton from '../../helper/custom-button/custom-button';
import { AdminNoteContext } from '../../../providers/admin.provider';

import styles from './admin-note.module.css';
import utilStyles from '../../../styles/utils.module.css';

export default function AdminNote() {
  const { toggleNoteOpen, handleTask } = useContext(AdminNoteContext);

  const [input, setInput] = useState({ note: '' });

  const handleChange = (evt) => {
    const { id, value } = evt.target;

    setInput({ ...input, [id]: value });
  };

  const handleSubmit = (evt) => {
    evt.preventDefault();

    handleTask(input.note);
    toggleNoteOpen();
  };

  return (
    <div className='w-screen max-w-full h-screen bg-black bg-opacity-50 fixed'>
      <div className={styles['pop-up']}>
        <form onSubmit={(evt) => handleSubmit(evt)}>
          <textarea
            placeholder='Note'
            onChange={(evt) => handleChange(evt)}
            className={`${utilStyles['text-area']} placeholder-cba-blue placeholder-opacity-50`}
            id='note'
          />
          <div className='text-center mt-5'>
            <CustomButton type='submit'>submit</CustomButton>
          </div>
        </form>
      </div>
    </div>
  );
}
