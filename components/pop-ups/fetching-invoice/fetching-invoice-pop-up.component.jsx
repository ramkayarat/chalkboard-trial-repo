import { useContext, useEffect, useState } from 'react';
import { download } from '../../../services/download.service';
import FileSaver from 'file-saver';

import { InvoiceContext } from '../../../providers/invoice.provider';
import { CircularProgressbar, buildStyles } from 'react-circular-progressbar';
import SuccessPopUp from '../success/success.component';

import styles from './fetching-invoice-pop-up.module.css';
import utilStyles from '../../../styles/utils.module.css';
import { colors } from '../../../constants/colors';
import { PopUpContext } from '../../../providers/pop-up.provider';
import { popUpStates } from '../../../constants/pop-up-state.types';

export default function FechingInvoicePopUp() {
  const [progress, setProgress] = useState(0);
  const [isDownloading, setIsDownloading] = useState(false);
  const { invoiceUrl, setInvoiceUrl } = useContext(InvoiceContext);
  const { togglePopUpState } = useContext(PopUpContext);

  useEffect(() => {
    if (invoiceUrl) {
      setIsDownloading(true);
      handleDownload();
    }
  }, [invoiceUrl]);

  const handleDownload = async () => {
    try {
      const res = await download({ setProgress, invoiceUrl });
      if (res.status == 200) {
        togglePopUpState(popUpStates.SUCCESS);
        FileSaver.saveAs(res.data, 'invoice');
        setInvoiceUrl(null);
      }
    } catch (err) {
      togglePopUpState(popUpStates.ERROR);
    }
  };

  return (
    <div className={styles['pop-up-container']}>
      <div className={styles['pop-up']}>
        <h6
          className={`${utilStyles.heading6} m-0 text-center text-cba-dark-gray`}
        >
          {isDownloading ? 'Downloading' : 'Requesting'} invoice ...
        </h6>
        <div className='h-20 w-20'>
          <CircularProgressbar
            text={`${progress}%`}
            value={progress}
            styles={buildStyles({
              textColor: colors.CBA_DAR_GRAY,
              pathColor: isDownloading ? colors.CBA_YELLOW : colors.CBA_BLUE,
            })}
          />
        </div>
        <p className={`${utilStyles.caption} text-cba-dark-gray text-center`}>
          Don’t press Back button
        </p>
      </div>
    </div>
  );
}
