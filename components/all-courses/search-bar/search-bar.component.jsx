import styles from './search-bar.module.css';
import { URIs } from '../../../constants/image-uri';

export default function SearchBar() {
  return (
    <div className='w-64 h-12 relative lg:mx-auto'>
      <input
        type='text'
        className={`${styles['search-input']} placeholder-cba-blue placeholder-opacity-50`}
        placeholder='Search Courses'
      />
      <img
        src={URIs.SEARCH_ICON}
        className={styles.searchIcon}
      />
    </div>
  );
}
