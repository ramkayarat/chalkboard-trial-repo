import CourseCard from '../../helper/course-card/course-card';

import utilStyles from '../../../styles/utils.module.css';

export default function AllCoursesSection({ batches, filteredBatches }) {
  if (batches <= 0) {
    return (
      <h3 className={`${utilStyles.heading3} text-cba-yellow mt-32`}>
        <center>COMING SOON!</center>
      </h3>
    );
  }

  if (filteredBatches <= 0) {
    return <ShowEmpty />;
  }

  return (
    <div className='grid grid-cols-1 lg:row-gap-20 md:grid-cols-2 lg:grid-cols-3 w-full md:w-95% xl:w-4/5 xxl:w-65% mx-auto lg:mb-40'>
      {filteredBatches.map((batch) => (
        <CourseCard batch={batch} key={batch.id} />
      ))}
    </div>
  );
}

const ShowEmpty = () => {
  return (
    <div className='w-full'>
      <h3
        className={`${utilStyles.heading3} text-cba-yellow text-center capitalize`}
      >
        Oops!
      </h3>
      <h6 className={`${utilStyles.heading6} text-cba-dark-gray text-center`}>
        No courses match your selections
      </h6>
    </div>
  );
};
