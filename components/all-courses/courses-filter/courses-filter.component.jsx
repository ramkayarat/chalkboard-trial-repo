import DropDown from '../../drop-down/drop-down';
import SearchBar from '../search-bar/search-bar.component';

import utilStyles from '../../../styles/utils.module.css';
import DateFilter from '../../helper/calender/calender';

export default function CoursesFilter({
  filterOptions,
  toggleSelected,
  selectedBoard,
  selectedClass,
  selectedSubject,
  selectedDate,
}) {
  return (
    <>
      <div className='w-full flex flex-col items-center'>
        {/* spacer of 20px */}
        <div className={utilStyles.divider} />
        <div className='flex flex-col md:flex-row w-full items-center justify-center'>
          <DropDown
            list={filterOptions.educationBoard}
            title='Board'
            temporaryTitle={selectedBoard}
            toggleItem={toggleSelected}
          />
          {/* spacer */}
          <div className='h-5 md:w-5 md:h-0 lg:w-12' />
          <DropDown
            list={filterOptions.class}
            title='Class'
            toggleItem={toggleSelected}
            temporaryTitle={selectedClass}
          />
        </div>
        {/* spacer */}
        <div className='h-5 md:w-0 lg:w-0' />

        <div className='flex flex-col md:flex-row w-full items-center justify-center '>
          <DropDown
            list={filterOptions.subject}
            title='Subject'
            toggleItem={toggleSelected}
            temporaryTitle={selectedSubject}
          />
          {/* spacer */}
          <div className='h-5 md:w-5 md:h-0 lg:w-12' />
          <DateFilter
            title='Starting after'
            toggleItem={toggleSelected}
            temporaryDate={selectedDate}
          />
        </div>
      </div>
    </>
  );
}
