import Link from 'next/link';
import validator from 'validator';
import { useState, useContext } from 'react';
import { forgotPassword } from '../../services/auth.service';
import { popUpStates } from '../../constants/pop-up-state.types';

import CustomButton from '../helper/custom-button/custom-button';
import SuccessPopUp from '../pop-ups/success/success.component';
import { PopUpContext } from '../../providers/pop-up.provider';

import utilStyles from '../../styles/utils.module.css';
import styles from './forgot-password-section.module.css';
import { AppRoutes } from '../../constants/app-routes';

export default function ForgotPasswordSection() {
  const [info, setInfo] = useState({ email: '' });
  const {togglePopUpState, popUpState} = useContext(PopUpContext)

  const handleChange = (evt) => {
    const { id, value } = evt.target;

    setInfo({ ...info, [id]: value });
  };

  const handleSubmit = async (evt) => {
    evt.preventDefault();

    if (info.email !== '' || validator.isEmail(info.email)) {
      const res = await forgotPassword(info.email);
      if(res.ok){
        togglePopUpState(popUpStates.SUCCESS)
      }
    } else {
      //TODO: manage some error
      alert('Please enter correct email');
    }
  };

  if(popUpState === popUpStates.SUCCESS){
    return <SuccessPopUp text='Password confirmation link is sent to your email' />
  }

  return (
    <div className={styles['forgot-password-section']}>
      <h4
        className={`${utilStyles.heading4} md:text-5xl lg:text-5.6xl m-0 text-center text-cba-dark-gray py-8`}
      >
        Forgot Password
      </h4>
      <div className={`h-20`} />
      <form
        id='forgot-password-form'
        onSubmit={(evt) => handleSubmit(evt)}
        className='flex flex-col justify-center items-center'
      >
        <input
          type='email'
          id='email'
          onChange={(evt) => handleChange(evt)}
          className={`
            ${utilStyles['text-field']} mb-20 placeholder-cba-blue placeholder-opacity-50`}
          placeholder='My Email ID'
          required
        />
        <CustomButton type='submit'>send password reset link</CustomButton>
      </form>
      <Link href={AppRoutes.LOGIN}>
        <a
          className={`${utilStyles.caption} text-cba-blue text-center block mt-3`}
        >
          Back to Log-in
        </a>
      </Link>
    </div>
  );
}
