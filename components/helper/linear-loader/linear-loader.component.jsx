import styles from './linear-loader.module.css';

function LinearLoader() {
  return (
    <div className={`${styles['loader-container']}`}>
      <div className={`${styles.loader}`} />
    </div>
  );
}
export default LinearLoader;