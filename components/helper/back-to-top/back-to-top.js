import { URIs } from '../../../constants/image-uri';
import { colors } from '../../../constants/colors';

import { FaChevronUp } from 'react-icons/fa';

import styles from './back-to-top.module.css';
import utilStyles from '../../../styles/utils.module.css';

export default function BackToTop() {
  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      left: 0,
      behavior: 'smooth',
    });
  };

  return (
    <div className={styles['back-to-top']} onClick={() => scrollToTop()}>
      <FaChevronUp size={22} color={colors.CBA_BLUE} />
      <span className={`${utilStyles.caption} text-cba-blue text-center`}>
        Back to Top
      </span>
    </div>
  );
}
