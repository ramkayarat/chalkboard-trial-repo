import { useEffect, useState } from 'react';

import { FaCalendarDay } from 'react-icons/fa';
import Calendar from 'react-calendar';
import moment from 'moment';

import styles from './calender.module.css';

export default function DateFilter({ title, toggleItem, temporaryDate }) {
  const [headerTitle, setHeaderTitle] = useState(title);
  const [isOpen, setIsOpen] = useState(false);
  const [activeDate, setActiveDate] = useState(new Date());

  useEffect(() => {
    if (temporaryDate) {
      setHeaderTitle(moment(temporaryDate).format('DD-MM-YYYY'));
    } else {
      setHeaderTitle(title);
    }
  }, [temporaryDate]);

  const toggleOpen = () => {
    setIsOpen(!isOpen);
  };

  return (
    <div className='md:flex md:flex-col relative'>
      <div className={`${styles['dd-header']}`} onClick={() => toggleOpen()}>
        <div className={styles['dd-title']}>{headerTitle}</div>
        <div className={styles['arrow-bg']}>
          <FaCalendarDay color='white' size={28} />
        </div>
      </div>
      {isOpen && (
        <div className={`${styles['dd-list']} lg:mx-auto`}>
          <Calendar
            value={activeDate}
            onChange={(date) => {
              let formatedDate = moment(date).format('DD-MM-YYYY');
              setHeaderTitle(
                formatedDate === headerTitle ? title : formatedDate
              );
              setActiveDate(date === activeDate ? new Date() : date);
              toggleOpen();
              toggleItem({ key: 'date', value: moment(date).toISOString() });
            }}
          />
        </div>
      )}
    </div>
  );
}
