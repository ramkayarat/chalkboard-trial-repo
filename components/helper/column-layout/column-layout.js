import styles from './column-layout.module.css';

export default function ColumnLayout({ children, justify, align, width, height }) {
  /*justify will allow column to justify content w.r.t. justify value,
  if no value provided defaults to center */
  return (
    <div
      className={`${styles.column} 
      justify-${justify ? justify : 'center'} 
      items-${align ? align : 'center'} 
      w-${width ? width : 'auto'} 
      h-${height ? height : 'auto'}`}
    >
      {children}
    </div>
  );
}
