import { FaChevronUp } from 'react-icons/fa';

import styles from './admin-back-to-top.module.css';

export default function AdminBackToTop({ isPageScrolled }) {
  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      left: 0,
      behavior: 'smooth',
    });
  };

  return (
    <div className='text-center my-5 lg:hidden'>
      <button className={styles['back-to-top']} onClick={() => scrollToTop()}>
        <center><FaChevronUp className='w-8 h-8' /></center>
      </button>
    </div>
  );
}
