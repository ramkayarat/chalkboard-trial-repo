import { URIs } from '../../../constants/image-uri';

import utilStyles from '../../../styles/utils.module.css';
import styles from './fetching-invoice.module.css';
import LinearLoader from '../linear-loader/linear-loader.component';

function FetchingInvoice() {
  return (
    <div className={`${styles.background}`}>
      <img src={URIs.LOGO_URI}  width='150'  />
      {/* 20px * 3 spacer */}
      <div className={utilStyles.divider} />
      <div className={utilStyles.divider} />
      <div className={utilStyles.divider} />
      <div className='w-128'>
        <LinearLoader />

        {/* 20px * 4 spacer */}
        <div className={utilStyles.divider} />
        <div className={utilStyles.divider} />
        <h4
          className={`${utilStyles.heading4} m-0 text-center text-cba-dark-gray`}
        >
          Fetching Invoice
        </h4>
      </div>
      <span
        className={`${utilStyles.caption} text-black text-center absolute mb-5 bottom-0`}
      >
        Don’t press Back button
      </span>
    </div>
  );
}

export default FetchingInvoice;
