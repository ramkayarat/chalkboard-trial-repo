import Header from '../../header/header.component';
import Footer from '../../home/footer/footer.component';

export default function MainLayout({ children, hideFooter }) {
  return (
    <div>
      <Header />
      {children}
      <div className={`hidden lg:block ${hideFooter ? 'lg:hidden' : ''}`}>
        <Footer />
      </div>
    </div>
  );
}
