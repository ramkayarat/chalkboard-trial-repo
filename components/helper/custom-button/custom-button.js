import styles from './custom-button.module.css';

export default function CustomButton({
  children,
  headSectionButton,
  disabled = false,
  alert,
  ...otherProps
}) {
  return (
    <button
      className={`${styles.cbaButtonBlue}  ${disabled ? styles.disabled : ''} ${
        alert ? styles.alert : ''
      }`}
      disabled={disabled}
      {...otherProps}
    >
      {children.toUpperCase()}
    </button>
  );
}
