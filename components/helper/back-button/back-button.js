import Link from 'next/link';
import { FaChevronLeft } from 'react-icons/fa';
import { AppRoutes } from '../../../constants/app-routes';
import { colors } from '../../../constants/colors';
import { URIs } from '../../../constants/image-uri';

export default function BackButton({ href, text }) {
  return (
    <div className='flex justify-start'>
      <div className='mr-1'>
        <img src={URIs.LEFT_ARROW_ICON_BLUE_URI} />
      </div>
      <Link href={href ? href : AppRoutes.HOME}>
        <a className='text-cba-blue uppercase'>{text ? text : 'back'}</a>
      </Link>
    </div>
  );
}
