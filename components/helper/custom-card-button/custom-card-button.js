import styles from './custom-card-button.module.css';

export default function CustomCardButton({
  children,
  alert,
  disabled,
  ...otherProps
}) {
  return (
    <button
      disabled={!!disabled}
      className={`${styles[`card-button`]} ${disabled ? styles.disabled : ''} ${
        alert ? styles.alert : ''
      }`}
      {...otherProps}
    >
      {children.toUpperCase()}
    </button>
  );
}
