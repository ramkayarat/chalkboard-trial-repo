import Link from 'next/link';
import moment, { months } from 'moment';
import { capitalizeString } from '../../../utils/utils';
import { paymentStatus } from '../../../constants/payment-status.type';
import CustomCardButton from '../custom-card-button/custom-card-button';

import styles from './course-card.module.css';
import utilStyles from '../../../styles/utils.module.css';
import { AppRoutes } from '../../../constants/app-routes';

export default function CourseCard({ batch, status }) {
  const { startDate, endDate, slug } = batch;
  const { title, featureImage, subject, class: courseClass } = batch.course;
  const {url:featureImageUrl,width,height}=featureImage.formats.small;
  
  const expired =
    Date.parse(batch.endDate) < Date.now() ||
    status === paymentStatus.CANCELLED;

    return (
    <Link
      href={`${AppRoutes.BATCHES}/[slug]`}
      as={`${AppRoutes.BATCHES}/${slug}`}
    >
      <div className={`${styles.card} w-90vw cursor-pointer`}>
        <img
          src={featureImageUrl}
          className='object-cover w-1/2 lg:w-full h-full rounded'
          width={width}
          height={height}
          alt= {title}
        />
        <div className={styles[`card-content`]}>
          <span
            className={`${utilStyles.body2} text-cba-dark-gray text-left lg:text-center`}
          >
            {`${capitalizeString(
              subject
            )} for Class ${courseClass}: ${capitalizeString(title)}`}
          </span>

          {expired && status && (
            <div className='text-right lg:text-center'>
              {status !== paymentStatus.CANCELLED ? (
                <CustomCardButton>give feedback</CustomCardButton>
              ) : (
                <CustomCardButton disabled>cancelled</CustomCardButton>
              )}
            </div>
          )}

          {!expired && status && (
            <div>
              <span
                className={`${utilStyles.overline} text-cba-disabled block`}
              >
                Start date: {moment(startDate).format('DD MMM YYYY')}
              </span>
              <span
                className={`${utilStyles.overline} text-cba-disabled block`}
              >
                End date: {moment(endDate).format('DD MMM YYYY')}
              </span>
            </div>
          )}

          {!status && (
            <span
              className={`${utilStyles.overline} text-cba-disabled lg:text-center block`}
            >
              Start date: {moment(startDate).format('DD-MM-YYYY')}
            </span>
          )}
        </div>
      </div>
    </Link>
  );
}
