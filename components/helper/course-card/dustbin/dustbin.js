import { URIs } from '../../../../constants/image-uri';

export default function Dustbin() {
  return (
    <div className='bg-white flex justify-center items-center rounded absolute w-6 h-6 top-0 left-0 mt-1 ml-1'>
      <img src={URIs.DUSTBIN_URI} />
    </div>
  );
}
