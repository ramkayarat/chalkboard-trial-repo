import { URIs } from '../../../constants/image-uri';

import utilStyles from '../../../styles/utils.module.css';
import styles from './loader.module.css';

function Loader() {
  return (
    <div
      className={`${utilStyles['yellow-pattern-background']} fixed top-0 left-0 w-screen h-screen flex justify-center`}
    >
      <img src={URIs.LOGO_URI} className={styles.logo} />
    </div>
  );
}

export default Loader;
