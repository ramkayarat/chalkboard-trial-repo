import { useState } from 'react';
import { signUp } from '../../../services/auth.service';
import {
  validateForm,
  validateInput,
} from '../../../services/form-validation.service';

import CustomButton from '../../helper/custom-button/custom-button';

import utilStyles from '../../../styles/utils.module.css';
import styles from './sign-up-input-column.module.css';

export default function SignUpForm() {
  const [signUpCreds, setSignUpCreds] = useState({
    firstName: '',
    familyName: '',
    email: '',
    phoneNumber: '',
    password: '',
  });

  const [formErrors, setFormErrors] = useState({
    email: '',
    password: '',
    phoneNumber: '',
  });

  const handleSubmit = async (evt) => {
    evt.preventDefault();
    try {
      if (validateForm(signUpCreds, { ...formErrors })) {
        const credsToSend = { ...signUpCreds };

        await signUp(credsToSend);
      }
    } catch (error) {
      document.getElementById('sign-up-form').reset();
    }
  };

  const handleChange = (evt) => {
    evt.preventDefault();
    const { id, value } = evt.target;

    setSignUpCreds({
      ...signUpCreds,
      [id]: value,
    });

    setFormErrors(validateInput(id, value, { ...formErrors }));
  };

  const { email, password, phoneNumber } = formErrors;

  return (
    <form
      className='flex flex-col w-auto'
      id='sign-up-form'
      onSubmit={(evt) => handleSubmit(evt)}
    >
      <div className='flex justify-between'>
        <input
          type='text'
          id='firstName'
          className={utilStyles['text-field-half']}
          placeholder='First Name'
          onChange={(evt) => handleChange(evt)}
          required
        />
        <input
          type='text'
          id='familyName'
          className={utilStyles['text-field-half']}
          placeholder='Last Name'
          onChange={(evt) => handleChange(evt)}
          required
        />
      </div>
      <input
        type='email'
        id='email'
        className={`${utilStyles['text-field']} mt-3`}
        placeholder='My Email ID'
        onChange={(evt) => handleChange(evt)}
        required
      />
      <span className={`${utilStyles.caption} capitalize text-cba-alert-red`}>
        {email !== '' ? email : null}
      </span>
      <div className='relative flex items-center'>
        <div className='absolute pl-2 text-cba-blue'>+91</div>
        <input
          type='tel'
          id='phoneNumber'
          className={styles['text-field-phone']}
          placeholder='Phone Number'
          onChange={(evt) => handleChange(evt)}
          required
        />
      </div>
        <span className={`${utilStyles.caption} capitalize text-cba-alert-red`}>
          {phoneNumber !== '' ? phoneNumber : null}
        </span>
      <input
        type='password'
        id='password'
        className={utilStyles['text-field']}
        placeholder='My Password'
        onChange={(evt) => handleChange(evt)}
        required
      />
      <span className={`${utilStyles.caption} capitalize text-cba-alert-red`}>
        {password !== '' ? password : null}
      </span>

      <div className='h-12' />
      <CustomButton type='submit'>Create my account</CustomButton>
    </form>
  );
}
