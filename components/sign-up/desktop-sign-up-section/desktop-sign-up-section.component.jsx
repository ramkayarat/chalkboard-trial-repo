import Link from 'next/link';

import SignUpForm from '../sign-up-input-column/sign-up-input-column.component';

import styles from './desktop-sign-up-section.module.css';
import utilStyles from '../../../styles/utils.module.css';
import { AppRoutes } from '../../../constants/app-routes';

export default function DesktopSignUp() {
  return (
    <section className={styles.background}>
      <div className='h-82px' />
      <div className={styles.cardContainer}>
        <div className={styles.card}>
          <div className='relative w-1/2 h-full'>
            <div className='flex flex-col h-auto absolute left-0 top-0 ml-16 mt-12'>
              <h2 className={`${utilStyles.heading2} text-left`}>Sign Up</h2>
              <Link href={AppRoutes.LOGIN}>
                <a
                  className={`${utilStyles.caption} text-center text-cba-blue pt-1`}
                >
                  Already have an account? Login
                </a>
              </Link>
            </div>
          </div>
          <div className='flex flex-col justify-evenly items-center w-1/2 h-full bg-white'>
            <SignUpForm />
          </div>
        </div>
      </div>
    </section>
  );
}
