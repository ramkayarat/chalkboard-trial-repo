import Link from 'next/link';
import { useEffect, useState } from 'react';

import ColumnLayout from '../helper/column-layout/column-layout';
import SignUpForm from './sign-up-input-column/sign-up-input-column.component';
import DesktopSignUp from './desktop-sign-up-section/desktop-sign-up-section.component';

import utilStyles from '../../styles/utils.module.css';
import styles from './signup-section.module.css';
import { AppRoutes } from '../../constants/app-routes';

export default function SignUpSection() {
  const [screenWitdh, setScreenWidth] = useState();

  useEffect(() => {
    if (window) {
      window.addEventListener('resize', () =>
        setScreenWidth(window.innerWidth)
      );
    }
    setScreenWidth(window.innerWidth);
  }, []);

  return (
    <section className={styles.background}>
      {screenWitdh < 1024 ? (
        <ColumnLayout>
          <h4 className={`${utilStyles.heading4} m-0 py-6`}>Sign-up</h4>

          <SignUpForm />

          <Link href={AppRoutes.LOGIN}>
            <a
              className={`${utilStyles.caption} text-center text-cba-blue pt-3`}
            >
              Already have an account? Login
            </a>
          </Link>
        </ColumnLayout>
      ) : (
        <DesktopSignUp />
      )}

    </section>
  );
}
