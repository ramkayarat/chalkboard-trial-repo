import Link from 'next/link';
import { useEffect, useState } from 'react';
import { AuthToken } from '../../services/auth-token.service';
import { URIs } from '../../constants/image-uri';

import { FaBars } from 'react-icons/fa';
import MobileNav from '../nav/mobile-nav/mobile-nav';
import NavLink from '../nav/nav-link/nav-link';

import styles from './header.module.css';
import { colors } from '../../constants/colors';
import { AppRoutes } from '../../constants/app-routes';

export default function Header() {
  const [mobileNavOpen, setMobileNavOpen] = useState(false);
  const [loggedIn, setLoggedIn] = useState(false);

  useEffect(() => {
    if (AuthToken.getToken()) {
      setLoggedIn(true);
    }
  }, []);

  return (
    <>
      <div className={`${styles['header-container']} `}>
        <div className={`${styles['header']} px-5 py-3 lg:px-24`}>
          <Link href={AppRoutes.HOME}>
            <a className={styles[`brand-logo`]}>
              <img src={URIs.HEADER_LOGO_URI} />
            </a>
          </Link>
          <div className={styles[`header-icons-row`]}>
            <NavLink header route={AppRoutes.BATCHES}>
              all batches
            </NavLink>
            {/* TODO:util for teachers page is live */}
            {/* <NavLink header route={AppRoutes.FOR_TEACHERS}>
            for teachers
          </NavLink> */}
            <NavLink header route={AppRoutes.FAQS}>
              faq
            </NavLink>
            <NavLink header route={AppRoutes.SUPPORT}>
              support
            </NavLink>
            <NavLink
              route={`${loggedIn ? AppRoutes.MY_PROFILE : AppRoutes.SIGN_UP}`}
              icon
            >
              <img src={URIs.PROFILE_ICON_URI} />
            </NavLink>
            <div
              className={`${styles['header-icon']} lg:hidden`}
              onClick={() => setMobileNavOpen(true)}
            >
              <FaBars size={22} cursor='pointer' color={colors.CBA_BLUE} />
            </div>
          </div>
          <MobileNav
            loggedIn={loggedIn}
            mobileNavOpen={mobileNavOpen}
            setMobileNavOpen={setMobileNavOpen}
          />
        </div>
      </div>
      <div className='h-82px' />
    </>
  );
}
