import Link from 'next/link';
import { useEffect, useState } from 'react';

import ColumnLayout from '../helper/column-layout/column-layout';
import LoginForm from './login-form/login-form.component';
import DesktopLogin from './desktop-login-section/desktop-login-section.component';

import utilStyles from '../../styles/utils.module.css';
import styles from './login-section.module.css';
import { AppRoutes } from '../../constants/app-routes';

export default function LoginSection() {
  const [screenWitdh, setScreenWidth] = useState();

  useEffect(() => {
    if (window) {
      window.addEventListener('resize', () =>
        setScreenWidth(window.innerWidth)
      );
    }
    setScreenWidth(window.innerWidth);
  }, []);

  return (
    <section className={styles.background}>

      {screenWitdh < 1024 ? (
        <ColumnLayout>
          <h4 className={`${utilStyles.heading4} m-0 py-6`}>Log-in</h4>
          <LoginForm />
          <Link href={AppRoutes.FORGOT_PASSWORD}>
            <a
              className={`${utilStyles.caption} text-center text-cba-blue mt-5`}
            >
              Forgot Password?
            </a>
          </Link>
        </ColumnLayout>
      ) : (
        <DesktopLogin />
      )}
      
    </section>
  );
}
