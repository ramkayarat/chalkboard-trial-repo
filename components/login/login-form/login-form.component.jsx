import Link from 'next/link';
import { useState } from 'react';
import {
  validateInput,
  validateForm,
} from '../../../services/form-validation.service';

import utilStyles from '../../../styles/utils.module.css';
import CustomButton from '../../helper/custom-button/custom-button';
import { login } from '../../../services/auth.service';
import { AppRoutes } from '../../../constants/app-routes';

export default function LoginForm() {
  const [loginCreds, setLoginCreds] = useState({ email: '', password: '' });
  const [formErrors, setFormErrors] = useState({ email: '', password: '' });

  const handleChange = (evt) => {
    evt.preventDefault();
    const { id, value } = evt.target;

    setLoginCreds({ ...loginCreds, [id]: value });

    setFormErrors(validateInput(id, value, { ...formErrors }));
  };

  const handleSubmit = async (evt) => {
    evt.preventDefault();
    if (validateForm(loginCreds, formErrors)) {
      login(loginCreds);
    } else {
      document.getElementById('login-form').reset();
    }
  };

  const { email, password } = formErrors;

  return (
    <form
      id='login-form'
      className='flex flex-col w-auto'
      onSubmit={(evt) => handleSubmit(evt)}
    >
      <input
        type='email'
        id='email'
        className={`${utilStyles['text-field']} placeholder-cba-blue placeholder-opacity-50`}
        placeholder='My Email ID'
        onChange={(evt) => {
          handleChange(evt);
        }}
        required
      />
      <span className={`${utilStyles.caption} text-cba-alert-red`}>
        {email !== '' ? email : null}
      </span>

      <input
        type='password'
        id='password'
        className={`${utilStyles['text-field']} mt-3`}
        placeholder='My Password'
        onChange={(evt) => {
          handleChange(evt);
        }}
        required
      />
      <span className={`${utilStyles.caption} text-cba-alert-red`}>
        {password !== '' ? password : null}
      </span>

      <div className='lg:h-13' />
      <Link href={AppRoutes.SIGN_UP}>
        <a
          className={`${utilStyles.caption} lg:hidden text-center text-cba-blue mt-16 py-3`}
        >
          First time here? Create an Account
        </a>
      </Link>
      <div className='text-center'>
        <CustomButton type='submit'>Log into my account</CustomButton>
      </div>
    </form>
  );
}
