import Link from 'next/link';

import LoginForm from '../login-form/login-form.component';

import styles from './desktop-login-section.module.css';
import utilStyles from '../../../styles/utils.module.css';
import { AppRoutes } from '../../../constants/app-routes';

export default function DesktopLogin() {
  return (
    <section className={styles.background}>
      <div className='h-82px' />
      <div className={styles.cardContainer}>
        <div className={styles.card}>
          <div className='relative w-1/2 h-full'>
            <div className='flex flex-col h-auto absolute left-0 top-0 ml-16 mt-12'>
              <h2 className={`${utilStyles.heading2} text-left`}>Log-in</h2>
              <Link href={AppRoutes.SIGN_UP}>
                <a
                  className={`${utilStyles.caption} text-center text-cba-blue pt-1`}
                >
                  First time here? Create an Account
                </a>
              </Link>
            </div>
          </div>

          <div className='flex flex-col justify-evenly lg:justify-center items-center w-1/2 h-full bg-white'>
            <LoginForm />
            <Link href={AppRoutes.FORGOT_PASSWORD}>
              <a
                className={`${utilStyles.caption} text-center text-cba-blue p-5`}
              >
                Forgot Password?
              </a>
            </Link>
          </div>
        </div>
      </div>
    </section>
  );
}
