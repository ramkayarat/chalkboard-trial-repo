import { useContext } from 'react';
import useSWR from 'swr';
import { get } from '../../../services/helpers/helpers';
import { GET_BATCHES_URL } from '../../../utils/api-urls.utils';
import { UnsubscribeContext } from '../../../providers/unsubscribe.provider';

import Card from '../card/card.component';
import Loader from '../../helper/loader/loader';
import styles from './subscribed-course.module.css';
import Error from '../../error/error.component';

import utilStyles from '../../../styles/utils.module.css';

function SubscribedCourse({ payments, batchId, paid }) {
  const { toggleVisible, updateBatchId } = useContext(UnsubscribeContext);

  const { data: batch, getBatchError } = useSWR(
    `${GET_BATCHES_URL}/${batchId}`,
    get
  );

  if (getBatchError) {
    return <Error />;
  }

  if (!batch) {
    return <Loader />;
  }

  const { subject, class: courseClass, title, featureImageUrl } = batch.course;

  const unsubscribeButton = () => (
    <span
      onClick={() => {
        updateBatchId(batchId);
        toggleVisible();
      }}
      className={`${styles.caption} cursor-pointer text-cba-blue text-center`}
    >
      unsubscribe
    </span>
  );

  return (
    <div className='pb-10 h-auto'>
      <div className='flex w-full h-auto mt-12'>
        <div className='flex w-full h-auto justify-start items-center'>
          <div className='w-10 h-10 mr-3'>
            <img src={featureImageUrl} />
          </div>
          <h6
            className={`${utilStyles.heading6} m-0 text-cba-dark-gray text-left`}
          >
            {subject} for Class {courseClass}: {title}
          </h6>
        </div>
        <div className='hidden md:block'>
          {paid ? null : unsubscribeButton()}
        </div>
      </div>
      <div className='flex h-auto'>
        <div className={styles['carousel-wrap']}>
          {payments.map((payment) => (
            <Card payment={payment} batch={batch} key={payment.id} />
          ))}
        </div>
      </div>
      <p className='md:hidden'>{paid ? null : unsubscribeButton()}</p>
    </div>
  );
}

export default SubscribedCourse;
