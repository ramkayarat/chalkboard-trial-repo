import { useContext, useState } from 'react';
import { UnsubscribeContext } from '../../providers/unsubscribe.provider';
import { groupPaymentsByBatchId } from '../../utils/payment.utils';

import SubscribedCourse from './subscribed-courses/subscribed-course.compnent';

import UnsubscribeSection from '../unsubscribe/unsubscribe-section.componet';
import MyPaymentsSwitch from './my-payments-switch/my-payments-switch.component';
import BackButton from '../helper/back-button/back-button';
import FechingInvoicePopUp from '../pop-ups/fetching-invoice/fetching-invoice-pop-up.component';
import utilStyles from '../../styles/utils.module.css';
import { myPaymentTypes } from '../../constants/my-payments.type';
import { paymentStatus } from '../../constants/payment-status.type';
import { PopUpContext } from '../../providers/pop-up.provider';
import { popUpStates } from '../../constants/pop-up-state.types';
import SuccessPopUp from '../pop-ups/success/success.component';
import ErrorPopUp from '../pop-ups/error/error.component';
import { CheckoutContext } from '../../providers/checkout.provider';
import CheckoutPopUp from '../pop-ups/checkout/checkout-pop-up.component';

export default function MyPaymentsSection({ payments }) {
  //there will be two payment states : paid or unpaid
  const [paymentState, setPaymentState] = useState(myPaymentTypes.UPCOMING);
  const { visible } = useContext(UnsubscribeContext);
  const { checkoutVisible } = useContext(CheckoutContext);
  const { popUpState } = useContext(PopUpContext);

  const groupedUpcomingPayments = groupPaymentsByBatchId(
    payments.filter((payment) => payment.status === paymentStatus.PENDING)
  );

  const groupedPastPayments = groupPaymentsByBatchId(
    payments.filter((payment) => payment.status !== paymentStatus.PENDING)
  );

  return (
    <>
      {checkoutVisible && <CheckoutPopUp />}
      {popUpState === popUpStates.FETCHING && <FechingInvoicePopUp />}
      {popUpState === popUpStates.SUCCESS && (
        <SuccessPopUp text='A copy of the invoice has been sent to your registered Email ID.' />
      )}
      {popUpState === popUpStates.UNSUBSCRIBE_SUCCESS && (
        <SuccessPopUp
          text='The course has been moved to “My Courses > Past”'
          unsubscribeSuccess
        />
      )}
      {popUpState === popUpStates.ERROR && (
        <ErrorPopUp text='There was an error downloading invoice' />
      )}
      {visible && <UnsubscribeSection />}
      <section
        className={`px-5 bg-transparent lg:w-4/5 lg:mx-auto ${
          visible || !!popUpState ? 'fix w-full' : ''
        }${checkoutVisible ? 'relative' : ''}`}
      >
        <div className='relative'>
          <h4 className={`${utilStyles.heading4} my-8 text-center`}>
            My Payments
          </h4>
          <div className='absolute top-0 mt-6 hidden lg:block'>
            <BackButton href='/my-profile' text='my profile' />
          </div>
        </div>

        <MyPaymentsSwitch
          paymentState={paymentState}
          setPaymentState={setPaymentState}
        />

        {paymentState === myPaymentTypes.UPCOMING &&
          Object.keys(groupedUpcomingPayments).map((batchId) => (
            <SubscribedCourse
              key={batchId}
              batchId={batchId}
              payments={groupedUpcomingPayments[batchId]}
            />
          ))}

        {paymentState === myPaymentTypes.PAST &&
          Object.keys(groupedPastPayments).map((batchId) => (
            <SubscribedCourse
              key={batchId}
              batchId={batchId}
              payments={groupedPastPayments[batchId]}
              paid
            />
          ))}
      </section>
    </>
  );
}
