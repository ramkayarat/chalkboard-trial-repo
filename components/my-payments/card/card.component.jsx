import moment from 'moment';
import { useContext } from 'react';
import { InvoiceContext } from '../../../providers/invoice.provider';
import {
  generatePendingPaymentInvoice,
  getInvoice,
} from '../../../services/payment.service';
import { paymentStatus } from '../../../constants/payment-status.type';
import { PopUpContext } from '../../../providers/pop-up.provider';

import ColumnLayout from '../../helper/column-layout/column-layout';
import CustomCardButton from '../../helper/custom-card-button/custom-card-button';

import utilStyles from '../../../styles/utils.module.css';
import styles from './card.module.css';
import { popUpStates } from '../../../constants/pop-up-state.types';
import { CheckoutContext } from '../../../providers/checkout.provider';
import {
  checkPaymentType,
  getPaymentMonth,
} from '../../../utils/payment.utils';

export default function Card({ payment, batch }) {
  const { status, dueDate, amount, pdfUrl, id: paymentId } = payment;
  const { durationInMonths, startDate, course, slug } = batch;
  const { setInvoiceUrl } = useContext(InvoiceContext);
  const { togglePopUpState } = useContext(PopUpContext);
  const { toggleCheckoutPopUp, setCheckoutData } = useContext(CheckoutContext);

  const handleGetInvoice = async () => {
    togglePopUpState(popUpStates.FETCHING);

    let url = pdfUrl;
    if (!url || url === '') {
      url = await getInvoice({ paymentId });
      setInvoiceUrl(url);
    } else if (url) {
      setInvoiceUrl(url);
    }
  };

  const handlePayment = async () => {
    const invoice = await generatePendingPaymentInvoice({ paymentId });
    setCheckoutData({
      course,
      invoice,
      isPendingPayment: true,
      paymentId,
      slug,
    });
    toggleCheckoutPopUp();
  };
  console.log(getPaymentMonth(startDate, dueDate))
  return (
    <div className={`${styles.card}`}>
      <p className={`${utilStyles.overline} text-cba-disabled`}>
        Due: {moment(dueDate).format('DD MMM YYYY')}
      </p>
      <div className='flex justify-between h-full items-end w-full pb-10px'>
        <ColumnLayout align='start'>
          <p className={`${utilStyles.heading6} text-cba-dark-gray`}>
            <span className={`${utilStyles.overline} text-cba-disabled`}>
              INR
            </span>
            {amount / 100}
          </p>
          <p className={`${utilStyles.overline} text-cba-disabled`}>
            Month: {getPaymentMonth(startDate, dueDate)} of {durationInMonths}
          </p>
        </ColumnLayout>

        {status === paymentStatus.CANCELLED && (
          <CustomCardButton disabled>cancelled</CustomCardButton>
        )}

        {status === paymentStatus.PENDING && (
          <CustomCardButton onClick={() => handlePayment()}>
            pay
          </CustomCardButton>
        )}

        {status === paymentStatus.PAID && (
          <CustomCardButton onClick={() => handleGetInvoice()}>
            get invoice
          </CustomCardButton>
        )}
      </div>
    </div>
  );
}
