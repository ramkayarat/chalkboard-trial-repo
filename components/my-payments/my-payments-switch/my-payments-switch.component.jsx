import { myPaymentTypes } from '../../../constants/my-payments.type';
import styles from './my-payments-switch.component.module.css';

function MyPaymentsSwitch({ setPaymentState, paymentState }) {
  return (
    <div className={`${styles[`switch-container`]}`}>
      <button
        className={`${styles['switch-button']} ${styles['switch-left']} ${
          paymentState === myPaymentTypes.UPCOMING ? styles.active : ''
        }`}
        onClick={() => setPaymentState(myPaymentTypes.UPCOMING)}
      >
        upcoming
      </button>
      <button
        className={`${styles['switch-button']} ${styles['switch-right']} ${
          paymentState === myPaymentTypes.PAST ? styles.active : ''
        }`}
        onClick={() => setPaymentState(myPaymentTypes.PAST)}
      >
        past
      </button>
    </div>
  );
}

export default MyPaymentsSwitch;
