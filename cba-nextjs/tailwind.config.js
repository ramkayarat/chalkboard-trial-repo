module.exports = {
  purge: [],
  theme: {
    extend: {
      screens: {
        xs: '375px',
        xxl: '1640px',
      },
      colors: {
        'cba-yellow': '#F9B81E',
        'cba-blue': '#03477A',
        'cba-dark-blue': '#001B3C',
        'cba-light-blue': '#035797',
        'cba-dark-gray': '#212121',
        'cba-disabled': '#777777',
        'cba-alert-red': '#FF0F0F',
      },
      opacity: {
        5: '0.05',
        10: '0.1',
      },
      spacing: {
        '0.5px': '0.5px',
        13: '3.5rem',
        '82px': '82px',
        128: '18rem',
        lg: '32rem',
        '3px': '3px',
        '10px': '10px',
        '90vw': '90vw',
        '65%': '65%',
        '95%': '95%',
      },
      fontFamily: {
        heading: ['Oxygen'],
        body: ['Prompt'],
      },
      fontSize: {
        '5.6xl': '3.6rem',
      },
    },
  },
  variants: {},
  plugins: [],
};
