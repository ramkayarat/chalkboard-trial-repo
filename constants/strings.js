export const reasonSectionStrings = {
  REASON_1_TAGLINE: 'Personalized Learning',
  REASON_1_DESCRIPTION:
    'Optimum student teacher ratio is maintained for each class to focus on individual students and track their progress effectively.',
  REASON_2_TAGLINE: 'Best-in-class Teaching Practices',
  REASON_2_DESCRIPTION:
    'We encourage our faculties to implement a combination of various teaching pedagogies.',
  REASON_3_TAGLINE: 'Expert Teachers',
  REASON_3_DESCRIPTION:
    'We encourage our faculties to implement a combination of various teaching pedagogies.',
  REASON_4_TAGLINE: 'Coverage across Multiple Boards & Categories',
  REASON_4_DESCRIPTION:
    'We make best learning avenues accessible to students from vernacular region as well as students with special needs.',
  REASON_5_TAGLINE: 'Affordability',
  REASON_5_DESCRIPTION:
    'We believe no child should be deprived of good education & learning because of financial constraints.',
};

export const COMPANY_TAGLINE = 'Online Tuition for Classes 6-12'
export const COMPANY_TAGLINE_2 = 'There’s no limit to what you can learn next.'
export const COMPANY_ADDRESS_LINE_1 = 'A1/101, GERA SONG OF JOY, GRANT ROAD'
export const COMPANY_ADDRESS_LINE_2 = 'KHARADI PUNE 411014'