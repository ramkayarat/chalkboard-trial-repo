export const popUpStates = {
  FETCHING: 'fetching',
  SUCCESS: 'success',
  UNSUBSCRIBE: 'usubscribe success',
  ERROR: 'error',
};
