export const colors = {
    CBA_BLUE: '#03477A',
    CBA_DAR_GRAY: '#212121',
    CBA_YELLOW: '#F9B81E',
    CBA_ERROR_RED: '#FF0F0F',
    CBA_DARK_BLUE: '#001B3C'
}