export const boards = [
  {
    id: 0,
    title: "ICSE",
    value: "ICSE",
    selected: false,
    key: "educationBoard",
  },
  {
    id: 1,
    title: "CBSE",
    value: "CBSE",
    selected: false,
    key: "educationBoard",
  },
  {
    id: 2,
    title: "Maharashtra Board",
    value: "MSBSHSE",
    selected: false,
    key: "educationBoard",
  },
  {
    id: 3,
    title: "Language/Culture",
    value: "Language_OR_Culture",
    selected: false,
    key: "educationBoard",
  },
  {
    id: 4,
    title: "Advanced Skill Development",
    value: "Advanced_Skill_Development",
    selected: false,
    key: "educationBoard",
  },
  {
    id: 5,
    title: "Competitive Exam Prep",
    value: "Competitive_Exam_Prep",
    selected: false,
    key: "educationBoard",
  },
];

export const classes = [
  { id: 0, title: 'All', value: '0', selected: false, key: 'class' },
  { id: 1, title: 'Class 6', value: 6, selected: false, key: 'class' },
  { id: 2, title: 'Class 7', value: 7, selected: false, key: 'class' },
  { id: 3, title: 'Class 8', value: 8, selected: false, key: 'class' },
  { id: 4, title: 'Class 9', value: 9, selected: false, key: 'class' },
  { id: 5, title: 'Class 10', value: 10, selected: false, key: 'class' },
  { id: 6, title: 'Class 11', value: 11, selected: false, key: 'class' },
  { id: 7, title: 'Class 12', value: 12, selected: false, key: 'class' },
];

export const subjects = [
  {
    id: 0,
    title: "Science",
    value: "science",
    selected: false,
    key: "subject",
  },
  {
    id: 1,
    title: "Mathematics",
    value: "mathematics",
    selected: false,
    key: "subject",
  },
  {
    id: 2,
    title: "Social Sciences",
    value: "Social_Sciences",
    selected: false,
    key: "subject",
  },
  {
    id: 3,
    title: "Language",
    value: "language",
    selected: false,
    key: "subject",
  },
  {
    id: 4,
    title: "Programming",
    value: "programming",
    selected: false,
    key: "subject",
  },
  { id: 5, title: "Others", value: "others", selected: false, key: "subject" },
];
