export const paymentStatus = {
  CANCELLED: 'cancelled',
  PENDING: 'pending',
  PAID: 'paid',
};
