export const URIs = {
  HEADER_LOGO_URI: '/svgs/brand-header-logo.svg',
  LOGO_URI: '/svgs/logo-new-transparent.svg',
  PROFILE_ICON_URI: '/svgs/profile.svg',
  LEFT_ARROW_ICON_BLUE_URI: '/svgs/left-arrow.svg',
  WHITE_WAVE: '/svgs/white-wave.svg',
  YELLOW_WAVE: '/svgs/yellow-wave.svg',
  EDIT_ICON: '/svgs/edit-icon.svg',
  TV_ICON_URI: '/svgs/tv-icon.svg',
  TV_YELLOW_ICON_URI: '/svgs/tv-icon-yellow.svg',
  LEARNING_IMAGE_URL: '/svgs/learning-image.svg',
  PERSONALISED_LEARNING_IMAGE_URL: '/svgs/personailised_learning.svg',
  BEST_IN_CLASS_IMAGE_URL: '/svgs/best-in-class.svg',
  EXPERT_TEACHERS_IMAGE_URL: '/svgs/expert-teachers.svg',
  COVERAGE_ACROSS_IMAGE_URL: '/svgs/coverage-across.svg',
  AFFORDIBILITY_IMAGE_URL: '/svgs/affordability.svg',
  SKILL_DEVELOPMENT_IMAGE_URL: '/svgs/skill-development.svg',
  FOR_TEACHERS_IMAGE_URL: '/svgs/for-teachers.svg',
  
  DEFENCE_EXAMINATION_IMAGE_URL:
  'https://res.cloudinary.com/dxzkcvxza/image/upload/v1603875075/assets/nda-cds_1_b6svtg.png',
  MAHARASHTRA_BOARD_IMAGE_URL:
  'https://res.cloudinary.com/dxzkcvxza/image/upload/v1603875060/assets/maharashtra-sb_1_io07tp.png',
  CBSE_BOARD_IMAGE_URL:
  'https://res.cloudinary.com/dxzkcvxza/image/upload/v1603875625/assets/cbse_1_f4nhot.png',
  CISCE_BOARD_IMAGE_URL:
  'https://res.cloudinary.com/dxzkcvxza/image/upload/v1603875610/assets/cisce_1_gh9f9s.png',
  
  //Deprecated
  // USER_ICON_URI: '/svgs/user-plus.svg',
  // DOLLAR_ICON_URI: '/svgs/dollar.svg',
  // CART_PLUS: '/svgs/cart-plus.svg',
  // DUSTBIN_URI: '/svgs/dustbin.svg',
  // ARROW_UP_BLUE_URI: '/svgs/arrow-up-blue.svg',
  // SEARCH_ICON: '/svgs/search-icon.svg',
};
