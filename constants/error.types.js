
//ids are provided by strapi
export const errorTypes = {
  //login error
  CONFIMRED_EMAIL: "Auth.form.error.confirmed",
  LOGIN_CREDENTIALS: "Auth.form.error.invalid",
  
  //sing up error
  ROLE_NOT_FOUND: 'Auth.form.error.role.notFound'
};
