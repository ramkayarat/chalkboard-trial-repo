export const AppRoutes = {
  HOME: '/',
  FAQS: '/faqs',
  BATCHES: '/batches',
  SUPPORT: '/support',
  PAYMENT_PLAN: '/payment-plan',
  MY_PROFILE: '/my-profile',
  MY_COURSES: '/my-courses',
  MY_PAYMENTS: 'my-payments',
  SIGN_UP: '/sign-up',
  LOGIN: '/login',
  FORGOT_PASSWORD: '/forgot-password',
  ERROR: '/error',
  SUCCESS: '/success',
  ADMIN: '/admin',
  API_SUCCESS: '/api/hello',
  //   #deprecared
  FOR_TEACHERS: '/for-teachers',
  PHONE_VERIFY: '/phone-verify',
  PRIVACY_POLICY: '/privacy-policy',
  TERMS_OF_SERVICE: '/terms-of-service',
};

//these routs are between []
export const dynamicRoutes = {
  SLUG: 'slug',
};

export const ExternalLinks = {
  PRIVACY_POLICY:
    'https://cookytech.github.io/chalkboard-terms-and-policies/policies/privacy.html',
  TERMS_OF_SERVICE:
    'https://cookytech.github.io/chalkboard-terms-and-policies/',
};
