import moment from 'moment';
import { paymentStatus } from '../constants/payment-status.type';
import { getPaymentsByBatchId } from './payment.utils';
//gets the course subjects and makes a SET of subjects
export const getCourseSubjectsForDD = (courses) => {
  let subjectsForDD = [];
  //get a SET of subjects
  let subjects = new Set(courses.map((course) => course.subject));
  //return SET as an array for drop down menu
  let i = 0;
  subjects.forEach((subject) => {
    subjectsForDD.push({
      title: subject,
      id: i,
      key: 'subject',
      selected: false,
    });

    i++;
  });

  return subjectsForDD;
};

export const getCourseById = (id, courses) => {
  return courses.find((course) => course.id == id);
};

//course filters
/*by subject*/
export const filterCourseBySubject = (batches, subject) =>
  batches.filter(
    (batch) => batch.course.subject.toLowerCase() == subject.toLowerCase()
  );

/*by board*/
export const filterCourseByBoard = (batches, board) =>
  batches.filter(
    (batch) => batch.course.board.toLowerCase() == board.toLowerCase()
  );

/*by class*/
export const filterCourseByClass = (batches, selectedClass) => {
  //return all batches if choice is all,i.e. 0
  if (selectedClass == 0) {
    return batches;
  }

  return batches.filter((batch) => batch.course.class == selectedClass);
};

/*by class*/
export const filterCourseByDate = (batches, date) => {
  return batches.filter((batch) => {
    let batchStartDate = moment(batch.startDate).format('DD-MM-YYYY');
    let userSelectedStartDate = moment(date).format('DD-MM-YYYY');
    return batchStartDate == userSelectedStartDate;
  });
};
//batch filters
export const getBatchById = (id, batches) =>
  batches.find((batch) => batch.id == id);

export const getBatchBySlug = ({ slug, batches }) =>
  batches.find((batch) => batch.slug == slug);

export const getBatchSlugs = (batches) => batches.map((batch) => batch.slug);

export const getPastSubscriptions = ({ subscriptions, batches, payments }) => {
  let batchesFilteredBySubscription = [];
  let batchesFilteredByEndDateAndStatus = [];

  subscriptions.forEach((subscription) => {
    let fb = getBatchById(subscription.batchId, batches);
    batchesFilteredBySubscription.push(fb);
  });

  batchesFilteredByEndDateAndStatus = batchesFilteredBySubscription.filter(
    (batch) => {
      let filteredPayments = getPaymentsByBatchId(payments, batch.id);
      let cancelled = false;

      filteredPayments.forEach(
        (payment) =>
          payment.status === paymentStatus.CANCELLED && (cancelled = true)
      );

      return Date.parse(batch.endDate) < Date.now() || cancelled;
    }
  );

  return batchesFilteredByEndDateAndStatus;
};

export const getOnGoingSubscriptions = ({
  subscriptions,
  batches,
  payments,
}) => {
  let filteredBySubscription = [];
  let filteredByEndDate = [];

  subscriptions.forEach((subscription) => {
    let fb = getBatchById(subscription.batchId, batches);
    filteredBySubscription.push(fb);
  });

  filteredByEndDate = filteredBySubscription.filter((batch) => {
    let filteredPayments = getPaymentsByBatchId(payments, batch.id);
    let cancelled = false;

    filteredPayments.forEach(
      (payment) =>
        payment.status === paymentStatus.CANCELLED && (cancelled = true)
    );

    return Date.parse(batch.endDate) >= Date.now() && !cancelled;
  });

  return filteredByEndDate;
};
