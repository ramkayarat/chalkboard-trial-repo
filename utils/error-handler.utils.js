import { errorTypes } from "../constants/error.types";

export const handleLoginError = (error) => {
  const errorMessage = error[0].messages[0];

  if (errorMessage.id === errorTypes.LOGIN_CREDENTIALS) {
    return "Email or password Invalid";
  }

  return errorMessage.message;
};

export const handlerSignUpError = (error) => {
  const errorMessage = error[0].messages[0];

  if (errorMessage.id === errorTypes.ROLE_NOT_FOUND)
    return "Default role not found. Contact Admin";

  return errorMessage.message;
};

export const handlePassowrdResetError = (error) => {
  const errorMessage = error[0].messages[0];

  return errorMessage.message;
};
