import moment from 'moment';

export const capitalizeString = (string) =>
  string.charAt(0).toUpperCase() + string.slice(1);

export const replaceAt = (index, replacement, string) =>
  string.substr(0, index) +
  replacement +
  string.substr(index + replacement.length);

export const formatDate = ({ date, format }) => moment(date, format);

export const checkIsSubscribed = (subscriptions, id) => {
  let isSubscribed = false;

  subscriptions &&
    subscriptions.forEach((subscription) => {
      subscription.batchId === id && (isSubscribed = true);
    });

  return isSubscribed;
};

export const getPathsFromSlugs = (slugs) =>
  slugs.map((slug) => ({
    params: {
      slug,
    },
  }));

export const getSelectedFilter = ({ key, filterOptions, selectedValue }) =>
  filterOptions[key].find((element) => element.value === selectedValue);
