export const shiftFocusToPrevious = ({ evt, id }) => {
  if (evt.which === 8 || evt.which === 37) {
    if (id > 0) {
      let previousId = parseInt(id) - 1;
      document.getElementById(previousId).focus();
    }
  }
};

export const shiftFocusToNext = ({ evt, id, otpLength }) => {
  if (evt.which !== 8 && evt.which !== 37) {
    if (id < otpLength - 1) {
      let nextId = parseInt(id) + 1;
      document.getElementById(nextId).focus();
    }
  }
};
