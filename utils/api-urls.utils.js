export const API_BASE_URL = process.env.NEXT_PUBLIC_CSR_STRAPI_BASE_URL;

//auth relatde
export const SIGNUP_URL = '/auth/local/register';
export const LOGIN_URL = '/auth/local/';

// user related
export const GET_USER_DATA_URL = '/users/me';

// stduent related
export const GET_STUDENT_DATA_URL = '/get-student-data';
export const UPDATE_STUDENT_PROFILE_URL = '/update-student-profile';

//notification related
export const SEND_VERIFICATION_SMS_URL = '/send-verification-sms';
export const VERIFY_OTP_URL = '/verify-otp';
export const FORGOT_PASSWORD_URL = '/auth/forgot-password';
export const RESET_PASSWORD_URL = '/auth/reset-password';

// payment related
export const GET_INVOICE_URL = '/get-invoice';
export const GET_PAYMENTS_URL = '/payments';
export const GENEARTE_INVOICE_URL = '/generate-invoice';
export const GENEARTE_PENDING_PAYMENT_INVOICE_ROUTE = '/generate-pending-payment-invoice';
export const CANCEL_SUBSCRIPTION_URL = '/cancel-subscription';
export const FULFILL_ORDER_URL = '/fulfill-order';
export const FULFILL_PENDING_PAYMENT_URL = '/fulfill-pending-payment';

//course related
export const GET_COURSES_URL = '/courses';
export const GET_BATCHES_URL = '/batches';

//faqs related
export const GET_FAQS_URL = '/faqs';

//reviews related
export const GET_REVIEWS_URL = '/reviews';

// admin task related
export const GET_TASKS_URL = '/tasks';

//admin functions
export const ENROLL_STUDENT_URL = '/enroll-student';
export const MARK_ADDED_URL = '/mark-added';
export const MARK_CANCELLED_PAYMENT_URL = '/mark-cancelled-payment';
export const MARK_PAID_PAYMENT_URL = '/mark-paid-payment';
export const UN_ENROLL_STUDENT_URL = '/un-enroll-student';
export const UNDO_ENROLL_STUDENT_URL = '/undo-enroll-student';


//support 
export const CONTACT_SUPPORT_ROUTE = '/contact-support';