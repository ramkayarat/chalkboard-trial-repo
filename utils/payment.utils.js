import moment from 'moment';

export const groupPaymentsByBatchId = (payments) => {
  let groupedPayments = payments.reduce((groupedPayment, payment) => {
    groupedPayment[payment.batchId] = [
      ...(groupedPayment[payment.batchId] || []),
      payment,
    ];
    return groupedPayment;
  }, {});

  return groupedPayments;
};

export const getPaymentsByBatchId = (payments, id) =>
  payments.filter((payment) => payment.batchId == id);

export const getPaymentMonth = (startDate, dueDate) =>
  (moment(dueDate).year() - moment(startDate).year()) * 12 -
  moment(startDate).month() +
  moment(dueDate).month();
